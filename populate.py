from assist.models import *
from twitter.models import *
from assist import register
from assist import reminder
from utils import *

from django.template import Context, loader
from django.views.decorators.http import require_http_methods
from django.template import Context, loader

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.core import serializers
import hashlib
import re
from datetime import *
import random
import json
import traceback, pdb
from django.conf import settings


def populate_services():

    s = Service() 
    s.slug = create_slug(length=10)
    s.name = "Free"
    s.trial_num_dmy = 30
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 0.0
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.allow_sf = True
    s.allow_sf_incoming = True
    s.allow_sf_outgoing = True
    s.allow_formmail = True
    s.crawl_email_howlong = 2
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2
    s.max_users_flat_rate = 1
    s.price_per_extra_user = 0
    s.price_per_synced_email = 0
    s.max_store_email_howlong = 30
    s.save()

    s = Service() 
    s.slug = create_slug(length=10)
    s.name = "Sync"
    s.trial_num_dmy = 30
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 0.0
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.allow_sf = True
    s.allow_sf_incoming = True
    s.allow_sf_outgoing = True
    s.allow_formmail = True
    s.crawl_email_howlong = 2
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2
    s.max_users_flat_rate = 1
    s.price_per_extra_user = 0
    s.price_per_synced_email = 0
    s.max_store_email_howlong = 30
    s.save()

    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Pro"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 9.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.allow_sf = True
    s.allow_sf_incoming = True
    s.allow_sf_outgoing = True
    s.allow_formmail = True
    s.crawl_email_howlong = 2
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2
    s.max_users_flat_rate = 3
    s.price_per_extra_user = 2.00
    s.price_per_synced_email = 0.02
    s.max_store_email_howlong = 30
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Yearly"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 99.95
    s.num_dmy = 1
    s.dmy = "Y"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.allow_sf = True
    s.allow_sf_incoming = True
    s.allow_sf_outgoing = True
    s.allow_formmail = True
    s.crawl_email_howlong = 2
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2
    s.max_users_flat_rate = 3
    s.max_store_email_howlong = 30
    s.price_per_extra_user = 20.00  
    s.price_per_synced_email = 0.02
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "SillyIntro"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 99.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.allow_sf = True
    s.allow_sf_incoming = True
    s.allow_sf_outgoing = True
    s.allow_formmail = False
    s.crawl_email_howlong = 2
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2  
    s.save()
    
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Corporate"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 159.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2  
    s.save()

    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Startup"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 79.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = 1000
    s.max_storage_gb = 20
    s.hours_training = 2  
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Intro"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 29.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = 200
    s.max_storage_gb = 10
    s.hours_training = 1  
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Assistant"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 4.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    s.max_store_email_howlong = 30
    s.max_queries_per_num_dmy = 50
    s.crawl_email_howlong = 2
    s.max_storage_gb = 1
    s.hours_training = None
    s.max_users_flat_rate = 1
    s.price_per_extra_user = 3.00  
    s.price_per_synced_email = 0.02
    s.save()

    s = Service()
    s.slug = create_slug(length=10)
    s.name = "SalesforceIntro"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 0
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    s.allow_mobile = False
    s.allow_sf = True
    s.allow_sf_incoming = False
    
    s.max_queries_per_num_dmy = 50
    s.crawl_email_howlong = 2
    s.max_storage_gb = 1
    s.hours_training = None
    s.max_users_flat_rate = 1
    s.price_per_extra_user = 3.00    
    s.save()

    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Archive"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 0
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    s.allow_mobile = False
    s.allow_sf = True
    s.allow_sf_incoming = True
    
    s.max_queries_per_num_dmy = 50
    s.crawl_email_howlong = 465
    s.max_storage_gb = 1
    s.hours_training = None
    s.max_users_flat_rate = 1
    s.price_per_extra_user = 3.00    
    s.save()

    s = Service()
    s.slug = create_slug(length=10)
    s.name = "Magnify"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 0
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = False
    s.allow_firefox = True
    s.allow_mobile = False
    s.allow_sf = False
    s.allow_sf_incoming = False
    
    s.max_queries_per_num_dmy = 50
    s.crawl_email_howlong = 2
    s.max_storage_gb = 1
    s.hours_training = None
    s.max_users_flat_rate = 1
    s.price_per_extra_user = 3.00    
    s.save()
    

    
def populate_account(account_name, service_name):
    a = Account.objects.get(name=account_name)
    s = Service.objects.get(name__iexact=service_name)
    a.service = s
    a.save()
    
def populate_services_hidden():
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "NotedTeam"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 0
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2  
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "NotedIndividual"
    s.trial_num_dmy = 7
    s.trial_dmy = 'D'
    s.price_per_num_dmy = 0
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = False
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2  
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "EarlyAdoptersTeam"
    s.price_per_num_dmy = 0
    s.num_dmy = None
    s.dmy = None
    s.recurring = True   
    s.hidden = True
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2  
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "EarlyAdoptersIndividual"
    s.price_per_num_dmy = 0
    s.num_dmy = None
    s.dmy = None
    s.recurring = True   
    s.hidden = True
    s.allow_crawl = False
    
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2  
    s.save()
    
    # For debugging Paypal stuff
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "CorporateNotrial"
    s.price_per_num_dmy = 159.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = True
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = None
    s.max_storage_gb = 50
    s.hours_training = 2  
    s.save()

    s = Service()
    s.slug = create_slug(length=10)
    s.name = "StartupNotrial"
    s.price_per_num_dmy = 79.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = True
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = 1000
    s.max_storage_gb = 20
    s.hours_training = 2  
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "IntroNotrial"
    s.price_per_num_dmy = 29.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = True
    s.allow_crawl = True
    
    s.max_queries_per_num_dmy = 200
    s.max_storage_gb = 10
    s.hours_training = 1  
    s.save()
    
    s = Service()
    s.slug = create_slug(length=10)
    s.name = "PersonalNotrial"
    s.price_per_num_dmy = 9.95
    s.num_dmy = 1
    s.dmy = "M"
    s.recurring = True   
    s.hidden = True
    s.allow_crawl = False
    
    s.max_queries_per_num_dmy = 50
    s.max_storage_gb = 1
    s.hours_training = None
    s.save()

#def populate_demo():
#    os.system('python2.6 worker.py procmail data/demo_setup.eml')
#    demo = Account.objects.get(name='demo')
#    populate_account('demo', "NotedTeam")
    
def populate_twitter():
    
    
    if settings.SERVER == 'local':
        try:
            tu, created = TwitterUser.objects.get_or_create_by_screen_name('testnoted2')
            tu.password_aes = CRYPTER.Encrypt('squish222')
            tu.site = Site.objects.get(name='Noted')
            tu.is_noted=True
            tu.save()
            
            tu, created = TwitterUser.objects.get_or_create_by_screen_name('testnoted3')
            tu.password_aes = CRYPTER.Encrypt('squish222')
            tu.site = Site.objects.get(name='Calenvy')
            tu.is_noted=True
            tu.save() 
        except:
            print "Warning -- could not connect to Twitter to create testnoted2/testnoted3 accounts"
        
    
def populate_all():
    populate_services()
    populate_services_hidden()
    populate_twitter()
    
