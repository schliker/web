import urllib, urllib2, hashlib, pdb, traceback, xml.etree.ElementTree as ET
import base
from creds import *
from utils import *

class RapleafFinder(base.BaseFinder):
    "Look up stuff on Rapleaf"
    
    def __init__(self, **kwargs):
        super(RapleafFinder, self).__init__(**kwargs)
        self.name = 'rapleaf'
        self.api_key = RAPLEAF_API_KEY              # API key to use to search Rapleaf
        
    def find(self, d):
        """"
        Fills the given dictionary with the name, location, job title, company,
        and any images found using Rapleaf. The dictionary needs to have an email.
        """
                        
        if 'email' not in d:
            return {}
        
        try:         
             
            email = d.get('email').strip().lower()
            url = "https://personalize.rlcdn.com/v4/dr?"
            # url = "http://api.rapleaf.com/v3/person/email/" + urllib.quote_plus(email) + '?'
            url += urllib.urlencode({
                'email':    email,
                'api_key':  self.api_key,
                'format':   'xml'
            })
            print url
            response = urllib2.urlopen(url) 
            
            if response.code != 200:
                return {}
            xml = response.read().decode('utf-8')
            print xml
            root = ET.XML(xml)
            
            basic_results = {}
                        
            self.fill_field('gender', basic_results, root)
            self.fill_field('location', basic_results, root)
            self.fill_field('age', basic_results, root)
            self.fill_field('occupation', basic_results, root)
            self.fill_field('education', basic_results, root)
            self.fill_field('likely_smartphone_user', basic_results, root)
            self.fill_field('high_net_worth', basic_results, root)
            
            memberships = self.get_memberships(root)
            
            return {
                'basics':       basic_results,
                'memberships':  memberships,
                'raw_xml':      xml
            }
        
            # TODO: Store everything we get from the XML
            
        except:
            log("Can't get info from Rapleaf: ", traceback.format_exc())
            return {}
        
    
    def fill_field(self, field_name, d, root):
        """
        Finds the first element in the root that matches the field name and 
        adds its text as a field to d, with field_name as a key.
        """
        if root is not None:
            field = root.find(field_name)
            if field is not None:
                d[field_name] = decode_htmlentities(field.text)
                return True
        return False
    
    def get_memberships(self, root):
        """
        Return a list of all the memberships the user has.
        Each membership is a dict with these possible fields:
            site: 'myyearbook.com'
            profile_url: 'http://blahblah'
            image_url: 'http://blahblah'
            num_friends: 234
            followers:    33
            num_followed: 44
            (maybe others)
        """
        
        memberships = []
        
        for child in root.findall('memberships/primary/membership'):
            if child.attrib.get('exists').lower() == 'true':
                memberships.append(dict((key, decode_htmlentities(value)) for key, value in child.attrib.iteritems()))
                
        return memberships
    
    