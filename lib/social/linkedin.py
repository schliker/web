import urllib, urllib2, hashlib, cookielib, BeautifulSoup, random, cPickle
import pdb, traceback
import base
from django.core.cache import cache
from creds import *
from utils import decode_htmlentities as decode

class LinkedInFinder(base.BaseFinder):
    "Look up stuff on LinkedIn"
    
    def __init__(self, **kwargs):
        super(LinkedInFinder, self).__init__(**kwargs)
        self.name = 'linkedin'
        
        
    def _login(self, opener, login, password):
        """Log into the site, pretending to be a browser user
        This sets the cookie within the HTTPCookieProcessor"""
        data = opener.open('https://m.linkedin.com/session', urllib.urlencode({
            'login': login, 
            'password': password
        })).read()
        
    def _search(self, opener, name, company):
        """Assuming the user is successfully logged in, search on a given name and return the
        search results as parsed by BeautifulSoup"""
        
        try:
            data = opener.open('https://m.linkedin.com/members?' + urllib.urlencode({
                'search_term': name + ' ' + company if company else name,
                'filter': 'keywords',
                'commit': 'Search'
            })).read()
            soup = BeautifulSoup.BeautifulSoup(data)
            search_results = soup.find(id='search_results')
            people = search_results.findAll('p')[1:]    # skip the interstatial_message class
            return people, True
        except:
            # If not a search results page (for example, we're not logged in), we get here
            return None, False
            
    def find(self, d):
        
        company = d.get('company')
                    
        if d.get('first_name'):
            name = ' '.join([d.get('first_name', ''), d.get('last_name', '')]).strip()
        elif d.get('name'):
            name = d.get('name')
        else:
            return {}                         # without a name, we can't do anything on LinkedIn
        
        try:
            login, password = random.choice(LINKEDIN_ACCOUNTS)  # choose a random account to do our search
            cache_key = 'LinkedInFinder.cookie:' + login 
            
            # Create a URL opener. If cookies are stored in memcached, use those
            cache_value = cache.get(cache_key)
            if cache_value is not None:
                try:
                    jar = cPickle.loads(str(cache_value))
                except:
                    jar = base.PicklableCookieJar()
            else:
                jar = base.PicklableCookieJar()
            cookie_processor = urllib2.HTTPCookieProcessor(jar)            
            opener = urllib2.build_opener(cookie_processor)
            opener.addheaders = [('User-Agent', LINKEDIN_USER_AGENT)]
                    
            if not cache_value:
                # No cookie - do a fresh login and store the cookie
                self._login(opener, login, password)
                cache.set(cache_key, cPickle.dumps(cookie_processor.cookiejar))
                    
            people, success = self._search(opener, name, company)
            if not success:
                self._login(opener, login, password)
                cache.set(cache_key, cPickle.dumps(cookie_processor.cookiejar))
                people, success = self._search(opener, name, company)
                if not success:
                    return {}
            
            # Form the JSON structure of search results
            
            result_people = []
            for p in people:
                                     
                divs = p.findAll('div')
                result_p = {}
                
                try:
                    result_p['name'] = decode(divs[0].contents[0].contents[0])
                except:
                    pass
                try:
                    result_p['mobile_profile_url'] = 'https://m.linkedin.com' + divs[0].contents[0]['href'] # accessible when logged into the mobile site.
                    result_p['profile_key'] = result_p['mobile_profile_url'].split('/')[4] # might be used for finding LinkedIn profile
                except:
                    pass
                try:
                    result_p['title'] = decode(divs[1].contents[0])
                except:
                    pass
                
                try:
                    result_p['location'] = decode(divs[2].contents[0])
                except:
                    pass
                
                try:
                    result_p['field'] = decode(divs[3].contents[0])
                except:
                    pass
                
                result_people.append(result_p)
                
            try: # search the mobile profile of the first person
                self._mobile_profile_search(opener, result_people[0]) 
            except: # probably no people found
                pass 

            result = {
                'people': result_people
            }
            
            return result
        
        except:
            return {}
        
        
    def _mobile_profile_search(self, opener, person):
        """Assuming the user is successfully logged in, searches a particular person that has 
        a mobile profile url, filling in additional information such as the photo.
        Returns the given person and whether or not it was updated. 
        """
        
        modified = False
        try:
            data = opener.open(person['mobile_profile_url']).read()
            soup = BeautifulSoup.BeautifulSoup(data)
        except: # url error. mobile_profile_url likely does not exist.
            return person, False
        
        try: # add the image to the person
            search_results = soup.find('div', {'class' : 'photo'})
            image_url = search_results.contents[1].contents[0]['src']
            if not image_url.startswith('http'):
                image_url = 'https://m.linkedin.com' + image_url
            person['image'] = {'url': image_url}
            modified = True
        except:
            pass
        
        try: # add person's experience
            search_results = soup.find('div', {'class' : 'experience vevent vcard'})
            experience = {}
            try:
                experience['title'] = decode(search_results.find('h3', {'class' : 'title'}).contents[0])
            except:
                pass
            try:
                experience['organization'] = decode(search_results.find('h4', {'class' : 'org summary'}).contents[0])
            except:
                pass
            try:
                period = search_results.find('span', {'class' : 'period'}).findAll('abbr')
                experience['period'] = decode(period[0].contents[0]) + ' - ' + decode(period[1].contents[0])
            except:
                pass
            try:
                experience['description'] = decode(search_results.find('div', {'class' : 'description'}).contents[0].contents[0])
            except:
                pass
            if experience:
                if ('experience' not in person):
                    person['experience'] = []
                person['experience'].append(experience)
                modified = True
        except: # experience not found
            pass
        
        try: # add person's education
            search_results = soup.find('div', {'class' : 'education vevent vcard'})
            education = {}
            try:
                education['organization'] = decode(search_results.find('h3', {'class' : 'summary fn org'}).contents[0])
            except: 
                pass
            try:
                period = search_results.find('div', {'class' : 'description'}).findAll('abbr')
                education['period'] = decode(period[0].contents[0] + ' - ' + period[1].contents[0])
            except: 
                pass
            if education:
                if ('education' not in person):
                    person['education'] = []
                person['education'].append(education)
                modified = True
        except: # education not found
            pass
        
        try: # add person's websites
            websites = {}
            search_results = soup.find('div', {'id' : 'additional_information'}).findAll('a')
            for website_candidate in search_results:
                url = website_candidate['href'] 
                if url != '#profile':
                     websites[website_candidate.contents[0]] = url
            
            if websites:
                person['websites'] = websites
                modified = True
        except:
            pass
        
        return person, modified
                

