import urllib, urllib2, hashlib, cPickle, pdb, traceback
import base
import linkedin, gravatar, rapleaf
from django.core.cache import cache

FINDERS = [gravatar.GravatarFinder, rapleaf.RapleafFinder] # disable linkedin.LinkedInFinder for now

class MultiFinder(base.BaseFinder):
    "Look up kwargs in each of the various finders, report back when we've got something"
    
    INFO_AGE = 'age'
    INFO_GENDER = 'gender'
    INFO_EXPERIENCE = 'experience'
    INFO_EDUCATION = 'education'
    INFO_LOCATION = 'location'
    INFO_TITLE = 'title'
    INFO_IMAGE = 'image'
    INFO_MEMBERSHIPS = 'memberships'
    
    def __init__(self, **kwargs):
        self.finders = [f(**kwargs) for f in FINDERS]
        
    def multi_find(self, d):
        
        result = {}
        
        for f in self.finders:
            try:
                r = f.cached_find(d)
                result[f.name] = r
            except:
                print traceback.format_exc()
                pass            # That finder doesn't apply to this person
        
        return result
    
    def _get_basic_memberships(self, result, require_profile_url=True):
        "Extract an easy list of memberships and websites from the result structure returned by multi_find"
        
        # Each service (rapleaf, etc.) optionally returns a list of memberships to third-party sites (twitter, etc.)
        memberships = {}
        for service in result.values():
            for m in service.get('memberships', []):
                if ('profile_url' in m) or (require_profile_url is False):
                    site_name = m['site'].split('.')[0].lower()
                    memberships[site_name] = m
            for m in service.get('people', [])[:1]:
                for key, value in m.get('websites', {}).items():
                    memberships[key] = {'site': key, 'profile_url': value}
                    
        # Turn memberships into a dictionary sorted by site name
        memberships = memberships.values()
        memberships.sort(key=lambda m: m['site'])
        return memberships
      
    def _get_basic_title(self, result):
        "Return a single title from the result structure returned by multi_find"
        
        for service in result.values():
            if service.get('title'):
                return service['title']
            if service.get('people'):
                for p in service['people']:
                    if p.get('title'):
                        return p['title']
                        
    def _get_basic_image(self, result):
        "Return a single image url from the result structure, or None if None"
        
        try:
            for service in result.values():
                if service.get('image'):
                    return service['image']['url']
                if service.get('people'):
                    for p in service['people']:
                        if p.get('image'):
                            return p['image']['url']
                for m in service.get('memberships', []):
                    if 'image_url' in m:
                        return m['image_url']
        except:
            return None
        return None
    
    def _get_basic_info_generic(self, result, key):
        try:
            for service in result.values():
                #print "YYYYYYY", service     
                #print "XXXXXXX", service.get('basics')
                if service.get('basics', None):
                    basics = service.get('basics')
                    #print 'ZZZZZ', basics
                    #print 'GGGGG', basics['gender']
                    #print '88888', basics.keys()
                    if key in basics:
                        #print '696969', basics[key]
                        return basics[key]
                if service.get(key):
                    return service[key]
                if service.get('people'):
                    for p in service['people']:
                        if p.get(key):
                            return p[key]
        except:
            return None
        return None        
    
    def get_basic_info(self, result, key, **kwargs):
        if result is None:
            return None
        
        if key in [self.INFO_EXPERIENCE, self.INFO_EDUCATION, self.INFO_LOCATION, self.INFO_AGE, self.INFO_GENDER]:
            return self._get_basic_info_generic(result, key)
        
        elif key == self.INFO_TITLE:
            return self._get_basic_title(result)
        
        elif key == self.INFO_IMAGE:
            return self._get_basic_image(result)
        
        elif key == self.INFO_MEMBERSHIPS:
            return self._get_basic_memberships(result, require_profile_url=kwargs.get('require_profile_url', True))
        
        else:
            raise NotImplementedError


    def _delete_basic_info_generic(self, result, key):
        try:
            for service in result.values():
                
                #print "YYYYYYY", service     
                #print "XXXXXXX", service.get('basics')
                if service.get('basics', None):
                    basics = service.get('basics')
                    #print 'ZZZZZ', basics
                    #print 'GGGGG', basics['gender']
                    #print '88888', basics.keys()
                    if key in basics:
                        #print '696969', basics[key]
                        del basics[key]
                
                if key in service:
                    del service[key]
                if service.get('people'):
                    for p in service['people']:
                        if key in p:
                            del p[key]
        except:
            return None
        return None        

    def _delete_basic_image(self, result):        
        try:
            for service in result.values():
                if service.get('image'):
                    del service['image']
                if service.get('people'):
                    for p in service['people']:
                        if p.get('image'):
                            del p['image']                            
        except:
            return None
        return None

    def _delete_basic_memberships(self, result):
        # Each service (rapleaf, etc.) optionally returns a list of memberships to third-party sites (twitter, etc.)
        memberships = {}
        for service in result.values():
            if service.get('memberships'):
                del service['memberships']
            for m in service.get('people', []):
                if m.get('websites'):
                    del m['websites']
                    

    def delete_basic_info(self, result, key, **kwargs):
        if key in [self.INFO_EXPERIENCE, self.INFO_EDUCATION, self.INFO_LOCATION, self.INFO_AGE, self.INFO_GENDER]:
            return self._delete_basic_info_generic(result, key)
        elif key == self.INFO_IMAGE:
            return self._delete_basic_image(result)
        elif key == self.INFO_MEMBERSHIPS:
            return self._delete_basic_memberships(result)
        else:
            raise NotImplementedError           
        