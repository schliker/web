import re, pytz
from htmlentitydefs import name2codepoint as n2cp
from django.conf import settings

# from http://github.com/sku/python-twitter-ircbot/blob/321d94e0e40d0acc92f5bf57d126b57369da70de/html_decode.py
def decode_htmlentities(string):
    
    def substitute_entity(match):
        ent = match.group(3)
        if match.group(1) == "#":
            # decoding by number
            if match.group(2) == '':
                # number is in decimal
                return unichr(int(ent))
            elif match.group(2) == 'x':
                # number is in hex
                return unichr(int('0x'+ent, 16))
        else:
            # they were using a name
            cp = n2cp.get(ent)
            if cp: return unichr(cp)
            else: return match.group()
    
    entity_re = re.compile(r'&(#?)(x?)(\w+);')
    return entity_re.subn(substitute_entity, string)[0]

SERVER_TIMEZONE = pytz.timezone(settings.TIME_ZONE)

def log(*args, **kwargs):
    if settings.LOG_ENABLED:
        try:
            sep = kwargs.get('sep', ' ')
            end = kwargs.get('end', '\n')
            f = kwargs.get('file', settings.LOG_FILE)
            level = kwargs.get('level', None)
            if f:
                timestamp = now().astimezone(SERVER_TIMEZONE).strftime("%m/%d %H:%M:%S")
                message = sep.join([u'%s' % str(item) for item in args]) + end
                f.write('%s> %s' % (timestamp, message))
                f.flush()
        except:
            pass