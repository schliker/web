"""
Cross Site Request Forgery Middleware.

This module provides a middleware that implements protection
against request forgeries from other sites.
"""

# MODIFIED by Emil: 
#  dependence on hashlib_constructor removed
#  ignores X-HttpRequest, does CSRF checking on all POST requests, and even on GET requests if @csrf_required is used
#  new csrf_required decorator 
#  added the "agent" field to every POST
#  allows the CSRF to be a comma-separated list of CSRF values, if any match then we're okay
#   (needed so that we can use multiple Noted plugins in the browser at the same time)

import re
import itertools
try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.3, 2.4 fallback.

from django.conf import settings
from django.http import HttpResponseForbidden
from django.utils.safestring import mark_safe
import hashlib  
from utils import *                             # ADDED [eg]                     

_ERROR_MSG = mark_safe('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><body><h1>403 Forbidden</h1><p>Cross Site Request Forgery detected. Request aborted.</p></body></html>')

_POST_FORM_RE = \
    re.compile(r'(<form\W[^>]*\bmethod\s*=\s*(\'|"|)POST(\'|"|)\b[^>]*>)', re.IGNORECASE)

# For windowmob, GET forms also need to include the agent field
_GET_FORM_RE = \
    re.compile(r'(<form\W[^>]*\bmethod\s*=\s*(\'|"|)GET(\'|"|)\b[^>]*>)', re.IGNORECASE)
    
_HTML_TYPES = ('text/html', 'application/xhtml+xml')

def _make_token(session_id):
    return hashlib.md5(settings.SECRET_KEY + session_id).hexdigest()

class CsrfViewMiddleware(object):
    """
    Middleware that requires a present and correct csrfmiddlewaretoken
    for requests that have an active session, if the requests are:
        POST requests (not used in Calenvy), or
        @csrf_required requests (most Ajax calls)
    """
    
    def process_view(self, request, callback, callback_args, callback_kwargs):
        if (request.method == 'POST') or (getattr(callback, 'csrf_required', False)):
            if getattr(callback, 'csrf_exempt', False):
                return None

            # Disable AJAX exemption -- we're using CSRF token there too [eg, 8/12]
            # if request.is_ajax():
            #    return None

            try:
                session_id = request.COOKIES[settings.SESSION_COOKIE_NAME]
            except KeyError:
                # No session, no check required
                return None

            csrf_token = _make_token(session_id)
            # check incoming token
            try:
                request_csrf_token = request.REQUEST['csrfmiddlewaretoken']
            except KeyError:
                return HttpResponseForbidden(_ERROR_MSG)
            
            if not any([t == csrf_token for t in request_csrf_token.split(',')]):
                return HttpResponseForbidden(_ERROR_MSG)

        return None

class CsrfResponseMiddleware(object):
    """
    Middleware that post-processes a response to add a
    csrfmiddlewaretoken if the response/request have an active
    session.
    """
    def process_response(self, request, response):
        if getattr(response, 'csrf_exempt', False):
            return response

        agent = get_agent(request)          # Added [eg]
        
        csrf_token = None
        try:
            # This covers a corner case in which the outgoing response
            # both contains a form and sets a session cookie.  This
            # really should not be needed, since it is best if views
            # that create a new session (login pages) also do a
            # redirect, as is done by all such view functions in
            # Django.
            cookie = response.cookies[settings.SESSION_COOKIE_NAME]
            csrf_token = _make_token(cookie.value)
        except KeyError:
            # Normal case - look for existing session cookie
            try:
                session_id = request.COOKIES[settings.SESSION_COOKIE_NAME]
                csrf_token = _make_token(session_id)
            except KeyError:
                # no incoming or outgoing cookie
                pass

        if csrf_token is not None and \
                response['Content-Type'].split(';')[0] in _HTML_TYPES:

            def add_csrf_and_agent_field(match):
                """Adds the CSRF field and the agent field (if any) to the <form>
                Use this one for POST requests"""
                hidden_div = "<div style='display:none;'>"
                hidden_div += "<input type='hidden' class='csrfmiddlewaretoken'" + \
                    " name='csrfmiddlewaretoken' value='" + csrf_token + \
                    "' />"
                if agent:
                    hidden_div += "<input type='hidden' name='agent' value='%s' />" % agent       
                hidden_div += "</div>"    
                return mark_safe(match.group() + hidden_div)

            def add_agent_field(match):
                """Adds the agent field (if any) to the <form>
                Use this one for GET requests"""
                hidden_div = "<div style='display:none;'>"
                if agent:
                    hidden_div += "<input type='hidden' name='agent' value='%s' />" % agent       
                hidden_div += "</div>"    
                return mark_safe(match.group() + hidden_div)            
            
            # Add CSRF and agent to any POST forms
            # Add agent only to any GET forms (don't add CSRF, it confuses the admin views) 
            
            u = unicode(response.content, 'utf-8') 
            u = _POST_FORM_RE.sub(add_csrf_and_agent_field, u)
            u = _GET_FORM_RE.sub(add_agent_field, u)
            response.content = u.encode('utf-8')

        return response

class CsrfMiddleware(CsrfViewMiddleware, CsrfResponseMiddleware):
    """Django middleware that adds protection against Cross Site
    Request Forgeries by adding hidden form fields to POST forms and
    checking requests for the correct value.

    In the list of middlewares, SessionMiddleware is required, and
    must come after this middleware.  CsrfMiddleWare must come after
    compression middleware.

    If a session ID cookie is present, it is hashed with the
    SECRET_KEY setting to create an authentication token.  This token
    is added to all outgoing POST forms and is expected on all
    incoming POST requests that have a session ID cookie.

    If you are setting cookies directly, instead of using Django's
    session framework, this middleware will not work.

    CsrfMiddleWare is composed of two middleware, CsrfViewMiddleware
    and CsrfResponseMiddleware which can be used independently.
    """
    pass

def csrf_response_exempt(view_func):
    """
    Modifies a view function so that its response is exempt
    from the post-processing of the CSRF middleware.
    """
    def wrapped_view(*args, **kwargs):
        resp = view_func(*args, **kwargs)
        resp.csrf_exempt = True
        return resp
    return wraps(view_func)(wrapped_view)

def csrf_view_exempt(view_func):
    """
    Marks a view function as being exempt from CSRF view protection.
    """
    # We could just do view_func.csrf_exempt = True, but decorators
    # are nicer if they don't have side-effects, so we return a new
    # function.
    def wrapped_view(*args, **kwargs):
        return view_func(*args, **kwargs)
    wrapped_view.csrf_exempt = True
    return wraps(view_func)(wrapped_view)

def csrf_exempt(view_func):
    """
    Marks a view function as being exempt from the CSRF checks
    and post processing.

    This is the same as using both the csrf_view_exempt and
    csrf_response_exempt decorators.
    """
    return csrf_response_exempt(csrf_view_exempt(view_func))


def csrf_required(view_func):
    """
    Marks a function as requiring CSRF protection, no matter whether
    it's a GET or a POST request. [eg 4/15/10] 
    Use this for all AJAX calls, except certain ones like login.
    
    Required in in this application because almost all of our Ajax
    functions use GET rather than POST (so they can be called via
    jsonp from the Firefox/Chrome plugin), so otherwise we wouldn't
    know which functions require this protection.
    """
    def wrapped_view(*args, **kwargs):
        return view_func(*args, **kwargs)
    wrapped_view.csrf_required = True
    return wraps(view_func)(wrapped_view)

