from pympler.muppy import muppy
from pympler.muppy import summary
from pympler.muppy import refbrowser
from pympler.muppy import tracker
from pympler.asizeof import asizeof
from django.conf import settings
import StringIO, sys
import cPickle

#print "CREATING MEMORY_TRACKER"
#MEMORY_TRACKER = tracker.SummaryTracker()
#
#def output_function(o):
#    return str(type(o))
#
#class MemoryMiddleware(object):
#    """
#    Measure memory taken by requested view, and response
#    """
#    def process_request(self, request):
#        if settings.PROFILER_ENABLED and request.GET.has_key(settings.PROFILER_MEM_KEY):      
#            req = request.META['PATH_INFO']
#            if req.find('static') == -1:
##                self.start_objects = muppy.get_objects()
#                #self.start_session_size = len(cPickle.dumps(request.session))
#                print "REQUEST DIFF:"
#                #MEMORY_TRACKER.print_diff()
#                
#                all_objects = muppy.get_objects()
#                print "# OF OBJECTS:" , len(all_objects)
#                print "SIZE OF OBJECTS:", asizeof(all_objects)
#                
#    def process_response(self, request, response):
#        if settings.PROFILER_ENABLED and request.GET.has_key(settings.PROFILER_MEM_KEY):
#            req = request.META['PATH_INFO']
#            if req.find('static') == -1:
#                print "RESPONSE:"
#                #MEMORY_TRACKER.print_diff()
#                
#                all_objects = muppy.get_objects()
#                print "# OF OBJECTS:" , len(all_objects)
#                print "SIZE OF OBJECTS:", asizeof(all_objects)
#                
#                #out = StringIO.StringIO()
#                #old_stdout = sys.stdout
#                #sys.stdout = out
#                
#                #print req
##                self.end_objects = muppy.get_objects()
##                sum_start = summary.summarize(self.start_objects)
##                sum_end = summary.summarize(self.end_objects)
##                diff = summary.get_diff(sum_start, sum_end)
##                summary.print_(diff)
##                #print '~~~~~~~~~'
##                #cb = refbrowser.ConsoleBrowser(response, maxdepth=2, str_func=output_function)
##                #cb.print_tree()
##                print '~~~~~~~~~'
##                a = asizeof(response)
##                print 'Total size of response object in kB: %s' % str(a/1024.0)
##                print '~~~~~~~~~'
##                a = asizeof(self.end_objects)
##                print 'Total size of end_objects in MB: %s' % str(a/1048576.0)
##                b = asizeof(self.start_objects)
##                print 'Total size of start_objects in MB: %s' % str(b/1048576.0)
##                print '~~~~~~~~~'
##                self.end_session_size = len(cPickle.dumps(request.session))
##                print 'Start size of request.session: %d' % self.start_session_size
##                print 'End size of request.session: %d' % self.end_session_size
#                
#                #sys.stdout = old_stdout
#                #stats_str = out.getvalue()
#
#                #if response and response.content and stats_str:
#                #    response.content = "<pre>" + stats_str + "</pre>"
#                    
#        return response



class MemoryMiddleware(object):
    """
    Measure memory taken by requested view, and response
    """
    def process_request(self, request):
        if settings.PROFILER_ENABLED and request.GET.has_key(settings.PROFILER_MEM_KEY):      
            req = request.META['PATH_INFO']
            if req.find('static') == -1:
                self.start_objects = muppy.get_objects()
                
                
    def process_response(self, request, response):
        if settings.PROFILER_ENABLED and request.GET.has_key(settings.PROFILER_MEM_KEY):
            req = request.META['PATH_INFO']
            if req.find('static') == -1:
                print req
                self.end_objects = muppy.get_objects()
                sum_start = summary.summarize(self.start_objects)
                sum_end = summary.summarize(self.end_objects)
                diff = summary.get_diff(sum_start, sum_end)
                summary.print_(diff)
                #print '~~~~~~~~~'
                #cb = refbrowser.ConsoleBrowser(response, maxdepth=2, str_func=output_function)
                #cb.print_tree()
                print '~~~~~~~~~'
                a = asizeof(response)
                print 'Total size of response object in kB: %s' % str(a/1024.0)
                print '~~~~~~~~~'
                a = asizeof(self.end_objects)
                print 'Total size of end_objects in MB: %s' % str(a/1048576.0)
                b = asizeof(self.start_objects)
                print 'Total size of start_objects in MB: %s' % str(b/1048576.0)
                print '~~~~~~~~~'
                #self.start_objects = None
                #self.end_objects = None
                
        return response

