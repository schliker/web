#!/usr/bin/env python2.6
# Script Written By Marcello Herreshoff
#pgtop.py -- This script reads settings.py and launches pgtop with the correct flags

import init_platform

from django.conf import settings



engine = settings.DATABASE_ENGINE
name = settings.DATABASE_NAME
user = settings.DATABASE_USER
password = settings.DATABASE_PASSWORD
host = settings.DATABASE_HOST 
port = settings.DATABASE_PORT

if "postgresql" not in engine:
    print "You are no longer using postgresql, and it would therefore be useless to call pgtop."
    sys.exit(1)

arguments = ['pgtop',
            '--host', host,
            '--database', name,
            '--user', user,
            '--pass', password]

if port: arguments += ['--port', port]

exit_code = os.spawnvp(os.P_WAIT, 'pgtop', arguments)

if exit_code != 0:
    print "pgtop exited with a non-zero exit code or is not installed."