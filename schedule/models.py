from django.contrib.contenttypes import generic
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.template.defaultfilters import date
from django.utils.translation import ugettext, ugettext_lazy as _
from schedule.periods import Month
from schedule.occurrence import Occurrence
from assist.constants import *
import datetime as _datetime                # Renamed [Emil]
import calendar
from dateutil_mod import rrule
from utils import *                         # Added [Emil]


freqs = (   ("YEARLY", _("Yearly")),
            ("MONTHLY", _("Monthly")),
            ("WEEKLY", _("Weekly")),
            ("DAILY", _("Daily")),
            ("HOURLY", _("Hourly")),
            ("MINUTELY", _("Minutely")),
            ("SECONDLY", _("Secondly")))

class Rule(models.Model):
    """
    This defines a rule by which an event will recur.  This is defined by the
    rrule in the dateutil documentation.

    * name - the human friendly name of this kind of recursion.
    * description - a short description describing this type of recursion.
    * frequency - the base recurrence period
    * param - extra params required to define this type of recursion. The params
      should follow this format:

        param = [rruleparam:value;]*
        rruleparam = see list below
        value = int[,int]*

      The options are: (documentation for these can be found at
      http://labix.org/python-dateutil#head-470fa22b2db72000d7abe698a5783a46b0731b57)
        ** count
        ** bysetpos
        ** bymonth
        ** bymonthday
        ** byyearday
        ** byweekno
        ** byweekday
        ** byhour
        ** byminute
        ** bysecond
        ** byeaster
    """
    name = models.CharField(_("name"), null=True, max_length=32)
    description = models.TextField(_("description"), null=True)
    frequency = models.CharField(_("frequency"), choices=freqs, max_length=10)
    params = models.TextField(_("params"), null=True, blank=True)

    class Meta:
        verbose_name = _('rule')
        verbose_name_plural = _('rules')

    def get_params(self):
        """
        >>> rule = Rule(params = "count:1;bysecond:1;byminute:1,2,4,5")
        >>> rule.get_params()
        {'count': 1, 'byminute': [1, 2, 4, 5], 'bysecond': 1}
        """
        if self.params is None:
            return {}
        params = self.params.split(';')
        param_dict = []
        for param in params:
            param = param.split(':')
            if len(param) == 2:
                param = (str(param[0]), [int(p) for p in param[1].split(',')])
                if len(param[1]) == 1:
                    param = (param[0], param[1][0])
                param_dict.append(param)
        return dict(param_dict)

    # Added [Emil]
    def get_rrule(self, start):
        """Returns the python-dateutil rrule structure corresponding to this Rule"""
        params = self.get_params()
        frequency = 'rrule.%s' % self.frequency
        r = rrule.rrule(eval(frequency), dtstart=start, **params)
        return r

    # Added [Emil]
    def get_ics_rrule(self):
        """Returns the string used for the .ics file to describe this rule
        .ics format is the same as self.params except:
            - it includes the self.frequency,
            - BYWEEKDAY is called BYDAY,
            - BYDAY uses two-letter day names instead of numbers"""

        ics_params = [
            "FREQ=%s" % self.frequency,
            "WKST=SU"
        ]

        if self.params:
            params = self.params.upper().split(';')
            for param in params:
                param = param.split(':')
                if len(param) == 2:
                    if param[0] == 'BYWEEKDAY' or param[0] == 'BYDAY':
                        day_numbers = param[1].split(',')
                        day_names = [ICS_DAY_NAMES[int(n)] for n in day_numbers]
                        ics_params.append("BYDAY=%s" % ','.join(day_names))
                    else:
                        ics_params.append("%s=%s" % tuple(param))

        result = ';'.join(ics_params)
        log('get_ics_rrule: returning %s' % result)
        return result

    # Added [Emil]
    @staticmethod
    def get_rule_from_ics_rrule(rrule_str):
        '''Make a Rule object from the .ics rule string
        (which does not include the RRULE= at the beginning).
        Returns None if this is not a valid rule.
        Does NOT save Rule to the DB!'''

        try:
            frequency = None
            params = []

            rrule_params = rrule_str.lower().split(';')
            for rp in rrule_params:
                rp = rp.split('=')
                if len(rp) == 2:
                    key, value = rp
                    if key == 'freq':
                        value = value.upper()
                        if any([value == freq[0] for freq in freqs]):
                            frequency = value
                    elif key == 'wkst':
                        pass
                    elif key == 'byday':
                        days = [str(ICS_DAY_NAMES.index(name.upper())) for name in value.split(',')]
                        params.append('byweekday:%s' % ','.join(days))
                    else:
                        params.append('%s:%s' % (key, value))

            params = ';'.join(params)
            if not frequency:
                return None
            rule = Rule(frequency=frequency, params=params)

            # Just in case, try to generate a dateutil.rrule from it (validate keywords, etc.)
            rrule = rule.get_rrule(now())

            return rule
        except:
            return None

    def __unicode__(self):
        """Human readable string for Rule"""
        return self.name or '%s %s' % (self.frequency, self.params)


class EventManager(models.Manager):

    def scheduled(self):
        queryset = self.filter(Q(status=EVENT_STATUS_SCHEDULED) | Q(status=EVENT_STATUS_TENTATIVE))
        queryset = queryset.filter(activated=True, guessed=False)
        return queryset

    def scheduled_Q(self):
        return (Q(status=EVENT_STATUS_SCHEDULED) | Q(status=EVENT_STATUS_TENTATIVE)) & Q(activated=True, guessed=False)


class Event(models.Model):
    # Extra fields added by Noted [Emil]
    EVENT_STATUS = (
        (EVENT_STATUS_TENTATIVE, 'Tentative'),
        (EVENT_STATUS_SCHEDULED, 'Scheduled'),
        (EVENT_STATUS_CANCELED, 'Canceled'),
        (EVENT_STATUS_COMPLETED, 'Completed'),
        (EVENT_STATUS_FADING, 'Fading'),
        (EVENT_STATUS_TAGONLY, 'Tag only'),
        (EVENT_STATUS_FOLLOWUP_REMINDER, 'Send a followup reminder at the given time'),
    )

    EVENT_PERMS = (
        (EVENT_PERMS_PRIVATE,    'Only From:, To: or Cc: people can see that the event exists'),
        (EVENT_PERMS_PROTECTED,  'Anyone can see the event, but non-From/To/Cc people need to request permission to do more with it'),
        (EVENT_PERMS_PUBLIC,     'Anyone can see the event (and download the file if any)'),
    )

    DISPLAY_DEFAULT = 'dDEF'
    DISPLAY_QUESTION = 'dQUE'
    DISPLAY_ANSWER = 'dANS'
    DISPLAY_EXCLAMATION = 'dEXC'
    DISPLAY_TWITTER = 'dTWI'

    status = models.CharField(max_length=4, choices=EVENT_STATUS, default=EVENT_STATUS_TENTATIVE, null=True)
    original_status = models.CharField(max_length=4, choices=EVENT_STATUS, default=EVENT_STATUS_TENTATIVE, null=True)   # status before we got any EventResponses
    source = models.CharField(max_length=4, null=True)                      # Where this email came from
    activated = models.NullBooleanField(default=True, null=True)            # Is this event "ready" for display in the newsfeed (digests, searches etc.)?
    guessed = models.NullBooleanField(default=False, null=True)             # Is this event one we parsed from an email but are unsure of?
    parent = models.ForeignKey('self', null=True, related_name='event_children')    # The parent of this event in a thread (null=root node)
    title_long = models.TextField(_("title_long"), null=True, blank=True)
    display_more_link = models.NullBooleanField(default=False, null=True, blank=True)   # whether there's more text than what we could display
    description = models.TextField(_("description"), null=True, blank=True)
    created_on = UTCDateTimeField(_("created on"), default = now)       # When we created this Event entry
    last_updated = UTCDateTimeField(null=True, default = now)
    original_email = models.ForeignKey('assist.Email', null=True, related_name='event_original_email')
    newsfeed_date = UTCDateTimeField(null=True, default=now)          # Date used for the newsfeed timeline in non-threaded view
    newsfeed_thread_date = UTCDateTimeField(null=True, default=now)   # Date used for the newsfeed timeline in threaded view
    perms = models.CharField(max_length=4, null=True, choices=EVENT_PERMS) # determines who can see / do stuff with this event
    timezone = models.CharField(max_length=64, default=settings.TIME_ZONE, null=True)
    phone = TruncCharField(max_length=64, null=True, blank=True)

    # People associated with event
    creator = models.ForeignKey('assist.Alias', null=True, related_name='alias_creator')
    owner = models.ForeignKey('assist.Alias', null=True, related_name='alias_owner')
    owner_account = models.ForeignKey('assist.Account', null=True, related_name='account_owner')
    ref_contacts = models.ManyToManyField('assist.Contact', null=True, related_name='event_ref_contacts')  # Contacts who this event is "about" (but they DON'T get notifications, etc.)
    formmail_contacts = models.ManyToManyField('assist.Contact', null=True, related_name='event_formmail_contacts') # Contacts who this (placeholder) event was sent to

    # For SCHEDULED, TENTATIVE and CANCELLED events
    start = UTCDateTimeField(_("start"), null=True)
    end = UTCDateTimeField(_("end"),help_text=_("The end time must be later than the start time."), null=True)
    rule = models.ForeignKey(Rule, null = True, blank = True, verbose_name=_("rule"), help_text=_("Select '----' for a one time only event."))
    end_recurring_period = UTCDateTimeField(_("end recurring period"), null = True, blank = True, help_text=_("This date is ignored for one time only events."))
    uid = models.CharField(max_length=255,unique=True)
    status_display = models.CharField(max_length=4, choices=EVENT_STATUS, null=True, blank=True)
    send_reminders = models.NullBooleanField(default=True)                  # are reminders enabled for this event?
    last_reminder_sent = UTCDateTimeField(null=True)
    modified = models.NullBooleanField(default=False, null=True)
    from_external_ics = models.NullBooleanField(default=False, null=True)
    ics = models.TextField(null=True, blank=True)                       # only store external ICSes here, for now
    dtstamp = UTCDateTimeField(null=True, default=now)
                                                                            #  (i.e., the only one we display in the newsfeed)
    # For TAGONLY events
    response_type = models.CharField(max_length=4, null=True)           # The EventResponse type comes from this

    objects = EventManager()

    class Meta:
        verbose_name = _('event')
        verbose_name_plural = _('events')

    def __unicode__(self):
        # date_format = u'l, %s' % ugettext("DATE_FORMAT")
        # return ugettext('%(title)s: %(start)s-%(end)s') % {
        #     'title': self.title_long,
        #     'start': date(self.start, date_format),
        #     'end': date(self.end, date_format),
        # }
        return '<Event: %s, %s>' % (self.status, ellipsize(self.title_long))

    def __repr__(self):
        return unicode(self)

    def get_owner(self):
        # returns a COntact
        owner_alias = self.original_email.owner_alias
        return owner_alias.contact if owner_alias else None

    def get_creator(self):
        return self.original_email.from_contact


    def deactivate_with_children(self):
        self.activated = False
        self.save()
        for c in self.event_children.all():
            c.deactivate_with_children()

    def find_thread_root(self):
        result = self
        while result.parent:
            result = result.parent
        return result


    # def get_absolute_url(self):
    #     return reverse('s_event', args=[self.id])

    def get_timezone(self):
        return timezone_from_timezonestr(self.timezone, self.ics)

    def format_for_display(self, tzinfo=None):
        if not tzinfo:
            tzinfo = self.get_timezone()

        local_event_date = self.start.astimezone(tzinfo)

        result = {\
            'reminder':             self.title_long,
            'day_of_week':          calendar.day_name[local_event_date.weekday()],
            'datetime':             format_span(self.start, display_today=True, display_future_weekdays=True, display_sometime=True, tzinfo=tzinfo, display_date=True),
            'date':                 '%d/%d' % (local_event_date.month, local_event_date.day),
            'time':                 format_time(local_event_date),
            'attendees':            self.original_email.contacts.all(),
            'newsfeed_thread_date': format_datetime(self.newsfeed_thread_date, display_today=True, display_past_weekdays=True, tzinfo=tzinfo),
            'newsfeed_date':        format_datetime(self.newsfeed_date, display_today=True, display_past_weekdays=True, tzinfo=tzinfo),
        }
        return result

    def has_owner_perms(self, alias):
        from assist.models import Email2Contact
        if alias and alias.contact == self.get_owner():
            return True
        account = self.owner_account
        if account and (alias.account == account) and alias.is_admin:
            return True
        if self.original_email:
            if self.original_email.owner_alias and self.original_email.owner_alias == alias:
                return True
            if Email2Contact.objects.filter(email=self.original_email, contact=alias.contact, type=Email2Contact.TYPE_ASSIGNED).count() > 0:
                return True
        # Meetings with just 2 people, either one should be able to change it
        if self.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE, EVENT_STATUS_CANCELED]:
            if self.original_email.contacts.count() <= 2 and alias.contact in self.original_email.contacts.all():   # Don't do the filtering for active here
                return True
        return False

    def get_local_span(self):
        tz = self.get_timezone()
        return (self.start.astimezone(tz), self.end.astimezone(tz))

    def get_occurrences(self, start, end):
        """
        >>> rule = Rule(frequency = "MONTHLY", name = "Monthly")
        >>> rule.save()
        >>> event = Event(rule=rule, start=_datetime.datetime(2008,1,1), end=_datetime.datetime(2008,1,2))
        >>> event.rule
        <Rule: Monthly>
        >>> occurrences = event.get_occurrences(_datetime.datetime(2008,1,24), _datetime.datetime(2008,3,2))
        >>> ["%s to %s" %(o.start, o.end) for o in occurrences]
        ['2008-02-01 00:00:00 to 2008-02-02 00:00:00', '2008-03-01 00:00:00 to 2008-03-02 00:00:00']

        Ensure that if an event has no rule, that it appears only once.

        >>> event = Event(start=_datetime.datetime(2008,1,1,8,0), end=_datetime.datetime(2008,1,1,9,0))
        >>> occurrences = event.get_occurrences(_datetime.datetime(2008,1,24), _datetime.datetime(2008,3,2))
        >>> ["%s to %s" %(o.start, o.end) for o in occurrences]
        []

        """

        tz = start.tzinfo                           # Time zone we will return times in (tz of the caller)
        self_start, self_end = self.get_local_span()# Event's start and end, expressed in timezone of the event

        if self.rule is not None:
            # rrule must be evaluated within the timezone of the original event, NOT the caller
            rule = self.rule.get_rrule(self_start)
            occurrences = []
            if self.end_recurring_period and self.end_recurring_period < end:
                end = self.end_recurring_period
            o_starts = iter(rule)
            try:
                while True:
                    o_start = o_starts.next()
                    o_end = o_start + (self_end - self_start)
                    o_start = reinterpret_by_tz(o_start)        # Deal with DST
                    o_end = reinterpret_by_tz(o_end)
                    if o_end >= start:
                        if o_start < end:
                            # Now, convert occurrence into caller's timezone (from start)
                            occurrences.append(Occurrence(self,o_start,o_end, tzinfo=tz))
                        else:
                            break
                return occurrences
            except StopIteration:
                pass
            return occurrences
        else:
            # check if event is in the period
            if self_start < end and self_end >= start:
                return [Occurrence(self, self_start, self_end, tzinfo=tz)]
            else:
                return []

    def happening_now(self):
        n = now()
        return self.start <= n <= self.end


# TODO: Delete this class, I don't think we're using it [Emil]
class EventThread(models.Model):
    # a collection of events that all belong to the same thread
    # for now, no parent-child relationships, it's just a collection
    slug = models.SlugField(unique=True)
    events = models.ManyToManyField(Event, null=True)

    def get_first_event(self):
        try:
            return self.events.order_by('newsfeed_date')[0]
        except:
            return None


class CalendarManager(models.Manager):
    """
    >>> user1 = User(username='tony')
    >>> user1.save()
    """
    def get_calendar_for_object(self, obj, distinction=None):
        """
        This function gets a calendar for an object.  It should only return one
        calendar.  If the object has more than one calendar related to it (or
        more than one related to it under a distinction if a distinction is
        defined) an AssertionError will be raised.  If none are returned it will
        raise a DoesNotExistError.

        >>> user = User.objects.get(username='tony')
        >>> try:
        ...     Calendar.objects.get_calendar_for_object(user)
        ... except Calendar.DoesNotExist:
        ...     print "failed"
        ...
        failed

        Now if we add a calendar it should return the calendar

        >>> calendar = Calendar(name='My Cal')
        >>> calendar.save()
        >>> calendar.create_relation(user)
        >>> Calendar.objects.get_calendar_for_object(user)
        <Calendar: My Cal>

        Now if we add one more calendar it should raise an AssertionError
        because there is more than one related to it.

        If you would like to get more than one calendar for an object you should
        use get_calendars_for_object (see below).
        >>> calendar = Calendar(name='My 2nd Cal')
        >>> calendar.save()
        >>> calendar.create_relation(user)
        >>> try:
        ...     Calendar.objects.get_calendar_for_object(user)
        ... except AssertionError:
        ...     print "failed"
        ...
        failed
        """
        calendar_list = self.get_calendars_for_object(obj, distinction)
        if len(calendar_list) == 0:
            raise Calendar.DoesNotExist, "Calendar does not exist."
        elif len(calendar_list) > 1:
            raise AssertionError, "More than one calendars were found."
        else:
            return calendar_list[0]

    def get_or_create_calendar_for_object(self, obj, distinction = None, name = None):
        """
        >>> user = User(username="jeremy")
        >>> user.save()
        >>> calendar = Calendar.objects.get_or_create_calendar_for_object(user, name = "Jeremy's Calendar")
        >>> calendar.name
        "Jeremy's Calendar"
        """
        try:
            return self.get_calendar_for_object(obj, distinction)
        except Calendar.DoesNotExist:
            if name is None:
                calendar = Calendar(name = unicode(obj))
            else:
                calendar = Calendar(name = name)
            calendar.save()
            calendar.create_relation(obj, distinction)
            return calendar

    def get_calendars_for_object(self, obj, distinction = None):
        """
        This function allows you to get calendars for a specific object

        If distinction is set it will filter out any relation that doesnt have
        that distinction.
        """
        ct = ContentType.objects.get_for_model(type(obj))
        if distinction:
            dist_q = Q(calendarrelation__distinction=distinction)
        else:
            dist_q = Q()
        return self.filter(dist_q, Q(calendarrelation__object_id=obj.id, calendarrelation__content_type=ct))

class Calendar(models.Model):
    '''
    This is for grouping events so that batch relations can be made to all
    events.  An example would be a project calendar.

    name: the name of the calendar
    events: all the events contained within the calendar.
    >>> calendar = Calendar(name = 'Test Calendar')
    >>> calendar.save()
    >>> data = {
    ...         'title': 'Recent Event',
    ...         'start': _datetime.datetime(2008, 1, 5, 0, 0),
    ...         'end': _datetime.datetime(2008, 1, 10, 0, 0)
    ...        }
    >>> event = Event(**data)
    >>> event.save()
    >>> calendar.events.add(event)
    >>> data = {
    ...         'title': 'Upcoming Event',
    ...         'start': _datetime.datetime(2008, 1, 1, 0, 0),
    ...         'end': _datetime.datetime(2008, 1, 4, 0, 0)
    ...        }
    >>> event = Event(**data)
    >>> event.save()
    >>> calendar.events.add(event)
    >>> data = {
    ...         'title': 'Current Event',
    ...         'start': _datetime.datetime(2008, 1, 3),
    ...         'end': _datetime.datetime(2008, 1, 6)
    ...        }
    >>> event = Event(**data)
    >>> event.save()
    >>> calendar.events.add(event)
    '''

    name = TruncCharField(_("name"), max_length = 200)
    slug = models.SlugField(_("slug"),max_length = 200)
    events = models.ManyToManyField(Event, verbose_name=_("events"), blank=True, null=True)
    timezone = models.CharField(max_length=64, default=settings.TIME_ZONE)

    objects = CalendarManager()

    class Meta:
        verbose_name = _('calendar')
        verbose_name_plural = _('calendar')

    def __unicode__(self):
        return self.name

    def create_relation(self, obj, distinction = None, inheritable = True):
        """
        Creates a CalendarRelation between self and obj.

        if Inheritable is set to true this relation will cascade to all events
        related to this calendar.
        """
        CalendarRelation.objects.create_relation(self, obj, distinction, inheritable)

    def get_recent_events(self, amount=5, in_datetime = now()):
        # TODO: in_datetime isn't used, should we fix this?
        """
        This shortcut function allows you to get events that have started
        recently.

        amount is the amount of events you want in the queryset. The default is
        5.

        in_datetime is the datetime you want to check against.  It defaults to
        now()
        """
        return self.events.order_by('-start').filter(start__lt=now())[:amount]

    def get_upcoming_events(self, amount=5, in_datetime = now()):
        # TODO: in_datetime isn't used, should we fix this?
        """
        This shortcut function allows you to get events that will start soon.

        amount is the amount of events you want in the queryset. The default is
        5.

        in_datetime is the datetime you want to check against.  It defaults to
        now()
        """
        return self.events.order_by('start').filter(start__gt=now())[:amount]

    def get_absolute_url(self):
        return reverse('s_calendar', args=[self.slug])

    def add_event_url(self):
        return reverse('s_create_event_in_calendar', args=[self.slug])

    def get_month(self, date=None):
        date = date or self.local_now()
        return Month(self.events.all(), date)

    def get_timezone(self):
        return timezone_from_timezonestr(self.timezone)

    def local_now(self):
        return now().astimezone(self.get_timezone())

class CalendarRelationManager(models.Manager):
    def create_relation(self, calendar, content_object, distinction=None, inheritable=True):
        """
        Creates a relation between calendar and content_object.
        See CalendarRelation for help on distinction and inheritable
        """
        ct = ContentType.objects.get_for_model(type(content_object))
        object_id = content_object.id
        cr = CalendarRelation(
            content_type = ct,
            object_id = object_id,
            calendar = calendar,
            distinction = distinction,
            content_object = content_object
        )
        cr.save()
        return cr

class CalendarRelation(models.Model):
    '''
    This is for relating data to a Calendar, and possible all of the events for
    that calendar, there is also a distinction, so that the same type or kind of
    data can be related in different ways.  A good example would be, if you have
    calendars that are only visible by certain users, you could create a
    relation between calendars and users, with the distinction of 'visibility',
    or 'ownership'.  If inheritable is set to true, all the events for this
    calendar will inherit this relation.

    calendar: a foreign key relation to a Calendar object.
    content_type: a foreign key relation to ContentType of the generic object
    object_id: the id of the generic object
    content_object: the generic foreign key to the generic object
    distinction: a string representing a distinction of the relation, User could
    have a 'veiwer' relation and an 'owner' relation for example.
    inheritable: a boolean that decides if events of the calendar should also
    inherit this relation

    DISCLAIMER: while this model is a nice out of the box feature to have, it
    may not scale well.  If you use this, keep that in mind.
    '''

    calendar = models.ForeignKey(Calendar, verbose_name=_("calendar"))
    content_type = models.ForeignKey(ContentType)
    object_id = models.IntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    distinction = models.CharField(_("distinction"), max_length = 24, null=True)
    inheritable = models.NullBooleanField(_("inheritable"), default=True)

    objects = CalendarRelationManager()

    class Meta:
        verbose_name = _('calendar relation')
        verbose_name_plural = _('calendar relations')

    def __unicode__(self):
        return '%s - %s' %(self.calendar, self.content_object)
