try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, RequestContext, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import sys, os, pprint
from utils import *
from django.core import serializers
from assist import mail
from assist.models import *
from models import *
import traceback

@global_def
def signup_twitter(request, twitteruser_slug=None):
        
    try:
        tu = TwitterUser.objects.get(slug=twitteruser_slug)
        if not tu.is_complete:
            tu.populate_from_twitter()
        
        return HttpResponse("Twitteruser: %s ID: %s" % (tu.screen_name, tu.user_id))
    except:
        return HttpResponse("error")
    




