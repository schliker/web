"""
Convert the contents of an SVN repo into a downloadable plugin.
"""

from django.conf import settings
import os, sys, tempfile, compileall, zipfile, StringIO, json, shutil
import pdb, traceback
from utils import *
from models import *


# Required stuff for plugins
MANIFEST_FILE = 'manifest.json'
MANIFEST_REQUIRED_FIELDS = [
    'base_name',
    'name',
    'version',
    'description',
    'vendor',
    'target',
    'target_version_min',
    'target_version_max',
    'main_class',
    'vendor_url',
    'icon',
    'icon_small_ico',           # small .ico for the windows
    'icon_small_png',           # small .png for the newsfeed
    'sync_from_external',       # True if this plugin gets data from some external service and saves it in the local DB
    'sync_to_external',         # True if this plugin reads the local DB and sends data to some external service
]

RE_PLUGIN_BASENAME = re.compile(r'^[a-z0-9][a-z0-9_]*_plugin$')
RE_PLUGIN_VERSION = re.compile(r'^(\d+\.)*\d+[a-z]*$')
RE_PNG_FILE = re.compile(r'^(([a-zA-Z0-9_-])+/)*(([a-zA-Z0-9_-])+)\.png$')

class Packager(object):
    
    def _remove_files(self, dir, remove_pattern):
        if dir[-1] == os.sep: dir = dir[:-1]
        files = os.listdir(dir)
        for file in files:
            if file == '.' or file == '..': continue
            path = os.path.join(dir, file)
            if os.path.isdir(path):
                self._remove_files(path, remove_pattern)
            else:
                if re.match(remove_pattern, file):
                    os.remove(path)
                    
    def _zip_dir(self, start_path, archive, prefix=None, exclude_pattern=None, path=None):
        if path is None:
            path = start_path
        paths = os.listdir(path)
        for filename in paths:
            p = os.path.join(path, filename)
            if os.path.isdir(p): # Recursive case
                self._zip_dir(start_path, archive, prefix=prefix, exclude_pattern=exclude_pattern, path=p)
            else:
                if (exclude_pattern is None) or (not re.match(exclude_pattern, p)):
                    archive_name = os.path.relpath(p, start_path)
                    if prefix:
                        archive_name = os.path.join(prefix, archive_name)
                    archive.write(p, archive_name)          # Write the file to the zipfile
        return
    
    def _get_manifest(self, tmp_path):
        
        try:
            f = open(os.path.join(tmp_path, MANIFEST_FILE), 'rb')
            manifest = json.load(f)
            f.close()
        except:
            return None             # no (valid) manifest file
        
        if not all(field in manifest for field in MANIFEST_REQUIRED_FIELDS):
            return None
        
        return manifest
    
        
    def package_from_svn(self, base_name, svn_revision, debug_source_path=None):
        """
        Create a zip file from an SVN repo of a plugin.
        """
        log("*******  Starting packaging for %s (revision: %s)" % (base_name, svn_revision))
        
        base_name = base_name.split('/')[-1]            # We can accept a full path, but ignore all parts except the last
        if not RE_PLUGIN_BASENAME.match(base_name):         # paranoia
            raise Exception("base_name ", base_name, " is invalid")
        
        if debug_source_path:
            # For debugging, just get the source from a given folder.
            tmp_path = debug_source_path
        else:
            tmp_path = tempfile.mkdtemp()
            # Export the plugin source to our temp folder
            repo_path = os.path.join(settings.DESKTOP_PACKAGE_SVN_ROOT, base_name)
            os.system('svn export --force -r %d %s %s' % (svn_revision, repo_path, tmp_path))
            
        manifest = self._get_manifest(tmp_path)
        
        if not manifest:
            raise Exception("base_name ", base_name, " is missing manifest file")
        
        if manifest['base_name'] != base_name:
            raise Exception("base_name ", base_name, " doesn't match manifest base_name", manifest['base_name'])
        
        version = manifest['version']
        if not RE_PLUGIN_VERSION.match(version):            # paranoia
            raise Exception("invalid version in manifest:", version)
        
        log("Verson: %s" % version)
        
        icon = manifest['icon']
        if not RE_PNG_FILE.match(icon):
            raise Exception("invalid PNG filename in manifest:", icon)
        
        if manifest.get('development', False):
            log("Not creating package because development=True")
            return
            
        # Remove any binary files, we're going to byte-compile the pyc files ourselves
        self._remove_files(tmp_path, r'(?i).*\.(so|dll|pyc|pyo|exe|bat|sh)$')
        
        # Create .pyc from .py
        #compileall.compile_dir(tmp_path)        # enable this when we have ability to handle .pyc downloads
        
        # Zip it up for distribution
        s = StringIO.StringIO()
        archive = zipfile.ZipFile(s, 'w', zipfile.ZIP_DEFLATED)
        #self._zip_dir(tmp_path, archive, exclude_pattern=r'(?i)(\..*|.*\.(py))$', prefix=None)    # enable this when we have ability to handle .pyc downloads
        self._zip_dir(tmp_path, archive, exclude_pattern=r'(?i)(\..*)$', prefix=None)
        archive.close()
                
        # Write the zip to the package directory
        s.seek(0)
        package_filename = '%s.%s.%s' % (base_name, version, settings.DESKTOP_PACKAGE_EXTENSION)
        f = open(os.path.join(settings.DESKTOP_PACKAGE_DIR, package_filename), 'wb')
        f.write(s.read())
        f.close()
        
        # Copy the icon file to the plugin directory
        icon_filename = '%s.%s.icon.png' % (base_name, version)
        shutil.copyfile(os.path.join(tmp_path, icon), os.path.join(settings.DESKTOP_PACKAGE_DIR, icon_filename))
        
        # Get or create the plugin object
        plugin, created = Plugin.objects.get_or_create(base_name=base_name)
        if created:
            plugin.status = Plugin.STATUS_BETA
        plugin.name = manifest['name']
        plugin.description = manifest['description']
        plugin.vendor = manifest['vendor']
        plugin.vendor_url = manifest['vendor_url']
        plugin.target = manifest['target']
        #plugin.required_install = manifest.get('required_install', False)       # this field isn't required
        # date_created automatically filled in
        plugin.date_updated = now()
        plugin.save()
        
        log("Updated plugin: %s" % (manifest['name']))
        
        # Create the plugin revision object
        
        log("Creating new revision")
        plugin_revision, created = PluginRevision.objects.get_or_create(plugin=plugin, version=version)
        plugin_revision.slug = create_slug(length=20)
        plugin_revision.status = PluginRevision.STATUS_ACTIVE
        plugin_revision.filename = package_filename
        log("plugin_revision.filename = package_filename = %s" % package_filename)
        plugin_revision.icon_filename = icon_filename
        plugin_revision.description = manifest.get('version_description', '')
        plugin_revision.target_version_min = manifest['target_version_min']
        plugin_revision.target_version_max = manifest['target_version_max']
        plugin_revision.date_created = now()
        plugin_revision.svn_revision = svn_revision
        log("plugin_revision.svn_revision = svn_revision = %s" % svn_revision)
        plugin_revision.save()
        
        log("*******  Finished packaging for %s (revision: %s)" % (base_name, svn_revision))
        
        