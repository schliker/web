# Create your views here.

from models import *
from utils import *
from django.contrib.syndication.feeds import Feed


def connectors_view(request):
    ''' Listing of all apps - located at /connectors/ '''
    apps = Plugin.objects.filter(status=Plugin.STATUS_ACTIVE).order_by('-date_updated')
            
    c = RequestContext(request, dict={
        'apps': apps
    })
    t = loader.get_template('main_connectors.html')
        
    return HttpResponse(t.render(c))    

def connectors_detail_view(request, base_name):
    ''' Details of a specific application /connectors/app_name '''
    
    app = Plugin.objects.get(base_name=base_name)
    
    c = RequestContext(request, dict={
        'app': app
    })
    
    t = loader.get_template('main_connector_details.html')
    
    return HttpResponse(t.render(c))


class LatestConnectors(Feed):
    title = "Connectors"
    link = "/download/connectors/"
    description = ""

    def items(self):
        return Plugin.objects.exclude(status='1').order_by('-date_created')[:10]

