import re
from tokens import *

class Range(Tag):
    @staticmethod
    def scan(tokens):
        for token in tokens:
            token.tag(Range.scan_for_until(token))
            token.tag(Range.scan_for_through(token))
            
    @staticmethod
    def scan_for_until(token):
        if re.match(r"^(to|until)$", token.word):
            return RangeUntil(PD_UNTIL)
        return None    
    
    @staticmethod
    def scan_for_through(token):
        if re.match(r"^(through|thru|-)$", token.word):
            return RangeThrough(PD_THROUGH)
        return None    
    
    def __repr__(self):
        return '_:%s' % str(self.data)

class RangeInfix(Range):
    pass

class RangeUntil(RangeInfix):
    pass
    
class RangeThrough(RangeInfix):
    pass
    
