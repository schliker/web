import re, pdb, traceback
import icalendar        # Don't import * from icalendar, as its names conflict with ours
import icalendar.cal
import icalendar.prop
from utils import *
import uuid
from assist.models import *
from assist.parsedate.tokens import Span
from django.utils.translation import ugettext as _
from icalendar.prop import TypesFactory

def item_from_ics_node(node, method, owner_alias, tzinfo):
    
    def correct_timezone(dt, tzinfo):
        # TODO: Read and parse timezones in .ics files
        
        if dt.tzinfo == icalendar.prop.UTC:
            return dt.replace(tzinfo=pytz.UTC)
        else:
            if hasattr(tzinfo, 'localize'):
                dt = dt.replace(tzinfo=None)
                return tzinfo.localize(dt)
            else:                                           # for timezones stored in the iCal file
                return dt.replace(tzinfo=tzinfo)
        
    try:
        if 'dtstart' in node:
            dtstart = correct_timezone(node.decoded('dtstart'), tzinfo)
        else:
            return None
        if 'dtend' in node:
            dtend = correct_timezone(node.decoded('dtend'), tzinfo)
        else:
            dtend = dtstart
        if 'uid' in node:
            uid = node.decoded('uid')
        else:
            uid = create_uuid() + '@' + settings.NOTED_DOMAIN_MAIN
        if 'dtstamp' in node:
            dtstamp = correct_timezone(node.decoded('dtstamp'), tzinfo)
        else:
            dtstamp = now()
        
        if dtstart > dtend:
            raise Exception('items_from_ics_file: invalid event')
        span = Span(dtstart, dtend)
        span.type = Span.DATE_TIME
                        
        if 'rrule' in node:
            rrule = node.get('rrule')
            if type(rrule) is list:
                if len(rrule) > 1:
                    raise NotImplementedError("items_from_ics_file: can't handle multiple RRULEs per VEVENT")
                else:
                    rrule = rrule[0]
            rrule = str(rrule)
            rule = Rule.get_rule_from_ics_rrule(rrule)
            span.rule = {\
                'frequency':    rule.frequency,
                'name':         rule.name,
                'description':  rule.description,
            }
            span.rule.update(rule.get_params())
                           
        item = {
            'span': span,
            'summary':  node.decoded('summary') if 'summary' in node else '',
            'description': node.decoded('description') if 'description' in node else '',
            'tentative':    False,
            'uid': uid,
            'dtstamp': dtstamp,
            'method': method
            # TODO: Put organizer and attendees here                    
        }
        
        # Ignore organizer for now
#        if 'organizer' in node:
#            o = node['organizer']
#            if hasattr(o, 'params') and 'cn' in o.params:
#                organizer_name = o.params['cn']
#            else:
#                organizer_name = ''
#            organizer_email = str(o).split(':')[-1].lower()
#            if is_assistant_address(organizer_email):
#                queryset = Assistant.objects.filter(email=organizer_email)
#                if queryset.count() == 1:
#                    organizer = queryset.get()
#                else:
#                    organizer = None
#                # TODO: Can the caller accept an assistant as the organizer?
#            else:
#                # TODO: Kill this!
#                organizer, created = Alias.objects.get_or_create_preferred_alias(organizer_name, organizer_email, account=account, include_pending=True)
#        else:
#            organizer = None
        
        if 'attendee' in node:                    
            attendees = []
            ics_attendees = node.get('attendee')
            if type(ics_attendees) is not list:
                ics_attendees = [ics_attendees]
            for a in ics_attendees:
                if hasattr(a, 'params') and 'cn' in a.params:
                    attendee_name = a.params['cn']
                else:
                    attendee_name = ''
                attendee_email = str(a).split(':')[-1].lower()
                if is_ignorable_from_address(ADDR_SPACE_RFC822, attendee_email):
                    continue
                if hasattr(a, 'params') and 'partstat' in a.params:
                    attendee_partstat = a.params['partstat'].upper()
                else:
                    attendee_partstat = ''
                attendee_contact, created = Contact.objects.get_or_create_smart(name=attendee_name, email=attendee_email, viewing_alias=owner_alias)
                attendees.append((attendee_contact, attendee_partstat))
        else:
            attendees = []
            
        return (item, attendees)
    except:
        return None
          

    
def items_from_ics_file(s, default_tzinfo, owner_alias=None):
    """
    Parse an ICS file, return a list of tuples representing event items:
    (item, organizer, attendees)
    """
        
    try:                    
        result = []
        tzinfo = default_tzinfo
        cal = icalendar.Calendar.from_string(s)
        method = cal.decoded('method').upper() if 'method' in cal else None
        
        for c in cal.subcomponents:
            if c.name == 'VTIMEZONE':
                tz_ical = tz.tzical(StringIO.StringIO(str(s)))
                try:
                    tzinfo = tz_ical.get()
                except:
                    log("Warning: cannot get timezone from .ics file", s)
                
        for c in cal.subcomponents:
            if c.name == 'VEVENT':
                item_tuple = item_from_ics_node(c, method, owner_alias, tzinfo=tzinfo)
                if item_tuple:
                    item_tuple[0]['ics'] = s
                    result.append(item_tuple)
                               
        if not result:
            # Novell Groupwise puts its reply info directly under the root node, without a VEVENT
            item_tuple = item_from_ics_node(cal, method, owner_alias, tzinfo=tzinfo)
            if item_tuple:
                result.append(item_tuple)
        return result
        
    except:
        log(traceback.format_exc())
        log("invalid .ics file")
        return []


def reply_uids_from_ics_file(s):
    """
    Parse an ICS file, return just the UIDs of its events if this is a REPLY:
    (item, organizer, attendees)
    """
        
    try:            
        result = []
        cal = icalendar.Calendar.from_string(s)
        method = cal.decoded('method').upper() if 'method' in cal else None
        if method == 'REPLY':
            for c in cal.subcomponents:
                if c.name == 'VEVENT':   
                    if 'uid' in c:
                        uid = c.decoded('uid')
                        result.append(uid)
        return result
        
    except:
        log(traceback.format_exc())
        log("invalid .ics file")
        return []    
    
# From http://cc.oulu.fi/~jarioksa/ical/icaloutlook.html
TIMEZONE_GMT = \
"""BEGIN:VTIMEZONE
TZID:GMT
BEGIN:STANDARD
DTSTART:20071028T010000
TZOFFSETTO:+0000
TZOFFSETFROM:+0000
END:STANDARD
END:VTIMEZONE
"""   

# TODO: Handle unicode names and descriptions, etc. in .ics
# To do this, we'd need two things:
#  - iCalendar parser.py, line 202 should say str(value)  
#  - base64 encode .ics attachments that we send out
 
def ics_from_events(events, to=None, method=None, description=None):

    def vcaladdr(contact):
        "This handles unicode encoding"
        v = icalendar.vCalAddress('MAILTO:%s' % contact.email)
        name = contact.get_full_name()
        if name and all(ord(c) < 128 for c in name):        # We don't handle Unicode names yet (see above)
            v.params['cn'] = icalendar.vText(name)    
        return v

    if type(events) is Event:
        if method is None:
            if events.status == EVENT_STATUS_CANCELED:
                method = 'CANCEL'
            else:
                method = 'REQUEST'
        events = [events]
    else:
        if method is None:
            method = 'PUBLISH'
               
    cal = icalendar.Calendar()
    cal.add('prodid', '-//Noted Inc.')
    cal.add('version', '2.0')
    cal.add('method', method)
    cal.add('calscale', 'GREGORIAN')
    
    # Always use GMT for our timezones, as (apparently) Outlook doesn't accept anything else
    #  according to http://cc.oulu.fi/~jarioksa/ical/icaloutlook.html
    standard = icalendar.cal.Component()
    standard.name = "STANDARD"
    standard.add('dtstart', '20071028T010000', encode=0)
    standard.add('tzoffsetto', '+0000', encode=0)
    standard.add('tzoffsetfrom', '+0000', encode=0)
    gmt = icalendar.Timezone()
    gmt.add('tzid', 'GMT')
    gmt.add_component(standard)
    cal.add_component(gmt)
    
    for event in events:
        desc = description if description else event.title_long
        attendees = list(event.original_email.contacts.all())       # TODO: Only active ones
        attendees = uniqify(attendees)
        if event.status == EVENT_STATUS_FOLLOWUP_REMINDER:
            summary = _('Follow-up: ') + ','.join([c.get_tag_or_name_for_display(to) for c in attendees if c != event.creator.contact])
        else:
            summary = ','.join([c.get_tag_or_name_for_display(to) for c in attendees if c != to]) or ellipsize(desc)
        # summary += ' ' + ellipsize(event.title_long)
        if event.phone:
            summary += ' ' + event.phone
        
        e = icalendar.Event()           # Not to be confused with models.Event
        e.add('summary', summary)
        e.add('description', desc)
        
        # Replace tzinfo with None for event.start and event.end, so that 
        #  icalendar doesn't stick a "Z" at the end of dtstart and dtend.
        #  This is necessary because the timezone is specified elsewhere
        #  in the .ics as GMT (MS Entourage gets confused otherwise?)
        dtstart = icalendar.vDDDTypes(event.start.replace(tzinfo=None))
        dtstart.params = icalendar.parser.Parameters()
        dtstart.params['tzid'] = 'GMT'
        dtend = icalendar.vDDDTypes(event.end.replace(tzinfo=None))
        dtend.params = icalendar.parser.Parameters()
        dtend.params['tzid'] = 'GMT'
                
        e['dtstart'] = dtstart
        e['dtend'] = dtend
        
        # Set the DTSTAMP
        # Special handling needed - newer versions of icalendar add "VALUE=DATE",
        #  which breaks Gmail! Remove it manually.
        dtstamp = event.dtstamp or now()
        dt = TypesFactory()['date-time'](dtstamp)
        try:
            del dt.params['value']
        except:
            pass
        e['dtstamp'] = dt
        
        e['uid'] = event.uid
        
        if event.rule:
            e.add('rrule', icalendar.vRecur.from_ical(event.rule.get_ics_rrule()))
    
        assistant = event.owner_account.get_assistant()
        from_contact = event.original_email.from_contact
        organizer = event.original_email.owner_alias.contact                     # Should be creator, maybe?
        attendees = [c for c in attendees if c != organizer]

        if organizer and (organizer != to):
            # In mail to the event owner, list Cc as the event organizer.
            # This way, if you accept/decline the event in Outlook it goes to Cc
            # (otherwise Outlook won't send an event response).
            e['organizer'] = vcaladdr(organizer)
        elif assistant:
            e['organizer'] = vcaladdr(assistant)
        
        if from_contact:
            attendees.append(from_contact)
        for attendee in attendees:
            if attendee.email:           # TODO: Is there an official way to describe contacts w/o email addresses?
                a = vcaladdr(attendee)
                if (attendee == from_contact) and (to != from_contact):
                    a.params['role'] = icalendar.vText('CHAIR')
                    a.params['partstat'] = icalendar.vText('ACCEPTED')
                else:
                    a.params['role'] = icalendar.vText('OPT-PARTICIPANT')
                    if method == 'REQUEST':
                        a.params['partstat'] = icalendar.vText('NEEDS-ACTION')
                        a.params['rsvp'] = icalendar.vText('TRUE')
                e.add('attendee', a, encode=0)
        
        cal.add_component(e)

    if events:
        e = events[0]
        name = strip_nonalpha(e.original_email.owner_alias.contact.first_name)
        filename = name if name else 'Invite'
        filename += events[0].start.strftime('.%m-%d.%H%M.ics')
    else:
        filename = 'Noted.ics'
    
    return filename, str(cal), method 


    
    