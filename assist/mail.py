"""
Tools for sending email.
"""

import mimetypes
import os
import smtplib
import copy
import socket
import pdb, traceback
import time as timemodule
import random, string
from utils import *
import assist.parsedate.tokens
import html2plaintext
from django.template import Context, loader, Template
from email import Charset, Encoders
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.Header import Header, decode_header
from email.Utils import formatdate, parseaddr, formataddr, parsedate_tz, mktime_tz
from django.utils.translation import ugettext as _
from models import *

from django.conf import settings
from django.utils.encoding import smart_str, force_unicode

# Don't BASE64-encode UTF-8 messages so that we avoid unwanted attention from
# some spam filters.
Charset.add_charset('utf-8', Charset.SHORTEST, Charset.QP, 'utf-8')

# Default MIME type to use on attachments (if it is not explicitly given
# and cannot be guessed).
DEFAULT_ATTACHMENT_MIME_TYPE = 'application/octet-stream'

# Cache the hostname, but do it lazily: socket.getfqdn() can take a couple of
# seconds, which slows down the restart of the server.
class CachedDnsName(object):
    def __str__(self):
        return self.get_fqdn()

    def get_fqdn(self):
        if not hasattr(self, '_fqdn'):
            self._fqdn = socket.getfqdn()
        return self._fqdn

DNS_NAME = CachedDnsName()

## Copied from Python standard library, with the following modifications:
## * Used cached hostname for performance.
## * Added try/except to support lack of getpid() in Jython (#5496).
#def make_msgid(idstring=None):
#    """Returns a string suitable for RFC 2822 compliant Message-ID, e.g:
#
#    <20020201195627.33539.96671@nightshade.la.mastaler.com>
#
#    Optional idstring if given is a string used to strengthen the
#    uniqueness of the message id.
#    """
#    timeval = timemodule.time()
#    utcdate = timemodule.strftime('%Y%m%d%H%M%S', timemodule.gmtime(timeval))
#    try:
#        pid = os.getpid()
#    except AttributeError:
#        # No getpid() in Jython, for example.
#        pid = 1
#    randint = random.randrange(100000)
#    if idstring is None:
#        idstring = ''
#    else:
#        idstring = '.' + idstring
#    idhost = DNS_NAME
#    msgid = '<%s.%s.%s%s@%s>' % (utcdate, pid, randint, idstring, idhost)
#    return msgid

class BadHeaderError(ValueError):
    pass

def forbid_multi_line_headers(name, val):
    """Forbids multi-line headers, to prevent header injection."""
    val = force_unicode(val)
    if '\n' in val or '\r' in val:
        raise BadHeaderError("Header values can't contain newlines (got %r for header %r)" % (val, name))
    try:
        val = val.encode('ascii')
    except UnicodeEncodeError:
        if name.lower() in ('to', 'from', 'cc'):
            result = []
            for item in val.split(', '):
                nm, addr = parseaddr(item)          # We don't need unicode decoding here, I think [eg]
                nm = str(Header(nm, settings.DEFAULT_CHARSET))
                result.append(formataddr((nm, str(addr))))
            val = ', '.join(result)
        else:
            val = Header(val, settings.DEFAULT_CHARSET)
    else:
        if name.lower() == 'subject':
            val = Header(val)
    return name, val

class SafeMIMEText(MIMEText):
    def __setitem__(self, name, val):
        name, val = forbid_multi_line_headers(name, val)
        MIMEText.__setitem__(self, name, val)

class SafeMIMEMultipart(MIMEMultipart):
    def __setitem__(self, name, val):
        name, val = forbid_multi_line_headers(name, val)
        MIMEMultipart.__setitem__(self, name, val)

class SMTPConnection(object):
    """
    A wrapper that manages the SMTP network connection.
    """

    def __init__(self, host=None, port=None, username=None, password=None,
                 use_tls=None, fail_silently=False):
        self.use_tls = (use_tls is not None) and use_tls or settings.EMAIL_USE_TLS
        self.host = host or settings.EMAIL_HOST
        if port:
            self.port = port
        elif self.use_tls:
            self.port = 465
        else:
            self.port = 25
        self.username = username and username.encode('utf-8')    # see http://trac.edgewall.org/ticket/8083
        self.password = password and password.encode('utf-8')    # see http://trac.edgewall.org/ticket/8083
        self.fail_silently = fail_silently
        self.connection = None

    def open(self):
        """
        Ensures we have a connection to the email server. Returns whether or
        not a new connection was required (True or False).
        """
        if self.connection:
            # Nothing to do if the connection is already open.
            log("SMTPConnection.open: already open, returning")
            return False
        try:
            log("SMTPConnection.open ...")
            if settings.DISABLE_SMTP_EMAILS:
                log('disabled smtp emails, via settings.DISABLE_SMTP_EMAILS = True')
                return True
            # If local_hostname is not specified, socket.getfqdn() gets used.
            # For performance, we use the cached FQDN for local_hostname.
            self.connection = smtplib.SMTP(self.host, self.port,
                local_hostname=DNS_NAME.get_fqdn(), timeout=HOWLONG_SMTP_TIMEOUT)
            if self.use_tls:
                self.connection.ehlo()
                self.connection.starttls()
                self.connection.ehlo()
            if self.username and self.password:
                self.connection.login(self.username, self.password)
            log("SMTPConnection.open: success!")
            return True
        except:
            log("SMTPConnection.open: failure: ", traceback.format_exc())
            if not self.fail_silently:
                raise

    def close(self):
        """Closes the connection to the email server."""
        log("SMTPConnection.close")
        try:
            try:
                self.connection.quit()
            except socket.sslerror:
                # This happens when calling quit() on a TLS connection
                # sometimes.
                self.connection.close()
            except:
                if self.fail_silently:
                    return
                raise
        finally:
            self.connection = None

    def send_messages(self, email_messages):
        """
        Sends one or more EmailMessageDjango objects and returns the number of email
        messages sent.
        """
        log("SMTPConnection.send_messages: ", [e.recipients() for e in email_messages])

        if not email_messages:
            return
        new_conn_created = self.open()
        if not self.connection:
            # We failed silently on open(). Trying to send would be pointless.
            return
        num_sent = 0
        for message in email_messages:
            sent = self._send(message)
            if sent:
                num_sent += 1
        if new_conn_created:
            self.close()
        return num_sent

    def _send(self, email_message):
        """A helper method that does the actual sending."""
        if not email_message.recipients():
            return False
        try:
            self.connection.sendmail(email_message.from_email,
                    email_message.recipients(),
                    email_message.message().as_string())
        except:
            if not self.fail_silently:
                raise
            return False
        return True

class EmailMessageDjango(object):
    """
    A container for email information.
    """
    content_subtype = 'plain'
    multipart_subtype = 'mixed'
    encoding = None     # None => use settings default

    def __init__(self, subject='', body='', from_email=None, to=None, bcc=None,
            connection=None, attachments=None, headers=None, noted=None, ics=None):
        """
        Initialize a single email message (which can be sent to multiple
        recipients).

        All strings used to create the message can be unicode strings (or UTF-8
        bytestrings). The SafeMIMEText class will handle any necessary encoding
        conversions.
        """
        if to:
            assert not isinstance(to, basestring), '"to" argument must be a list or tuple'
            self.to = list(to)
        else:
            self.to = []

        if bcc:
            assert not isinstance(bcc, basestring), '"bcc" argument must be a list or tuple'
            self.bcc = list(bcc)
        else:
            self.bcc = []
        self.from_email = from_email or settings.DEFAULT_FROM_EMAIL
        self.subject = subject
        self.body = body
        self.attachments = attachments or []
        self.ics = ics or []
        self.extra_headers = headers or {}
        self.connection = connection
        self.noted = noted      # Noted-specific fields [Emil]
        self.message_id = make_msgid()

    def get_connection(self, fail_silently=False):
        if not self.connection:
            self.connection = SMTPConnection(fail_silently=fail_silently)
        return self.connection

    def message(self):
        encoding = self.encoding or settings.DEFAULT_CHARSET
        msg = SafeMIMEText(smart_str(self.body, settings.DEFAULT_CHARSET),
                           self.content_subtype, encoding)
        msgid = self.message_id
        if self.ics:
            for ics in self.ics:
                msg.attach_ics(self._create_ics(ics, as_attachment=False))
        if self.attachments or self.ics:
            body_msg = msg
            msg = SafeMIMEMultipart(_subtype=self.multipart_subtype)
            if self.body:
                msg.attach(body_msg)
            for attachment in self.attachments:
                if isinstance(attachment, MIMEBase):
                    msg.attach(attachment)
                else:
                    msg.attach(self._create_attachment(*attachment))
            for ics in self.ics:
                msg.attach(self._create_ics(ics, as_attachment=True))
                    # msg.attach_ics(self._create_ics(ics, as_attachment=True))

        msg['Subject'] = self.subject
        msg['From'] = self.from_email
        msg['To'] = ', '.join(self.to)
        msg['Date'] = formatdate()

        msg['Message-ID'] = msgid
        for name, value in self.extra_headers.items():
            msg[name] = value

        if self.noted:
            msg['X-Mailer'] = EMAIL_X_MAILER_NOTED

        return msg

    def recipients(self):
        """
        Returns a list of all recipients of the email (includes direct
        addressees as well as Bcc entries).
        """
        return self.to + self.bcc

    def send(self, fail_silently=False):
        """Sends the email message."""
        result = self.get_connection(fail_silently).send_messages([self])

        if result and self.noted:
            msg = self.message()
            e, edata, extras = Email.email_from_rfc822_message(msg)
            e.message_type = self.noted.get('message_type', EMAIL_TYPE_NOTED)
            e.message_subtype = self.noted.get('message_subtype', None)
            e.owner_account = self.noted.get('owner_account', None)
            e.original_email = self.noted.get('original_email', None)
            e.save()
            event = self.noted.get('event', None)
            if event:
                e.original_email = self.noted['event'].original_email
                e.events.add(event)
            e.message_body = edata
            e.save()

        return result

    def attach(self, filename=None, content=None, mimetype=None):
        log ("attach: self.attachments = ", self.attachments)
        """
        Attaches a file with the given filename and content. The filename can
        be omitted (useful for multipart/alternative messages) and the mimetype
        is guessed, if not provided.

        If the first parameter is a MIMEBase subclass it is inserted directly
        into the resulting message attachments.
        """
        if isinstance(filename, MIMEBase):
            assert content == mimetype == None
            self.attachments.append(filename)
        else:
            assert content is not None
            self.attachments.append((filename, content, mimetype))
        log ("attach DONE self.attachments = ", self.attachments)

    def attach_ics(self, filename=None, content=None, method=None):
        log ("attach_ics: self.ics = ", self.ics)
        self.ics.append((filename, content, method))

    def attach_file(self, path, mimetype=None):
        """Attaches a file from the filesystem."""
        filename = os.path.basename(path)
        content = open(path, 'rb').read()
        self.attach(filename, content, mimetype)

    def _create_attachment(self, filename, content, mimetype=None, disposition=None):
        """
        Converts the filename, content, mimetype triple into a MIME attachment
        object.
        """

        log ("_create_attachment")
        if mimetype is None:
            mimetype, _ = mimetypes.guess_type(filename)
            if mimetype is None:
                mimetype = DEFAULT_ATTACHMENT_MIME_TYPE
        basetype, subtype = mimetype.split('/', 1)
        if basetype == 'text':
            attachment = SafeMIMEText(smart_str(content,
                settings.DEFAULT_CHARSET), subtype, settings.DEFAULT_CHARSET)
        else:
            # Encode non-text attachments with base64.
            attachment = MIMEBase(basetype, subtype)
            attachment.set_payload(content)
            Encoders.encode_base64(attachment)

        if disposition is None:
            if mimetype == 'text/plain':
                disposition = 'attachment'
            else:
                disposition = 'inline'

        if filename:
            attachment.add_header('Content-Disposition', disposition,
                                  filename=filename)
        return attachment

    def _create_ics(self, ics, as_attachment=False):
        filename, content, method = ics
        if as_attachment:
            # Don't include the method if it's an attachment
            return self._create_attachment(filename, content, mimetype='application/ics', disposition='attachment')

        log("_create_ics")
        mimetype = 'text/calendar'
        basetype, subtype = mimetype.split('/', 1)
        if method:
            subtype += ';method=%s' % method
        # subtype += ';name="%s"' % filename    # Don't pass the name here, do it in the attachment [eg 7/10]
        if basetype == 'text':
            ics = SafeMIMEText(smart_str(content,
                settings.DEFAULT_CHARSET), subtype, settings.DEFAULT_CHARSET)

        return ics


# Modified EmailMultiAlternatives that allows attachments.
# From: http://code.djangoproject.com/attachment/ticket/9367/mail.py

class EmailMultiAlternatives(EmailMessageDjango):
    def __init__(self, subject='', body='', from_email=None, to=None, bcc=None,
            connection=None, attachments=None, headers=None, ics=None, alternatives=None, noted=None):
        super(EmailMultiAlternatives, self).__init__(subject, body,
                from_email, to, bcc, connection, attachments, headers, noted, ics)
        self.alternatives=alternatives or []

    def message(self):
        encoding = self.encoding or settings.DEFAULT_CHARSET
        msg = SafeMIMEText(smart_str(self.body, settings.DEFAULT_CHARSET),
                           self.content_subtype, encoding)
        if self.alternatives or self.ics:
            body_msg = msg
            msg = SafeMIMEMultipart(_subtype='alternative')
            if self.body:
                msg.attach(body_msg)
            for alternative in self.alternatives:
                if isinstance(alternative, MIMEBase):
                    msg.attach(alternative)
                else:
                    msg.attach(self._create_alternative(*alternative))
            if self.ics:
                for ics in self.ics:
                    msg.attach(self._create_ics(ics, as_attachment=False))
        if self.attachments or self.ics:
            body_msg = msg
            msg = SafeMIMEMultipart(_subtype=self.multipart_subtype)
            if self.body:
                msg.attach(body_msg)
            for attachment in self.attachments:
                if isinstance(attachment, MIMEBase):
                    msg.attach(attachment)
                else:
                    msg.attach(self._create_attachment(*attachment))
            for ics in self.ics:
                msg.attach(self._create_ics(ics, as_attachment=True))
                    # msg.attach_ics(self._create_ics(ics, as_attachment=True))
        msg['Subject'] = self.subject
        msg['From'] = self.from_email
        msg['To'] = ', '.join(self.to)
        msg['Date'] = formatdate()
        msg['Message-ID'] = self.message_id
        for name, value in self.extra_headers.items():
            msg[name] = value

        if self.noted:
            msg['X-Mailer'] = EMAIL_X_MAILER_NOTED

        return msg

    def attach_alternative(self, content=None, mimetype=None):
        """Attach an alternative content representation."""
        assert content is not None
        assert mimetype is not None
        self.alternatives.append((content, mimetype))

    def _create_alternative(self, content, mimetype):
        """
        Converts the content, mimetype pair into a MIME attachment object.
        """
        basetype, subtype = mimetype.split('/', 1)
        if basetype == 'text':
            alternative = SafeMIMEText(smart_str(content,
                settings.DEFAULT_CHARSET), subtype, settings.DEFAULT_CHARSET)
        else:
            alternative = MIMEBase(basetype, subtype)
            alternative.set_payload(content)
            Encoders.encode_base64(attachment)
        return alternative

def send_mail(subject, message, from_email, recipient_list,
              fail_silently=False, auth_user=None, auth_password=None):
    """
    Easy wrapper for sending a single message to a recipient list. All members
    of the recipient list will see the other recipients in the 'To' field.

    If auth_user is None, the EMAIL_HOST_USER setting is used.
    If auth_password is None, the EMAIL_HOST_PASSWORD setting is used.

    Note: The API for this method is frozen. New code wanting to extend the
    functionality should use the EmailMessageDjango class directly.
    """
    connection = SMTPConnection(username=auth_user, password=auth_password,
                                fail_silently=fail_silently)
    return EmailMessageDjango(subject, message, from_email, recipient_list,
                        connection=connection).send()


# Use this to send generic error messages to admins! [eg]
def mail_admins(subject, message, fail_silently=False):
    """Sends a message to the admins, as defined by the ADMINS setting."""
    EmailMessageDjango(settings.EMAIL_SUBJECT_PREFIX + subject, message,
                 settings.SERVER_EMAIL, [a[1] for a in settings.ADMINS],
                 bcc=[settings.ALL_DEBUG_EMAIL],
                 ).send(fail_silently=fail_silently)


# Use this to send generic error messages to admins, using an
#  alternate mail server (cough Gmail cough).
def mail_admins_alternate(subject, message, fail_silently=False):
    """Sends a message to the admins, as defined by the ADMINS setting."""

    try:
        log("mail_admins_alternate, sending from ", settings.ALT_NOTIFY_SERVER_USERNAME)
        connection = SMTPConnection(
            host=settings.ALT_NOTIFY_SERVER_HOST,
            port=settings.ALT_NOTIFY_SERVER_PORT,
            username=settings.ALT_NOTIFY_SERVER_USERNAME,
            password=settings.ALT_NOTIFY_SERVER_PASSWORD,
            use_tls=True,
            fail_silently=fail_silently
        )

        EmailMessageDjango(settings.EMAIL_SUBJECT_PREFIX + subject, message,
            settings.SERVER_EMAIL, [a[1] for a in settings.ADMINS],
            connection=connection
        ).send(fail_silently=fail_silently)
    except:
        log(traceback.format_exc())
        log("mail_admins_alternate failed")


def mail_managers(subject, message, fail_silently=False):
    """Sends a message to the managers, as defined by the MANAGERS setting."""
    EmailMessageDjango(settings.EMAIL_SUBJECT_PREFIX + subject, message,
                 settings.SERVER_EMAIL, [a[1] for a in settings.MANAGERS]
                 ).send(fail_silently=fail_silently)


# ===================
# Noted utility functions for mails

def send_email(template_file, context, subject, from_email, to, reply_to=None, sender=None, request=None, \
    extra_headers=None, noted=None, attachments=None, ics=None, bcc=None, bcc_to_debug=True, content=None, content_type='text/html',\
    alias=None, allow_all_domains=False, mode=None, connection=None):
    try:
        log('send_email: init')

        if mode and 'debug' in mode:
            log ("*** send_email: skipping because of debug mode")
            return

        # Normalize 'to' to a list of (name, addr) tuples
        original_to = copy.copy(to)
        if type(original_to) is list:        # list of (name, addr)
            pass
        elif type(original_to) is tuple:     # one (name, addr) tuple
            original_to = [original_to]
        else:                       # raw address
            original_to = [('', original_to)]

        to = []
        for name, addr in original_to:
            domain = addr.split('@')[-1].lower()
            if ((settings.ALLOWED_SEND_EMAIL_DOMAINS is True) or (domain in settings.ALLOWED_SEND_EMAIL_DOMAINS)) and \
                not (domain in settings.DISALLOWED_SEND_EMAIL_DOMAINS):
                to.append((name, addr))

        if not to:
            log('send_email: avoiding sending to ', original_to)
            return True

        log('send_email__init')
        if request:
            pass # do something with the request object.
            #c.update({'site': Account.get_site(request).domain})
        if content is None:
            c = Context({})
            c.update(context)
            t = loader.get_template('emails/'+template_file)
            content = t.render(c)
        if content_type=='text/html':
            from assist.html2plaintext import html2plaintext
            text_content = html2plaintext(content, for_parsing=False)
            html_content = content
        else:
            text_content = content
            html_content = None

        headers = {}
        if reply_to is not None:
            if type(reply_to) is tuple:
                reply_to = formataddr_unicode(reply_to)
            headers['Reply-To'] = reply_to
        if sender is not None:
            if type(sender) is tuple:
                sender = formataddr_unicode(sender)
            headers['Sender'] = sender
        if extra_headers is not None:
            for key, value in extra_headers.items():
                headers[key] = strip_header_newlines(value)
        if type(from_email) is tuple:
            from_address = from_email[1]
            from_email = formataddr_unicode(from_email)
        else:
            from_address = from_email
        if type(to) is tuple:
            to = [formataddr_unicode(to)]
        elif type(to) is list:
            to = [formataddr_unicode(t) for t in to]
        log("Sending with headers: %s" % str(headers))
        log("Subject: ", subject)
        log("To: ", to)
        log("Noted: ", noted)
        log("From_email: ", from_email)
        if bcc is None:
            bcc = []
        if bcc_to_debug:
            bcc.append(settings.ALL_DEBUG_EMAIL)

        if alias and alias.server_smtp_enabled and from_address==alias.email:
            use_smtp = True
            if connection is None:
                connection = alias.get_smtp_connection(fail_silently=True)
        else:
            connection = None
            use_smtp = False

        msg = EmailMultiAlternatives(subject, text_content, from_email, to, headers=headers, noted=noted, \
            bcc=bcc, connection=connection,
            alternatives=((html_content, 'text/html'),) if html_content else None,
            attachments=attachments, ics=ics)

#        if html_content is not None:
#            msg.attach_alternative(html_content, "text/html")
#        if attachments:
#            for filename, content, mimetype in attachments:
#                msg.attach(filename=filename, content=content, mimetype=mimetype)
#        if ics:
#            for filename, content, method in ics:
#                msg.attach_ics(filename=filename, content=content, method=method)
        try:
            result = msg.send()
            if not result:
                log('send_email: result is ', result)
                log('not raising exception because not allowing send email via boolean in settings.DISABLE_SMTP_EMAILS')
                #raise Exception('send_email: could not send mail')
        except:
            log('send_email: could not send mail [2]')
            raise

        log('__send_email__ sent')

        return msg
    except:
        log(traceback.format_exc())
        return None


def send_debug_mail(to, content, subject='Debug'):
    if settings.DEBUG:
        if to in [t[1] for t in settings.ADMINS] or to==settings.ALL_DEBUG_EMAIL:
            from_email = formataddr_unicode(('Debug', settings.ALL_DEBUG_EMAIL))
            msg = EmailMultiAlternatives(subject, content, from_email, [to],\
                noted={'message_type': EMAIL_TYPE_DEBUG})
            msg.send()

RE_URL_BEGINNING = re.compile(r'https?://\S+$')
RE_FULL_LINE = re.compile(r'^\S{74}$')

def get_content_collapse_urls(content, line_length=74):
    "Some emails (Outlook) have long URLs split across lines; rejoin them."
    # TODO: Is the line length always 74?
    lines = content.split('\n')
    out_lines = []

    while lines:
        line = lines.pop(0)
        if len(line)==line_length and RE_URL_BEGINNING.match(line):
            while lines:
                line_fragment = lines.pop(0)
                if not line_fragment or line_fragment[0].isspace():
                    break
                line += line_fragment
                if not RE_FULL_LINE.match(line_fragment):
                    break
        out_lines.append(line)

    content = '\n'.join(out_lines)
    return content


def get_content_collapse_doublelines(content):
    "Some emails (Windows Mobile?) are double-spaced; make them single-spaced"
    count_double = len(re.findall('\n\n', content))
    count_single = len(re.findall('[^\n]\n[^\n]', content))
    if count_single == 0 or count_double / float(count_single) >= 10:
        content = content.replace('\n\n', '\n')
    return content


def get_all_text_from_message(message, collapse_double=True, collapse_urls=True):
    """Extract all the text sections from a message, concatenate them into
    one (Unicode) string
    Quoted-printable, and charsets like Windows-1252 are decoded
    TODO: HTML mail"""

    content = '\n\n'.join([p.strip() for p in get_parts_from_message(message, 'text/plain')])

    # We don't deal with text/html, but sometimes Outlook sends text/html only that's been converted from text/plain
    if not content:
        content = '\n\n'.join([html2plaintext.html2plaintext(p, for_parsing=True) for p in get_parts_from_message(message, 'text/html')])

    content = content.replace('\r\n', '\n')                 # Convert from Windows line endings

    if collapse_urls and 'Outlook' in message.get('X-Mailer', ''):
        content = get_content_collapse_urls(content)

    if collapse_double:
        content = get_content_collapse_doublelines(content)

    return strip_nonprintable(content.strip())

def remove_quotes(content):
    """
    From the text section of a message, remove anything that looks like a quote from another email
    Extract the quotes themselves, they might be useful later
    """

    lines = content.split('\n')
    main_lines = []
    quote_headers = []
    quote_lines = []
    other_lines = []                                            # For stuff like the Google Groups footers

    mode = 'main'
    i = 0
    while i < len(lines):
        l = lines[i]
        if re.match('^\s*>', l):                                # Old-fashioned quote lines
            quote_lines.append(l.split('>', 1)[1])              # Remove the leading >
            mode = 'quote-bracket'
        elif re.match('(?i).*(wrote:|sent:)\s*\**', l):         # (* at end is from html2plaintext's version of bold text)
            quote_headers.append(l)
            mode = 'quote'
        elif re.match(r'\s*(?i)On.*(,|\/).*at.*:', l) and (i < len(lines)-1) and re.match('(?i).*(wrote:|sent:)', lines[i+1]):
            quote_headers.append(l)
            quote_headers.append(lines[i+1])
            mode = 'quote'
            i += 1
        elif re.match(r'\s*--+\s*\w+ \w+\s*--+', l):                # "--- Forwarded message ---", "-----Mensaje original-----" etc.
            quote_headers.append(l)
            mode = 'headers'
        elif re.match(r'\s*_{5}_*\s*', l):                      # Separator
            if mode == 'quote':
                other_lines.append(l)                           # end of a quote
                mode = 'other'
            else:
                quote_headers.append(l)
        elif re.match('^\s*\*?(Delivered-To|Received|Return-Path|From|To|Date|Sent|Subject):', l):   # The rest of the email is probably quotes after this point
            quote_headers.append(l)
            mode = 'headers'
        elif re.search('[-~]{18}-~[-~]{18}', l):                    # Google groups footer
            other_lines.append(l)
            mode = 'other'
        elif mode == 'main':
            main_lines.append(l)
        elif mode == 'headers':
            if not l.strip():
                mode = 'quote'
            else:
                quote_headers.append(l)
        elif mode in 'quote':
            quote_lines.append(l)
        elif mode in 'quote-bracket':
            if not re.match('^\s*>', l):
                main_lines.append(l)
                mode = 'main'
            else:
                quote_headers.append(l)
        elif mode == 'other':
            other_lines.append(l)
            if re.search('[-~]{18}-~[-~]{18}', l):
                mode = 'main'
        i += 1

    main_content = '\n'.join(main_lines)
    return main_content, '\n'.join(quote_headers), '\n'.join(quote_lines), '\n'.join(other_lines)


FEATURE_STRENGTH_LOW = 1
FEATURE_STRENGTH_MED = 2
FEATURE_STRENGTH_NEG_MED = -2
FEATURE_STRENGTH_HI  = 3

BEGIN_FOOTER_NONE = 0
BEGIN_FOOTER_PROBABLY = 1
BEGIN_FOOTER_HI = 2
BEGIN_FOOTER_MAX = 3

NO_MULTISTRENGTH = 'no_multistrength'

MAX_FOOTER_LINE_LENGTH = 80

class LineFeatures(object):
    def __init__(self, line, first_name=None, last_name=None):
        self.features = []
        self.strength = 0
        self.heat = 0
        self.peak = False
        self.line = line
        self.mangled_line = line
        self.begin_footer_strength = BEGIN_FOOTER_NONE
        self.find_features(first_name=first_name, last_name=last_name)

    def __str__(self):
        return str(self.features) + ',' + str(self.strength)

    def append(self, type, text, strength, scoring=None):
        if scoring == NO_MULTISTRENGTH:
            if not any([f[0]==type for f in self.features]):
                self.strength += strength
        else:
            self.strength += strength
        self.features.append((type, text))

    def find_features(self, first_name=None, last_name=None):

        try:
            # --------- Line-wide features:

            line = self.line

            # quoted content is not part of a footer
            m = re.search(r"^>", line)
            if m:
                self.features, self.strength = [], -0.5
                return

            if len(line) > MAX_FOOTER_LINE_LENGTH:         # Doubtful that any footer line would be this long
                self.features, self.strength = [], -2.3
                return

            m = RE_GOOGLE_CALENDAR_FOOTER.search(line)
            if m:
                self.features, self.strength, self.begin_footer_strength = [('separator', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_HI
                return

            m = RE_SEPARATOR.search(line)
            if m:
                self.features, self.strength, self.begin_footer_strength = [('separator', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_HI
                return

            m = re.search(r"(?i)with tiny keys .*", line)      # "sent via BB with tiny keys"
            if m:
                self.features, self.strength, self.begin_footer_strength = [('tiny_keys', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_HI
                return

            m = RE_CLOSING_HI.search(line)
            if m:
                self.features, self.strength, self.begin_footer_strength = [('closing', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_MAX
                return

#            m = RE_DISCLAIMER.search(line)
#            if m:
#                strength = FEATURE_STRENGTH_MED
#                self.append('disclaimer', line, strength)

            # --------- Individual features

            def repl_email(m):
                addr = m.group(0)
                strength = FEATURE_STRENGTH_LOW
#                if contact and (addr.lower() == contact.email.lower()):
#                    strength = FEATURE_STRENGTH_HI
#                else:
#                    strength = FEATURE_STRENGTH_LOW
                self.append('email', addr, strength, scoring=NO_MULTISTRENGTH)
                return ''
            line = RE_EMAIL_SEARCH.sub(repl_email, line)

            # TODO: What if too many of these in a line?

            def repl_url(m):
                url = m.group(0)
                strength = FEATURE_STRENGTH_LOW
                self.append('url', url, strength, scoring=NO_MULTISTRENGTH)
                return ''
            line = RE_URL.sub(repl_url, line)

            def repl_zip(m):
                zip = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('zip_code', zip, strength, scoring=NO_MULTISTRENGTH)
                return ''
            line = RE_ZIP.sub(repl_zip, line)

            def repl_phone(m):
                phone = m.group('area') + '-' + m.group('prefix') + '-' + m.group('number')
                try:
                    if m.group('extension'):
                        phone = phone + ' x' + m.group('extension')
                except:
                    pass
                strength = FEATURE_STRENGTH_MED
                self.append('phone', phone, strength)
                return ''
            line = RE_PHONE.sub(repl_phone, line)

            # International phone numbers
            def repl_phone2(m):
                phone = '(+%s) %s' % (m.group('country_code'), m.group('number'))
                try:
                    if m.group('extension'):
                        phone = phone + ' x' + m.group('extension')
                except:
                    pass
                strength = FEATURE_STRENGTH_MED
                self.append('phone', phone, strength)
                return ''
            line = RE_PHONE2.sub(repl_phone2, line)

            def repl_device(m):
                device = m.group('device')
                strength = FEATURE_STRENGTH_HI
                self.append('device', device, strength)
                return ''
            line = RE_DEVICE.sub(repl_device, line)

            def repl_company(m):
                company = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('company', company, strength)
                return ''
            line = RE_COMPANY1.sub(repl_company, line)
            line = RE_COMPANY2.sub(repl_company, line)
            line = RE_COMPANY3.sub(repl_company, line)

            def repl_title(m):
                title = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('title', title, strength)
                return ''
            line = RE_TITLE1.sub(repl_title, line)
            line = RE_TITLE2.sub(repl_title, line)

            def repl_address(m):
                address = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('address', address, strength)
                return ''
            line = RE_ADDRESS.sub(repl_address, line)

            def repl_greeting(m):
                greeting = m.group(0)
                strength = FEATURE_STRENGTH_NEG_MED
                self.append('greeting', greeting, strength)
                return ''
            line = RE_GREETING.sub(repl_greeting, line)

            def repl_closing_low(m):
                closing = m.group(0)
                strength = FEATURE_STRENGTH_LOW
                self.append('closing', closing, strength)
                return ''
            line = RE_CLOSING_LOW.sub(repl_closing_low, line)

            def repl_contact_name(m):
                n = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.begin_footer_strength = BEGIN_FOOTER_PROBABLY
                self.append('name', n, strength, scoring=NO_MULTISTRENGTH)
                return ''
            if first_name and last_name:
                line = re.sub(r"(?i)([-=]|\s)*%s\s+%s" % (re.escape(first_name), re.escape(last_name)), repl_contact_name, line)
            if first_name:
                line = re.sub(r"(?i)([-=]|\s)*" + re.escape(first_name), repl_contact_name, line)

            def repl_first_name(m):
                n = m.group(0)
                strength = FEATURE_STRENGTH_LOW
                self.append('name', n, strength, scoring=NO_MULTISTRENGTH)
                return ''
            if first_name:
                line = re.sub(r"(?i)" + re.escape(first_name), repl_first_name, line)

            def repl_name(m):
                n = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('name', n, strength, scoring=NO_MULTISTRENGTH)
                return ''
            line = RE_NAME1.sub(repl_name, line)
            line = RE_NAME2.sub(repl_name, line)

            # line = re.sub(r"\{\w+\}", "", line)
            crap = ''.join([c for c in line if c.isalpha()])        # what remains, that we couldn't parse
            self.strength -= 2.3 * len(crap) / MAX_FOOTER_LINE_LENGTH
#            if len(self.line) > 0:
#                self.strength *= 1 - (len(line)/len(self.line))

        except:
            log("find_footer_features error:", type(line), traceback.format_exc())

        self.mangled_line = line


def calc_footer_heatmap(lf_list):
    length = len(lf_list)
    for i in xrange(length):
        lf_list[i].heat = 0
        for j in xrange(length):
            weighting = 1 if i >= j else 0.25
            lf_list[i].heat += ((lf_list[j].strength)*(0.6**abs(i-j))) * weighting
    return lf_list


def find_footer_range(lf_list):

    log("find_footer_range")
#    log("Scores:")
#    for lf in lf_list:
#        print lf.strength,
#    print
#    log("Heat:")
#    for lf in lf_list:
#        print lf.heat,
#    print

    max_index, max_heat = None, 0
    start, end = None, None

    for i, lf in enumerate(lf_list):
        if lf.begin_footer_strength == BEGIN_FOOTER_MAX:         # Definitely the beginning of a footer -- we're done
            return (i, len(lf_list))
        if lf.heat > max_heat:
            max_index, max_heat = i, lf.heat

    for i, lf in enumerate(lf_list):
        if lf.begin_footer_strength == BEGIN_FOOTER_HI:         # Definitely the beginning of a footer -- we're done
            return (i, len(lf_list))
        if lf.heat > max_heat:
            max_index, max_heat = i, lf.heat

    # Now try to find the actual footer starting from the end of the email
    total_strength = 0

    for i in reversed(range(len(lf_list))):
        lf = lf_list[i]

        log ("begin_footer_strength:", lf.begin_footer_strength)

        if lf.begin_footer_strength == BEGIN_FOOTER_PROBABLY:
            return (i, len(lf_list))

        if lf.heat >= max(1, 0.5*max_heat):
            # Here's a possible starting point.

            start, n_blank, n_crap = i - 1, 0, 0
            while start >= 0 and n_crap <= 2: # and n_blank <= 4

                lf_start = lf_list[start]
                if lf_start.begin_footer_strength == BEGIN_FOOTER_PROBABLY:
                    start -=1
                    break

                if lf_start.line.strip() == '':
                    n_blank += 1
                elif len(lf_start.features) == 0:
                    n_crap += len(''.join(c for c in lf_start.line if c.isalnum()))/80

                if lf_list[start].heat < 0:
                    break
                start -= 1
            start += 1

            if lf_list[start].begin_footer_strength == 0:
                if start > 0 and lf_list[start-1].line.strip() != '':
                    while lf_list[start].line.strip() != '' and start < i:
                        start += 1

            end = len(lf_list)
            break

        total_strength += lf.strength
        if total_strength < -20:                # Too much junk at the end -- not likely that it's a footer
            break

    return (start, end)


def remove_footers(content, first_name=None, last_name=None):
    lines = content.split('\n')
    search_line_offset = max(len(lines)-NUM_FOOTER_LINES_SEARCH, 0)
    search_lines = lines[search_line_offset:]

    lf_list = [LineFeatures(l, first_name=first_name, last_name=last_name) for l in search_lines]

    footer_index_start, footer_index_end = None, None

#    for i, lf in enumerate(lf_list):
#        if lf.strength == BEGIN_FOOTER:
#            footer_index_start, footer_index_end = i, len(lf_list)

    if footer_index_start is None:
        calc_footer_heatmap(lf_list)

#        for lf in lf_list:
#            try:
#                print ">>", "%d,  %3.2f  ->  %3.2f   %s" % (lf.begin_footer_strength, lf.strength, lf.heat, lf.line.encode('utf-8'))
#                print "            ", lf.features
#            except:
#                pass

        footer_index_start, footer_index_end = find_footer_range(lf_list)

    if footer_index_start is not None:
        footer_features = {}
        filtered_content = lines[:search_line_offset+footer_index_start]
        footer = lines[search_line_offset+footer_index_start:search_line_offset+footer_index_end]
        for lf in lf_list[footer_index_start:footer_index_end]:
            for key, value in lf.features:
                if key not in footer_features:
                    footer_features[key] = value
        filtered_content, footer, footer_features = '\n'.join(filtered_content), '\n'.join(footer), footer_features

        if filtered_content.strip():
#            if Alias is not None:
#                log("*************")
#                log(filtered_content)
#                log("&&&&&&&&&&&&&&&")
            return filtered_content, footer, footer_features
        else:
            # The footer can't be the entire email... in this case, say there's no footer
            return content, '', {}
    else:
        # No footer
        return content, '', {}


def strip_subject_outer(subject):
    subject = subject.strip()
    while subject and subject[0] == '[' and subject[-1] == ']':
        subject = subject[1:-1]
    return subject

def is_subject_reply(subject):
    """Return true if subject starts with Re:
    (Note: not Fwd!)"""
    subject = strip_subject_outer(subject)
    if re.match(r"(?i)\s*re(\[\w+\])?:", subject):
        return True
    return False

def is_subject_forward(subject):
    subject = strip_subject_outer(subject)
    if re.match(r"(?i)\s*(fwd?(\[\w+\])?):", subject):
        return True
    return False

def is_subject_reply_or_forward(subject):
    return is_subject_reply(subject) or is_subject_forward(subject)

def is_subject_event_response(subject):
    subject = strip_subject_outer(subject)
    if re.match(r"(?i)\s*((accept|decline|akzept|deklin)\w*(\[\w+\])?):", subject):
        return True
    return False

def strip_subject_reply_or_forward(subject):
    subject = strip_subject_outer(subject)
    while is_subject_reply_or_forward(subject):
        subject = ':'.join(subject.split(':')[1:])
        subject = strip_subject_outer(subject)
    return subject

def extra_headers_for_reply(old_emails):
    """Return the References: and In-Reply-To: fields needed when
    replying to old_email (of type Email).
    See http://www.rfc-editor.org/rfc/rfc5322.txt
    """

    if old_emails is None:
        return {}
    elif type(old_emails) != list:
        old_emails = [old_emails]

    in_reply_to, references = '', ''

    for old_email in old_emails:
        if old_email.message_id:
            in_reply_to += old_email.message_id
            if old_email.references:
                references += old_email.references + old_email.message_id
            elif old_email.in_reply_to:
                references += old_email.references + old_email.message_id
            else:
                references += old_email.message_id

    d = {}
    if in_reply_to:
        d['In-Reply-To'] = strip_header_newlines(in_reply_to)
    if references:
        d['References'] = strip_header_newlines(references)

    return d


def extra_headers_for_forward(old_emails):
    """Return the References: and In-Reply-To: fields needed when
    forwarding old_email (of type Email).
    See http://www.rfc-editor.org/rfc/rfc5322.txt
    """

    if old_emails is None:
        return {}
    elif type(old_emails) != list:
        old_emails = [old_emails]

    in_reply_to, references, noted_forward_id = '', '', ''

    in_reply_to = ''.join([old_email.in_reply_to for old_email in old_emails if old_email.in_reply_to])
    references = ''.join([old_email.references for old_email in old_emails if old_email.references])
    noted_forward_id = ','.join([str(old_email.message_id) for old_email in old_emails if old_email.message_id])

    d = {}
    if in_reply_to:
        d['In-Reply-To'] = strip_header_newlines(in_reply_to)
    if references:
        d['References'] = strip_header_newlines(references)
    if noted_forward_id:
        d['X-Noted-Forward-ID'] = strip_header_newlines(noted_forward_id)

    return d

# ===================================
# Files


# Modified from Python's email.iterators.walk
def walk_message_parts(msg, is_related=False):
    """Walk over the message tree, returning a sequence of tuples:
        (message_part, is_related_flag)
    is_related_flag is true if the message part is a "multipart/related" node or
        is any descendant of a multipart/related node. This lets us detect
        images which are meant to accompany HTML mail, as opposed to attachments,
        so we can ignore them.

    The walk is performed in depth-first order.  This method is a
    generator.
    """

    if msg.get_content_type() == 'multipart/related':
        is_related = True
    yield (msg, is_related)
    if msg.is_multipart():
        for subpart in msg.get_payload():
            for subsubpart in walk_message_parts(subpart, is_related=is_related):
                yield subsubpart

def get_ics_files_from_message(message):

    ics = {}
    for part in message.walk():
        part_type = part.get_content_type()
        if part_type in ICS_MIMETYPES:
            s = part.get_payload(decode=True)   # Don't decode UTF-8 into Unicode; iCalendar wants UTF-8
            m = re.search(r"UID:(\S+)\s", s)
            if m:
                uid = m.group(1)
                ics[uid] = s
    return ics.values()


def email_supports_ics(addr):
    "Tell whether the email address is capable of accepting .ics files"
    if '@' in addr:
        addr = addr.split('@')[-1]
        parts = addr.split('.')
        if len(parts) > 1:
            if parts[-2].lower() in EXCLUDE_ICS_DOMAINS:
                return False
        return True
    return False



def is_allowed_mimetype(mimetype, account=None, disposition=None):
    # disposition=None means don't check the disposition
    # disposition='' means there's no disposition, and do check against this
    if '/' not in mimetype:
        return False
    mimetype = mimetype.split('/')
    maintype, subtype = mimetype[0], mimetype[-1]

    if maintype in ['application', 'image', 'audio']:
        if subtype in ['ico', 'icon', 'x-icon', 'vnd.microsoft.icon']:
            return False
        return True
    if maintype in ['text'] and ((disposition is None) or (disposition.lower() in ['inline', 'attachment'])):
        return True
    return False

def is_part_downloadable_file(part, is_related, message, account=None):
    if part.get_all('Content-ID'):
        return False                    # This file is an embedded part of something else, ignore
    parttype, maintype, subtype = part.get_content_type(), part.get_content_maintype(), part.get_content_subtype()
    params = part.get_params(None)
    if params:
        params = dict([(key.lower(), value) for key, value in params])
    else:
        params = {}
    disposition = part.get_all('Content-Disposition')
    if disposition:
        disposition = disposition[0].split(';')[0]
    # disposition = params.get('Content-Disposition', '')
    # log("content disposition:", disposition)
    if is_related:
        return False
    if account and not is_allowed_mimetype(parttype, account=account, disposition=disposition):
        return False
    name = get_part_filename(part)
    if not name:
        return False
    if RE_CONFIDENTIAL.search(name):
        return False
    if ('.' in name) and (name.lower().split('.')[-1] in ['ics', 'vcf', 'vcs']):
        return False            # Some folks include an .ics as an application/octet-stream or application/ics
    if name.lower() == 'winmail.dat':
        return False
    msgstr = message.as_string()
    if ('<<%s>>' % name) in msgstr:
        return False            # MS Exchange sometimes makes references to files this way, ignore.
    return True


def is_placeholder_downloadable_file(fp, message, account=None):

    if fp.content_id:
        return False                    # This file is an embedded part of something else, ignore
    parttype = '%s/%s' % (fp.mimetype, fp.mimesubtype)
    if account and not is_allowed_mimetype(parttype, account=account):
        return False
    if not fp.name:
        return False
    if RE_CONFIDENTIAL.search(fp.name):
        return False
    if ('.' in fp.name) and (fp.name.lower().split('.')[-1] in ['ics', 'vcf', 'vcs']):
        return False            # Some folks include an .ics as an application/octet-stream or application/ics
    if fp.name.lower() == 'winmail.dat':
        return False
    msgstr = message.as_string()
    if ('<<%s>>' % fp.name) in msgstr:
        return False            # MS Exchange sometimes makes references to files this way, ignore.
    return True



def get_files_from_message(message, email, store=True, account=None, alias=None, file_placeholders=None, viewing_aliases=None):
    """For messages emailed directly to the assistant, this routine creates the File objects and saves the files to the cloud."""

    def setup_file_tokens(email, account, f, viewing_aliases):
        if email.from_contact and email.from_contact.alias and (email.from_contact.alias.account == account):
            ft, created = FileToken.objects.get_or_create_token(email.from_contact.alias, f, type=FILETOKEN_TYPE_FROM)
        if email.crawled_alias and email.crawled_alias.account == account:
            ft, created = FileToken.objects.get_or_create_token(email.crawled_alias, f, type=FILETOKEN_TYPE_CRAWLED)
        extras = email.get_extras()
        if extras:
            for name, addr in extras['tocc_other']:
                alias = Alias.objects.get_preferred_alias_by_email(
                    addr, account=account, include_pending=True, require_account_match=True)
                if alias:
                    ft, created = FileToken.objects.get_or_create_token(alias, f, type=FILETOKEN_TYPE_TOCC)

        if viewing_aliases:
            for v in viewing_aliases:
                ft, created = FileToken.objects.get_or_create_token(v, f, type=FILETOKEN_TYPE_OTHER)

    n = now()
    if email and email.date_sent:
        date = email.date_sent
    else:
        date = n
    datestr = '%s/%s' % (date.month, date.day)
    if viewing_aliases:
        viewing_aliases = uniqify(viewing_aliases)
    em_modified = False

    files = []

    for i, tup in enumerate(walk_message_parts(message)):
        part, is_related = tup
        part_type = part.get_content_type()
        maintype, subtype = part.get_content_maintype(), part.get_content_subtype()
        params = part.get_params(None)
        if params:
            params = dict([(key.lower(), value) for key, value in params])
        else:
            params = {}

        if not is_part_downloadable_file(part, is_related, message, account=account):
            continue

        name = get_part_filename(part)   # If we get here, we know params has a name

        # Get the size and md5 sum of the file
        s = part.get_payload(decode=True)
        size, md5sum = File.calc_file_stats(s)

        # Create new File
        f, created = File(slug=create_slug(length=20)), True
        f.name = name
        f.mimetype = part_type
        if '.' in f.name:
            f.extension = f.name.split('.')[-1]
        f.email = email
        f.date = date

        if store:
            f.store_local(s, size=size, md5sum=md5sum)
        f.save()

        # Make the initial permanent tokens that go with this file
        setup_file_tokens(email, account, f, viewing_aliases)
        files.append((f, created))

    if file_placeholders:
        for fp in file_placeholders:
            # These are descriptions of files that we didn't actually download (e.g. IMAP)

            if not is_placeholder_downloadable_file(fp, message, account=account):
                continue

            # Create new File
            f, created = File(slug=create_slug(length=20)), True
            f.name = fp.name
            f.mimetype = '%s/%s' % (fp.mimetype, fp.mimesubtype)
            if '.' in f.name:
                f.extension = f.name.split('.')[-1]
            f.email = email
            f.mimepath = fp.mimepath
            f.date = date
            f.save()

            # Make the initial permanent tokens that go with this file
            setup_file_tokens(email, account, f, viewing_aliases)
            files.append((f, created))

    return files



def get_parts_from_message(message, mimetypes):
    """Extract all the sections from a message of the given mimetype
    that aren't attachments
    Quoted-printable, and charsets like Windows-1252 are decoded
    TODO: HTML mail"""

    if type(mimetypes) is not list:
        mimetypes = [mimetypes]

    parts = []
    for part, related in walk_message_parts(message):
        if is_part_downloadable_file(part, related, message):
            continue
        part_type = part.get_content_type()
        if part_type in mimetypes:
            s = part.get_payload(decode=True)
            charset = part.get_content_charset()
            if charset:
                try:
                    s = s.decode(charset)
                except:
                    s = ''.join([char for char in s if char in string.printable])    # Hack, but works for now
            else:
                s = ''.join([char for char in s if char in string.printable])
            parts.append(s)

    return parts


# ===================================
# Other Noted utility functions for mails

# def format_email_headersonly_for_display(old_email, tzinfo=None):
#     if not tzinfo:
#         tzinfo = old_email.get_timezone()
#
#     extras = old_email.get_extras()
#     if extras:
#         to = ', '.join([formataddr_unicode(x) for x in extras['to']])
#         cc = ', '.join([formataddr_unicode(x) for x in extras['to']])
#     else:
#         to = formataddr_unicode((old_email.to_name, old_email.to_address))
#         cc = ''
#
#     old_email_formatted = {\
#         'email':        old_email,
#         'subject':      old_email.subject,
#         'message_id':   old_email.message_id,
#         'from':         formataddr_unicode((old_email.from_name, old_email.from_address)),
#         'date':         format_datetime(old_email.date_sent.astimezone(tzinfo), display_today=True, display_past_weekdays=True),
#         'to':           to,
#         'cc':           cc,
#     }
#     return old_email_formatted

def get_content_sections(e, content, contact=None, first_name=None, last_name=None):

    if e.source == Email.SOURCE_TWITTER:
        lines = content.split('\n')
        content, footer = None, None
        for i, l in enumerate(lines):
            if i >= 3 and l.strip() == '--':
                content = '\n'.join(lines[:i-2])
                footer = '\n'.join(lines[i-2:])
        if content is None:
            content = lines[0]
            footer = '\n'.join(lines[1:])
        content, quoted_headers, quoted_lines, other_lines, footer, footer_fields = content, '', '', '', footer, {}
    elif e.source == Email.SOURCE_QUICKBOX:
        content, quoted_headers, quoted_lines, other_lines, footer, footer_fields = content, '', '', '', '', {}
    else:
        content, quoted_headers, quoted_lines, other_lines = remove_quotes(content)
        if contact:
            first_name, last_name = contact.first_name, contact.last_name
        content, footer, footer_fields = remove_footers(content, first_name=first_name, last_name=last_name)

    return content, quoted_headers, quoted_lines, other_lines, footer, footer_fields


def get_content_from_email(e, edata=None, edata_is_rfc822=False, contact=None, content_mode=CONTENT_MODE_ALL):
    if (e.type == Email.TYPE_RFC822) and edata_is_rfc822:
        py_message = email.Parser.Parser().parsestr(str(strip_nonascii(edata)))
        if py_message is None:
            return None, None
        if contact is None:
            contact = e.from_contact
        collapse_double = (e.source not in [Email.SOURCE_QUICKBOX, Email.SOURCE_TWITTER])
        content = get_all_text_from_message(py_message, collapse_double=collapse_double).strip()

        if content_mode == CONTENT_MODE_ALL:
            return content, None
        elif content_mode == CONTENT_MODE_MIN:
            tup = get_content_sections(e, content, contact=contact)
            if tup:
                content = tup[0]
            return content[:MAX_CONTENT_BODY_LENGTH], None
    elif edata:
        return edata, None
    else:
        return e.get_body(), None    # Return the raw body of the message


# Used by the Outlook reply mechanism
def email_as_json(e):
    content = get_content_from_email(e, content_mode=CONTENT_MODE_MIN)[0]
    if content is None:
        raise Exception("email_as_json: cannot retrieve original email")
    content_quoted = '\n'.join(['> ' + line for line in content.split('\n')])

    d = {
         'message_id':      str_or_none(e.message_id),
         'from_name':       e.from_name,
         'from_address':    e.from_address,
         'to_name':         e.to_name,
         'to_address':      e.to_address,
         'subject':         e.subject_normalized,
         'subject_stripped':   strip_subject_reply_or_forward(e.subject_normalized),
         'content':         content,
         'content_quoted':  content_quoted
    }
    return d

def format_email_for_display(e, edata=None, tzinfo=None, include_content=True, content=None, ellipsize_content=False, include_filelinks=False, contact=None):
    if not tzinfo:
        tzinfo = e.get_timezone()

    extras = e.get_extras()
    if extras:
        to = ', '.join([formataddr(tup) for tup in extras['to']])   # DON'T use formataddr_unicode here!
        cc = ', '.join([formataddr(tup) for tup in extras['cc']])   #  This is for display on the webpage!
    else:
        to = formataddr((e.to_name, e.to_address))
        cc = ''

    if include_content:
        if content is None:
            content = get_content_from_email(e, edata=edata, content_mode=CONTENT_MODE_MIN)[0]
        else:
            pass            # use the content that was given to us
    else:
        content = ''
    if ellipsize_content:
        content = ellipsize(content, length_min=240, length_max=270)

    alias = contact.alias if contact else None

    filelinks = []
    if include_filelinks and alias and (alias.status==ALIAS_STATUS_ACTIVE):
        for file in e.get_files():
            token, created = FileToken.objects.get_or_create_token(alias, file)
            filelinks.append({\
                'file':         file,
                'token_slug':   token.slug
            })

    old_email_formatted = {\
        'email':        e,
        'subject':      e.subject_normalized,
        'message_id':   e.message_id,
        'in_reply_to':  e.in_reply_to,
        'from':         formataddr((e.from_name, e.from_address)),  # Don't use formataddr_unicode here!
        'date':         format_datetime(e.date_sent, tzinfo=tzinfo),
        'to':           to,
        'cc':           cc,
        'content':      content,
        # 'content_full': content_full,       # without footers and stuff removed - for SMTP
        'filelinks':    filelinks,
        'contact':      contact
    }
    return old_email_formatted


def find_thread_for_formatting(email, additional_emails=None):
    if additional_emails is None:
        additional_emails = []
    elif type(additional_emails) is not list:
        additional_emails = [additional_emails]
    emails = additional_emails
    if email.events.filter(status=EVENT_STATUS_FOLLOWUP_REMINDER).count() == 0:
        # Don't quote mails that are followup reminders
        for e in email.events.all():
            child_events = e.event_children.all()
            emails += [c.original_email for c in child_events]
        emails += [email]
    emails = uniqify(emails)
    emails.sort(key=lambda e: e.date_sent)
    return emails


def post_tweet(from_tu, alias_tu, contact, content, parent_email=None):
    api = from_tu.get_api()
    tweet = '@%s %s' % (contact.twitter_screen_name, content) if contact else content
    tweet = ellipsize(tweet, length_min=130, length_max=135, length_with_url_max=135)
    if (alias_tu is not None) and (alias_tu != from_tu) and (alias_tu.screen_name):
        tweet_suffix = ' (@%s)' % alias_tu.screen_name
        if len(tweet + tweet_suffix) <= 140:
            tweet += tweet_suffix
    try:
        tweet = tweet.encode('utf-8')
        log("Trying to post to Twitter: ", tweet, ("in reply to %s" % parent_email.message_id) if parent_email else "")
        api.PostUpdate(tweet, in_reply_to_status_id=parent_email.message_id if parent_email else None)
    except:
        log(traceback.format_exc())
        if contact:
            raise NotifyUserException(_('Cannot reply to @%s right now -- Twitter may be down temporarily.') % contact.twitter_screen_name)
        else:
            raise NotifyUserException(_('Cannot send a tweet right now -- Twitter may be down temporarily.'))


def send_forwarded_mail(e, assistant, from_contact, to_contacts, tzinfo=False, is_assistant_for_name=False, content=None, \
                        noted=None, include_filelinks=True, use_smtp_if_possible=False, mode=None):

    log("send_forwarded_mail", e.message_id)

    from twitter.models import TwitterUser

    if type(to_contacts) != list:
        to_contacts = [to_contacts]

    parent_email = e.original_parent

    if parent_email and is_assistant_address(parent_email.from_address):
        parent_email = None                 # don't quote the assistant
    if parent_email and parent_email.contacts.filter(email2contact__type__in=[Email2Contact.TYPE_REF], email2contact__contact__in=to_contacts).count() > 0:
        parent_email = None                 # don't quote mails that are "in ref. to" a person who's going to get this mail!

    use_smtp = bool(use_smtp_if_possible and from_contact.alias and from_contact.alias.server_smtp_enabled)

    for to_contact in to_contacts:

        # TODO: Factor all this out somewhere else, get rid of the multiple emails for replies
        if to_contact.status != Contact.STATUS_ACTIVE:
            if mode:
                mode['refresh_contacts'] = True
            to_contact.status = Contact.STATUS_ACTIVE

        to_contact.update_last_contacted_date(from_contact.alias)
        to_contact.contacted_by_assistant = True
        to_contact.save()

        if parent_email and is_assistant_address(parent_email.from_address):
            parent_email = None             # don't quote the assistant

        email_formatted = format_email_for_display(e, tzinfo=tzinfo, content=content, include_filelinks=include_filelinks, contact=to_contact)
        parent_email_formatted = format_email_for_display(parent_email, tzinfo=tzinfo, content=content, include_filelinks=include_filelinks, contact=to_contact) if parent_email else None
        thread_formatted = remove_nones([parent_email, e])

        c = Context({\
            'from_first_name':          from_contact.first_name if from_contact.first_name else from_contact.email,
            'from_name':                from_contact.get_full_name(),
            #'old_emails':               thread_formatted,
            'old_emails':               uniqify([parent_email]),
            'assistant':                assistant,
            'intro':                    not to_contact.contacted_by_assistant,
            'is_assistant_for_name':    is_assistant_for_name,
            'contact':                  to_contact,
            'site':                     from_contact.alias.get_site() if from_contact.alias else Site.objects.get_current(),
            'email_formatted':          email_formatted,
            'parent_email':             uniqify([parent_email]),
            'parent_email_formatted':   parent_email_formatted
        })

        extra_headers = extra_headers_for_forward([e])

        to = [(to_contact.get_full_name(), to_contact.email)]

        if noted is None:
            noted = {'message_type': EMAIL_TYPE_NOTED,\
                'message_subtype': EMAIL_SUBTYPE_NOTED_CONTACT_REPLY,
                'owner_account': assistant and assistant.owner_account
            }\

        if parent_email:
            subject = _("Re: ") + strip_subject_reply_or_forward(parent_email.subject)
        else:
            subject = strip_subject_reply_or_forward(e.subject)
#            if not is_assistant_for_name:            # Re-enable these if we want forwarding for guests
#                subject = _("Fwd: ") + subject

        log ("use_smtp:", use_smtp)

        send_email('main_forward_smtp.html',    # Re-enable this if we want forwarding for guests: if is_assistant_for_name else 'main_forward_self.html',
            c,
            subject,
            (from_contact.get_full_name(), from_contact.email),
            to,
            extra_headers = extra_headers,
            reply_to = (from_contact.get_full_name(), from_contact.email),
            sender = (assistant.get_full_name(), assistant.email),    # Re-enable this if we want forwarding for guests: or None if it's a guest
            noted=noted,
            alias=from_contact.alias, mode=mode
        )



def send_forwarded_mail_to_incoming(old_email, edata):
    """For messages sent directly to the assistant, forward them to our debugging address
    Assumes edata is the raw rfc822 source of a message!"""

    debug_email = settings.TWITTER_DEBUG_EMAIL if (old_email.type == Email.TYPE_TWITTER) else settings.INCOMING_DEBUG_EMAIL

    if debug_email:
        old_emails_formatted = [format_email_for_display(old_email, edata=edata)]

        c = Context({\
            'old_emails':               old_emails_formatted,
        })

        to = [('Incoming Debug', debug_email)]

        send_email('main_forward_incoming_debug.html', c,
            old_email.subject_normalized,
            (old_email.from_name, old_email.from_address),
            to,
            attachments=[('message.eml', str(edata), 'text/plain')], # can't attach as mimetype message/rfc822 for some reason
            bcc_to_debug=False
        )


# ===================================
# Formatting
