try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import sys, os
from django.core import serializers
from utils import *

def err_500(request):
    c = Context({
    })
    t = loader.get_template('500.html')
    c.update(global_context(request))
    return HttpResponse(t.render(c))
    
def err_404(request):
    c = Context({
    })
    t = loader.get_template('404.html')
    c.update(global_context(request))
    return HttpResponse(t.render(c))