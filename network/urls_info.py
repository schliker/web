from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^down/$', 'network.views_info.down'),
    url(r'^users', 'network.views_info.success'),
    url(r'^order', 'network.views_info.order'),
    url(r'^pay/(?P<service_name>\w+)', 'network.views_info.go'),           # old URL for the go page
    url(r'^register/(?P<service_name>\w+)', 'network.views_info.go', name='register'),
    url(r'^billing', 'network.views_info.billing'),
    url(r'^registered/$', 'network.views_info.payment_complete', name='registration_complete'),
    url(r'^payment_cancel/$', 'network.views_info.payment_cancel', name='payment_cancel'),
    url(r'^terms/(?P<terms_type>\w+)', 'network.views_info.terms'),
    url(r'^prompt/$', 'network.views_info.prompt'),
)
