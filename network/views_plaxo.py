try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, RequestContext, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.core import paginator
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import calendar
import sys, os
from utils import *
from django.core import serializers
from assist.models import *
#from schedule import views
from assist.constants import *
from schedule.periods import *
from assist import mail
from assist import constants
from assist.parsedate.tokens import Span
from twitter.models import *
from remote.models import *
import ajax_feeds
from ajax_feeds import get_widget_manager

from basic.blog.models import *

import traceback, pdb, pprint

def always_error(request):
    raise Exception('always_error')

### blog RSS

from django.contrib.syndication.feeds import Feed


@global_def
def plaxo_callback_view(request):
    log('plaxo_callback_view')
    try:
        t = loader.get_template('plaxo_callback.html')
        c = RequestContext(request)
        return HttpResponse(t.render(c))
    except:
        return HttpResponseRedirect('/')
    