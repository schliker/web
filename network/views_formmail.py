try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, RequestContext, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.core import paginator
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import calendar
import sys, os
from utils import *
from django.core import serializers
from assist.models import *
#from schedule import views
from assist.constants import *
from schedule.periods import *
from assist import mail
from assist import constants
from assist.parsedate.tokens import Span
from twitter.models import *
from remote.models import *
import ajax_feeds

from basic.blog.models import *

import traceback, pdb, pprint


def unsubscribe_view(request):
    try:        
        slug = request.GET['slug']
        mail_id = int(request.GET['mail_id'])
        unsubscribe = bool(int(request.GET.get('unsubscribe', 0)))
        
        email = Email.objects.get(pk=mail_id)
        contact = Contact.objects.get(unsubscribe_slug=slug, owner_account=email.owner_account)
        
        if unsubscribe:
            contact.subscribed = False
            contact.save()
            
        c = RequestContext(request, dict={
            'contact': contact
        })
        
        t = loader.get_template('main_unsubscribe_contact.html')
        return HttpResponse(t.render(c))

    except:
        log('unsubscribe_view fail: ', traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/dash/?unsubscribe_view=fail')
