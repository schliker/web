try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import calendar
import sys, os
from utils import *
from django.core import serializers
from assist.models import *
from schedule import views
from assist.constants import *
from schedule.periods import *
from assist import mail, ics
from assist import constants
import traceback, pdb, pprint


def get_alias_ical(request, subdomain=None, slug_ics=None):
    log("get_alias_ical...")
    try:
        alias = Alias.objects.active().get(slug_ics=slug_ics, account__base_url=subdomain)
        
        # Get all the events for that alias
        events = list(Event.objects.scheduled().filter(original_email__contacts=alias.contact, start__gte=now()-HOWLONG_AGO_ICAL_FEED).distinct())
        
        # Get all the followup reminders for that alias
        events += list(Event.objects.filter(activated=True, guessed=False, status=EVENT_STATUS_FOLLOWUP_REMINDER, start__gte=now()-HOWLONG_AGO_ICAL_FEED, creator=alias))
        
        filename, cal, method = ics.ics_from_events(events, to=alias, method='PUBLISH')

        alias.date_last_accessed_ics = now()
        alias.save()
        
        response = HttpResponse(cal, mimetype='text/calendar')
        response['Content-Disposition'] = 'attachment; filename="%s"' % ('calendar.ics')
        return response
                
    except:
        log('__get_alias_ical__ fail: ', subdomain, slug_ics, traceback.format_exc())
        return HttpResponse('')
    
     
