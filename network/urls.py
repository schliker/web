from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
import network.views_cover
import network.views_info

admin.autodiscover()

''' TODO: setup RSS feeds. Feeds can be used by Masters to see scheduled events
    and syndicate these events to iCal, Gmail, or Outlook, etc.
    
    http://docs.djangoproject.com/en/dev/ref/contrib/syndication/
    
    the following fields dictionary is for specifying RSS feeds.  This can be
    used later in the project.
    
    from django.contrib.syndication.feeds import Feed
    from chicagocrime.models import NewsItem

    class LatestEntries(Feed):
        title = "Chicagocrime.org site news"
        link = "/sitenews/"
        description = "Updates on changes and additions to chicagocrime.org."

        def items(self):
            return NewsItem.objects.order_by('-pub_date')[:5]
    
    class AtomSiteNewsFeed(RssSiteNewsFeed):
        feed_type = Atom1Feed
        subtitle = RssSiteNewsFeed.description

    '''


''' TODO: setup rss/atom feeds 
        
    feeds = {
        #'latest': LatestEntries,
        #'categories': LatestEntriesByCategory,
    }

    (r'^feeds/(?P<url>.*)/$', 'django.contrib.syndication.views.feed',{'feed_dict': feeds}), 
    
    '''

urlpatterns = patterns('',
    (r'^$', 'network.views_cover.default'),
    (r'', include('network.urls_account')),
    
    (r'^main_javascript.js$', network.views_cover.main_javascript_js_view),
    (r'^main_account.js$', network.views_cover.main_account_js_view)
    #(r'^(?P<subdomain>\w+)', 'network.views_cover.account_home'),
)
