try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, RequestContext, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.core import paginator
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re, json, time, random, calendar, sys, os
from datetime import datetime, timedelta
from utils import *
from django.core import serializers
from assist.models import *
#from schedule import views
from assist.constants import *
from schedule.periods import *
from assist import mail
from assist import constants
from assist.parsedate.tokens import Span
from twitter.models import *
from remote.models import *
import ajax_feeds
from ajax_feeds import get_widget_manager
from crawl.models import NotedTaskMeta

from basic.blog.models import * 

import traceback, pdb, pprint

def always_error(request):
    raise Exception('always_error')

### blog RSS

from django.contrib.syndication.feeds import Feed

class LatestEntries(Feed):
    title = "Blog"
    link = "/blog/"
    description = "Blog updates - email and Twitter powered CAL"

    def items(self):
        return Post.objects.exclude(status='1').order_by('-created')[:10] 
    
@global_def
def default(request):
    
    try:          
        log('__init__ group home:')
        log('Now, settings.TEMPLATE_DIRS:', settings.TEMPLATE_DIRS)
        to = get_redirect_param(request)
        agent = get_agent(request)
        if 'alias' in request.GET:
            log('__force_login__', request.GET['alias'])
            alias = Alias.objects.active(include_inactive_accounts=True).get(slug=request.GET['alias'])
            if 'alias' in request.session and request.session['alias'] is not None and request.session['alias'] == alias:
                log('alias %s is already auth' % alias.slug)
                if to:
                    return HttpResponseRedirectAgent(request, to)
                log('default: [1]')
                return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')               
                
            account_user = alias.account_user
            # user = account_user.user
            
            if alias.password_hash:     # user has already set the password, take them to the login page
                t = loader.get_template('main_account_login.html')
                c = RequestContext(request, dict={
                    'email': alias.email,
                    'alias_slug': alias.slug,
                    'redirect_to': redirect_agent_url(agent, to)
                })
                                
                return HttpResponse(t.render(c))
            else:                       # first time, log them in
                log('__force login alias__', alias.email)
                clear_session(request)
                request.session['alias'] = alias
                user = auth.authenticate(user=alias.user)        # dummy authentication, just fill in the 'backend' attribute
                auth.login(request, user)
                log('__force_login__ redirecting because alias is auth')
                if to:
                    return HttpResponseRedirectAgent(request, to)
                return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')
        else:
            alias = request.session['alias']
            if alias:
                account_user = alias.account_user
                return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')
            else:
                account_user = None
    except:
        
        log('__default__: error: Could not grab account account user', traceback.format_exc())
        alias = None
        account_user = None
    
    random_person = random_names(1)[0]
    random_name = random_person[0]
    random_gender = random_person[1]
    
    blog_posts = Post.objects.filter(status='2').order_by('-id')[:3]
        
    logo_account_tuples = list(Account.objects.active().exclude(logo_url=None).exclude(logo_url='').order_by('-logo_weight').values_list('id','logo_weight'))
    featured_account_ids = [t[0] for t in logo_account_tuples[:10]]
    
#    import random
#    while len(featured_account_ids) < 10:
#        if len(logo_account_tuples) == 0:
#            break
#        choice = random.choice(logo_account_tuples)
#        logo_account_ids.remove(choice)
#        featured_account_ids.append(choice)  
    featured_accounts = Account.objects.filter(id__in=featured_account_ids).values('logo_url', 'logo_weight', 'name').order_by('-logo_weight')   
        
    viewing_account_user = account_user
    if alias is not None:
        return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')
    elif agent is not None:
        return HttpResponseRedirectAgent(request, '/login/')
    else:
        t = loader.get_template('main.html')
        
        c = RequestContext(request, dict={
            'alias': alias,
            'viewing_account_user': viewing_account_user,
            'random_name': random_name,
            'random_gender': random_gender,
            'blog_posts': blog_posts })
        return HttpResponse(t.render(c))

@global_def    
def terms(request, subdomain=None):
    alias, viewing_account_user, account = validate_account(request, subdomain=subdomain)
    if alias.beta_terms_date is None:
        log('__terms__ init: ', dir(request.session))
        t = loader.get_template('main_terms.html')
        c = RequestContext(request, dict={
            'base_url': subdomain,
        })
        return HttpResponse(t.render(c))
    else:
        # They've already agreed to the terms
        return HttpResponseRedirectAgent(request, '/')


@global_def
def account_home(request, subdomain=None, fullscreen='default'):
    try:
                    
        log("The full path is: [[0]] ", request.get_full_path())
        log("The session key is: [[0]] ", request.session._get_session_key())
        log("Alias [[0]]:", request.session.get('alias', None))
        
        alias = request.session.get('alias', None)

        start_time = now()
        log("Start...")
        
        log('__account_home__ init: ', subdomain)
        
        log("{1}", now() - start_time)
        
        to = get_redirect_param(request)
        agent = get_agent(request)
        log('using subdomain %s' % subdomain)
        
        try:
            account = Account.objects.get(base_url=subdomain)   # , site=get_site(request)
        except:
            if alias:
                log('finding subdomain via alias: %s' % alias)
            account = Account.objects.get(base_url=alias.account.subdomain)
            
        try:
            followup_remote_user = RemoteUser.objects.get(account=account, alias=None, type=RemoteUser.TYPE_FOLLOWUP, enabled=True)
        except:
            followup_remote_user = None
        
        log("Agent [[1]]:", agent)
        log("Alias [[1]]:", request.session.get('alias', None))

        if request.session.get('alias', None) is None:
            
            log('__account_home__ init: ', dir(request.session))
            t = loader.get_template('main_account_login.html')
            c = RequestContext(request, dict={
                'base_url': subdomain,
                'redirect_to': redirect_agent_url(agent, to)
            })
                        
            return HttpResponse(t.render(c))
        else:            
            alias, viewing_account_user, account = validate_account(request, subdomain=subdomain)
                                    
            # If they don't have the right service for this agent, don't let 'em in ... but let 'em upgrade
            if not alias.account.service.allow_agent(agent):
                return HttpResponseRedirectAgent(request, '/info/order/?upgrade=1')
            
            if (alias.beta_terms_date is None) and not ('master_override' in request.session):
                return HttpResponseRedirectAgent(request, '/dash/'+subdomain+'/terms/')
            elif to:
                return HttpResponseRedirectAgent(request, to)
            # For iPhone, Outlook and Salesforce, take them to the special setup wizard
            #  (but NOT the Firefox/Chrome plugin!)
            elif agent and (agent != 'firefox') and (alias.password_hash is None):
                return HttpResponseRedirectAgent(request, '/dash/'+subdomain+'/settings/')
            
        log("{1}", now() - start_time)

        user_tz = viewing_account_user.get_timezone()
        n = now().astimezone(user_tz)
                    
        log("{2}", now() - start_time)
        
        storage_used_mb, storage_total_mb = alias.account.calc_storage_used_mb()
        if storage_total_mb:
            storage_used_percent = int(max(0, min(100, (storage_used_mb/storage_total_mb)*100)))
        else:
            storage_used_percent = None
        
        log("{5}", now() - start_time)

        get_widget_manager(request).select_from_args(request)   # any filters/actions specified in the GET request?
                                                                #  (for e.g. Salesforce plugin, Firefox/Chrome plugin)
        
        log("{6}", now() - start_time)
           
        higher_services = []
        if alias.is_admin and (alias.account.trial_period or alias.account.service.is_free()):
            # Show the current service, for comparison
            higher_services.append({
                'service': alias.account.service, 
                'features_json': json.dumps(alias.account.service.get_features_for_display(other=alias.account.service)),
                'price_difference': alias.account.service.get_price_difference_string(alias.account.service, account=account)
            })
            # Show all higher services
            for s in Service.objects.filter(hidden=False).order_by('price_per_num_dmy'):
                if not s.is_free():
                    if s > alias.account.service:
                        higher_services.append({
                            'service': s, 
                            'features_json': json.dumps(s.get_features_for_display(other=alias.account.service)),
                            'price_difference': s.get_price_difference_string(alias.account.service, account=account)
                        })
            # If there aren't any higher services, then don't show anything at all
            if len(higher_services) <= 1:
                higher_services = []
        
        log("views_cover: templates = ", settings.TEMPLATE_DIRS)
        log("views_cover: agent = ", request.session.get("agent", None))
        t = loader.get_template('main_account.html')
        
        c = RequestContext(request, dict={
            'base_url': subdomain,
            'messages': None, # :) request.messages,
            'viewing_account_user': viewing_account_user,
            'alias': alias,
            'higher_services': higher_services,
            'followup_remote_user': followup_remote_user,
            'constants':  constants,
            'time_zones': COMMON_TIMEZONES,
            'fullscreen': fullscreen,
            'mail_services':  json.dumps(constants.MAIL_SERVICES),
            'storage_used_percent': storage_used_percent,
            'buckets': ajax_feeds.format_buckets_for_display(alias, None, _("Everything")),
            'current_bucket': get_widget_manager(request).bucket,
            'followup_only': get_widget_manager(request).followup_only,
            'search': get_widget_manager(request).search,
            'bucket': get_widget_manager(request).bucket,
            'logo_url': alias.account.logo_url if (alias.account.logo_url and not alias.account.logo_url.startswith('http://')) else None # Don't show logos if they're from insecure domain
        })
           
        log("{7}", now() - start_time)
        response = t.render(c)
        log("{8}", now() - start_time)

        #request.session.modified = True 
        return HttpResponse(t.render(c))
    
    except:
        log('__account_home__ fail: ', subdomain, traceback.format_exc())
        
        log("Clearing request.session [y]")        
        request.session.clear()
        
        msg = _("Unable to access account: %s.  You may be logged out or are running an older session on an incompatible upgrade." % (subdomain))
        try:
		request.messages['notices'].append(msg)
        except:
		pass

        #raise Exception(traceback.format_exc())
        if not settings.DEBUG:
            log('redirecting to %s/dash' % settings.NOTED_ROOT_URL)
            return HttpResponseRedirectAgent(request, '%s/dash/' % settings.NOTED_ROOT_URL)
        else:
            return HttpResponseRedirectAgent(request, '/dash/')

@global_def
def salesforce_view(request, subdomain=None, rule_id=None):
    '''
    The "automation" page where you can set up Salesforce syncing rules.
    '''
    try:
        alias, viewing_account_user, account = validate_account(request, subdomain=subdomain)
        
        token = None
        remote_credentials = RemoteCredential.objects.filter(remote_user__alias=alias, type__in=[RemoteCredential.TYPE_LEGACY, RemoteCredential.TYPE_BASIC_AUTH_TOKEN])
        if remote_credentials.count() > 0:
            token = remote_credentials[0].token
        
        try:
            sfuser = RemoteUser.objects.filter(type=RemoteUser.TYPE_SF, alias=alias, enabled=True)[0]
        except:
            sfuser = None
        team_sfusers = RemoteUser.objects.filter(type=RemoteUser.TYPE_SF, alias__account=alias.account, enabled=True)
        
        try:
            followup_remote_user = RemoteUser.objects.get(account=account, alias=None, type=RemoteUser.TYPE_FOLLOWUP, enabled=True)
        except:
            followup_remote_user = None
            
        if subdomain is None:
            raise Exception('salesforce: subdomain is None')
                
        next = request.GET.get('next')
        if not is_valid_redirect_url(next):
            next = None
        
        
        if rule_id:
            if rule_id == 'new':
                # Get a default rule (this is not saved to DB)
                rule = EmailRule.objects.get_default_for_account(alias, account)
            else:
                # Make sure they have access to this rule
                rule = EmailRule.objects.get(id=rule_id, type__in=[EmailRule.TYPE_SF, EmailRule.TYPE_FOLLOWUP], owner_account=account)
            
            t = loader.get_template('main_salesforce_edit.html')
            c = RequestContext(request, dict={
                'base_url': subdomain,
                'alias': alias,
                'logo_url': alias.account.logo_url if (alias.account.logo_url and not alias.account.logo_url.startswith('http://')) else None, # Don't show logos if they're from insecure domain
                'account': alias.account,
                'team_aliases': Alias.objects.active().filter(account=alias.account),
                'sfuser': sfuser,
                'team_sfusers': team_sfusers,
                'followup_remote_user': followup_remote_user,
                'viewing_account_user': viewing_account_user,
                'account_regexes': account.emailregex_set.all().order_by('id'),
                'token': token,
                'rule_id': rule_id,     # a number, or 'new'
                'rule': rule,
                'regexes': rule.regexes.all() if rule.id else [],
                'next': next
            })
            return HttpResponse(t.render(c))
        else:
            t = loader.get_template('main_salesforce.html')
            c = RequestContext(request, dict={
                'constants': constants,
                'base_url': subdomain,
                'alias': alias,
                'logo_url': alias.account.logo_url if (alias.account.logo_url and not alias.account.logo_url.startswith('http://')) else None, # Don't show logos if they're from insecure domain
                'account': alias.account,
                'team_aliases': Alias.objects.active().filter(account=alias.account),
                'sfuser': sfuser,
                'sf_rules': EmailRule.objects.filter(owner_account=alias.account, type__in=[EmailRule.TYPE_SF, EmailRule.TYPE_FOLLOWUP]).order_by('id'),
                'team_sfusers': team_sfusers,
                'viewing_account_user': viewing_account_user,
                'account_regexes': account.emailregex_set.all().order_by('description'),
                'token': token,
                'next': next
            })
            return HttpResponse(t.render(c))
        
    except:
        log('__account_salesforce__ fail: ', subdomain, traceback.format_exc())
        if subdomain:
            return HttpResponseRedirectAgent(request, '/dash/?name='+subdomain)
        else:
            return HttpResponseRedirectAgent(request, '/dash/')


@global_def
def settings_view(request, subdomain=None):
    try:
        alias, viewing_account_user, account = validate_account(request, subdomain=subdomain)
        query = TwitterUser.objects.filter(alias=alias)
        tw_user = TwitterUser.objects.filter(is_noted=True, site=get_site(request))
        
        if tw_user.count() >= 1:
            tw_user = tw_user[0]
        else:
            tw_user = None
            
        if query.count() == 1:
            twitteruser = query.get()
        else:
            twitteruser = None
            
        wizard_mode = (alias.password_hash is None)
        if 'wizard_section' in request.GET:
            wizard_mode = True
            wizard_section = request.GET['wizard_section']
        elif alias.password_hash is None:
            wizard_mode = True
            wizard_section = 'password'
        else:
            wizard_mode = False
            wizard_section = ''
         
        parent_url = request.session.get('parent_url')
        if parent_url:
            parent_url = parent_url.split('#')[0]       # ignore any previous hash tag
            
        token = None
        remote_credentials = RemoteCredential.objects.filter(remote_user__alias=alias, type__in=[RemoteCredential.TYPE_LEGACY, RemoteCredential.TYPE_BASIC_AUTH_TOKEN])
        if remote_credentials.count() > 0:
            token = remote_credentials[0].token
                        
        if subdomain:
            t = loader.get_template('main_settings.html')
            c = RequestContext(request, dict={
                'constants': constants,
                'base_url': subdomain,
                'alias': alias,
                'sfuser': RemoteUser.objects.get_primary_for_alias(alias, type=RemoteUser.TYPE_SF),
                'sf_rules': EmailRule.objects.filter(owner_account=alias.account, type=EmailRule.TYPE_SF).order_by('id'),
                'team_sfusers': RemoteUser.objects.filter(type=RemoteUser.TYPE_SF, alias__account=alias.account, enabled=True),
                'team_aliases': Alias.objects.active().filter(account=alias.account),
                'viewing_account_user': viewing_account_user,
                'noted_twitteruser': tw_user,
                'twitteruser': twitteruser,
                'time_zones': COMMON_TIMEZONES,
                'wizard_mode': wizard_mode,
                'wizard_section': wizard_section,
                'mail_services':  json.dumps(constants.MAIL_SERVICES),
                'parent_url': parent_url,
                'token': token,
                'logo_url': alias.account.logo_url if (alias.account.logo_url and not alias.account.logo_url.startswith('http://')) else None # Don't show logos if they're from insecure domain
            })
                        
            return HttpResponse(t.render(c))
        else:
            raise Exception('settings: subdomain is None')
    except:
        log('__account_home__ fail: ', subdomain, traceback.format_exc())
        if subdomain:
            return HttpResponseRedirectAgent(request, '/dash/?name='+subdomain)
        else:
            return HttpResponseRedirectAgent(request, '/dash/')


# No global_def on this view, because if the account isn't active, it needs to
#  redirect the user to /login (not to /net!) -- we don't want to show the
#  upgrade page directly inside the Firefox/Chrome plugin sidebar.
def dashboard_view(request):
    "Used by the Salesforce widget, and other things that need to display a specific piece of data w/o knowing the base URL"
    try:        
        log("dashboard_view: ", request.get_full_path())
        alias = request.session.get('alias', None)
        #if (not alias) or (alias.account.status != ACCOUNT_STATUS_ACTIVE):
        #    return HttpResponseRedirectAgent(request, '/login/?next=%s' % urllib.quote(request.get_full_path()))
        return account_home(request, alias.account.base_url)
    except:
        log("Clearing request.session [z]")        
        request.session.clear()
        return HttpResponseRedirectAgent(request, '/')


def login_view(request):
    '''
    Show the user the login form, or...
    For Salesforce, Firefox, Outlook: if a login token is given, log the user in immediately
     and redirect them to their main view (e.g., the iframe in Salesforce).
    '''
    
    try:

        agent = get_agent(request)
        log("login_view: agent=", agent)
        redirect_to = get_redirect_param(request)
        token = request.GET.get('token')
        version = request.GET.get('version')
        referer = request.META.get('HTTP_REFERER', '')
        log("login_view: referer=", referer)
        log("redirect_to [1]:", redirect_to)
        if not is_valid_redirect_url(redirect_to):
            redirect_to = None
                          
        if token:
            log("token [1] starting with", token[:6])    
            try:                
                rc = RemoteCredential.objects.get(token=token, type__in=[RemoteCredential.TYPE_LEGACY, RemoteCredential.TYPE_WEB_ONETIME_TOKEN, RemoteCredential.TYPE_WEB_ONETIME_TOKEN_USED])
                
                remote_user = rc.remote_user
                 
                alias = remote_user.alias
                # TODO: If alias is inactive ...
                log("login_view: [1]")
                if (remote_user.type != RemoteUser.TYPE_SF) or \
                    (settings.DEBUG or RE_REFERER_SALESFORCE.match(referer)) or \
                    (request.session.get('alias', None) == alias):
                    if rc.type == RemoteCredential.TYPE_WEB_ONETIME_TOKEN:
                        rc.type == RemoteCredential.TYPE_WEB_ONETIME_TOKEN_USED # Token can only be used once
                        rc.save()
                    elif rc.type == RemoteCredential.TYPE_WEB_ONETIME_TOKEN_USED:
                        if (request.session.get('alias', None) != alias):
                            raise Exception(_('Cannot use expired web token.'))
                    log("login_view: [2]")
                    if version:
                        remote_user.version = version
                        remote_user.save()
                    user = auth.authenticate(user=alias.user)        # dummy authentication, just fill in the 'backend' attribute
                    auth.login(request, user)
                    request.session['alias'] = alias
                    
                    # Keep any extra parameters being passed to us through the iframe as this dictionary in request.session
                    request.session['login_params'] = dict([(str(key), str(value)) for key, value in request.GET.items()])
                    
                    log("redirect_to [2]:", redirect_to)
                    log("request.session['alias'] is: [3]", request.session['alias'])
                    log("The session key is: [3] ", request.session._get_session_key())
                    log("The full path is: [3] ", request.get_full_path())
                    redirect_to = redirect_to or ('/dash/%s/' % alias.account.base_url)
                    log("redirecting to ", redirect_to)
                    return HttpResponseRedirectAgent(request, redirect_to)
            except:
                log("Could not validate token.")
                log(traceback.format_exc())
                auth.logout(request)      
                clear_session(request)
                return HttpResponseRedirectAgent(request, '/login/')
                      
        t = loader.get_template('main_account_login.html')
        c = RequestContext(request, dict={
            'agent': agent,
            'redirect_to': redirect_to or '/dash/'
        })
        if int(bool(request.GET.get(settings.PROFILER_KEY))):
            c['profiler_key'] = settings.PROFILER_KEY
        else:
            c['profiler_key'] = ''
            
        # For debugging and profiling, a comma-separated list of widgets we won't create at login time
        c['disable_widgets'] = request.GET.get('disable_widgets', '')
        c['summary_enabled'] = request.GET.get('summary_enabled', True)
        return HttpResponse(t.render(c))
    except:
        log('login_view fail: ', traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')
            
def forgot_view(request):
    try:
        agent = get_agent(request)
        log("forgot_view: agent=", agent)
        to = get_redirect_param(request)
        
        t = loader.get_template('main_account_login_forgot.html')
        c = RequestContext(request, dict={
            'agent': agent,
            'redirect_to': to if to else '/dash/',
            'time_zones': COMMON_TIMEZONES
        })

        return HttpResponse(t.render(c))
    except:
        log('forgot_view fail: ', traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')
    
def reset_password_view(request, slug=None):
    try:
                
        if not slug:
            raise Exception()
        
        slug_hash = hashlib.md5(slug).hexdigest()
        
        # Don't filter by active() here -- owners of expired accounts need to be able to log in to pay
        queryset=Alias.objects.filter(reset_password_slug_hash=slug_hash, reset_password_slug_date__gte=now()-ONE_DAY)
        if queryset.count() == 0:
            raise Exception()
        
        alias = queryset[0]
        alias.password_hash = None
        alias.save()
        
        return HttpResponseRedirectAgent(request, '/dash/?alias=%s' % alias.slug)
    
    except:
        log('reset_password_view fail: ', traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')
    
        

def logout_view(request): 
    auth.logout(request)      
    clear_session(request)
    to = get_redirect_param(request)
    return HttpResponseRedirectAgent(request, to if to else '/dash/')


def check_sub_domain(request):  
#    log('__check_sub_domain__: mobile=', mobile)
#    if mobile:                       # Send all mobile devices to the login page
#        return HttpResponseRedirectAgent(request, '/login/')   
    
    # For debugging
    auth_string = request.META.get('HTTP_AUTHORIZATION', None)
    log('auth_string: ', auth_string)
        
    domain = request.META.get('HTTP_HOST', '').replace(':3636','')
    if not domain:                              # This shouldn't happen
        return HttpResponseRedirectAgent(request, '/')
    log('__check_sub_domain__: domain', domain)
    domain_parts = domain.split(".")
    if domain.count('www') > 0 or domain.count('web') > 0 or domain.count('staging') > 0 or len(domain_parts) < 3:
        return HttpResponseRedirectAgent(request, '/dash/')
    domain = ".".join(domain_parts[1:])

    try:
        subdomain = request.META.get('HTTP_HOST', '').replace(':3636','').replace(domain, '').replace('.', '').replace('www', '').lower().strip()
        log('__check_sub_domain__ init: loading account_home', subdomain)
        if settings.SERVER == 'live':
            domain = get_site(request).domain.lower()
            if domain in settings.SSL_DOMAINS:
                new_url = 'https://%s/dash/%s/' % (domain, subdomain)
            else:
                new_url = 'http://%s/dash/%s/' % (domain, subdomain)
            log("Redirect to", new_url)
            return HttpResponseRedirectAgent(request, new_url)
        return account_home(request,subdomain)
    except:
        return HttpResponseRedirectAgent(request, '/dash/')

@global_def
def send_eventresponse_mail(request, subdomain=None):
    log("send_eventresponse_mail...")
    try:
        alias, viewing_account_user, account = validate_account(request, subdomain=subdomain)
        
        slug = request.GET['slug']
        er = EventResponse.objects.get(slug=slug)
        
        # Is the response for this alias?
        if er.alias != alias:
            raise Exception("send_eventresponse_mail: EventResponse slug %s doesn't match alias %s" % (slug, str(alias)))
        
        # Is there a function to resend the thingy?
              
        
        # 
        # if subdomain:
        #      t = loader.get_template('main_settings.html')
        #      c = Context({
        #          'constants': constants,
        #          'base_url': subdomain,
        #          'alias': alias,
        #          'viewing_account_user': viewing_account_user,
        #      })
        #      c.update(global_context(request))
        #      return HttpResponse(t.render(c))
        #  else:
        #      raise Exception('settings: subdomain is None')
    except:
        log('__account_home__ fail: ', subdomain, traceback.format_exc())
        if subdomain:
            return HttpResponseRedirectAgent(request, '/dash/?name='+subdomain)
        else:
            return HttpResponseRedirectAgent(request, '/dash/')
        
    
    return HttpResponse("send_eventresponse")
    
     #    
     # domain = request.META['HTTP_HOST'].replace(':3636','')
     # log('__check_sub_domain__ init: domain', domain)
     # domain_parts = domain.split(".")
     # if domain.count('www') > 0 or len(domain_parts) < 3:
     #     return HttpResponseRedirectAgent(request, '/dash/')
     # domain = ".".join(domain_parts[1:])    
     # subdomain = request.META['HTTP_HOST'].replace(':3636','').replace(domain, '').replace('.', '').replace('www', '')
     # try:
     #     log('__check_sub_domain__ init: loading account_home', subdomain)
     #     return account_home(request,subdomain)
     # except:
     #     return HttpResponseRedirectAgent(request, settings.NOTED_ROOT_URL+'/dash/?name='+subdomain)
     # 



def main_javascript_js_view(request):
    
    try:
        t = loader.get_template('js/main_javascript.js')
        c = RequestContext(request)
        
        return HttpResponse(t.render(c))

    except:
        log(traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')        
    
    
def main_account_js_view(request):
    """
    Loader for account.js, where most of the Javascript functionality needed for logged-in users lives.
    This file also needs to be able to load for non-logged in users, because the Firefox/Chrome plugin
    loads it only once (when the window loads)
    """
    
    try:                
        alias = request.session.get('alias')        # None if no logged-in user
        agent = get_agent(request)
        
        # Get the URL of the containing window (if we're in an iframe [Salesforce only, for now])
        #  Also get the root URL ('https://na6.salesforce.com/blah/?thing=123' becomes 'https://na6.salesforce.com/')
        agent_url = request.session.get('login_params') and request.session['login_params'].get('agent_url')
        if agent_url:
            s = urlparse(agent_url)
            agent_url_root = '%s://%s/' % (s.scheme, s.netloc)
        else:
            agent_url_root = None
                    
        # Start a crawl if they just logged in
        if alias:
            crawl_ongoing = NotedTaskMeta.objects.filter(status=NotedTaskMeta.STATUS_STARTED, type=NotedTaskMeta.TYPE_CRAWL, alias=alias).count() > 0
            
            if alias not in request.session['aliases_crawled'] and alias.server_enabled and alias.account.service.allow_crawl:
                if (agent == 'salesforce'):
                    # For Salesforce, only show the crawl bar the first time
                    crawl_needed = (Email.objects.filter(crawled_alias=alias).count() == 0)
                elif agent == 'firefox':
                    # For the Firefox/Chrome plugin, we don't do crawls, period
                    crawl_needed = False
                else:
                    crawl_needed = True
            else:
                crawl_needed = False
        else:
            crawl_ongoing, crawl_needed = False, False
                
        t = loader.get_template('js/main_account.js')
        c = RequestContext(request, dict={
            'crawl_ongoing': crawl_ongoing,
            'agent_url': agent_url,
            'agent_url_root': agent_url_root,
            'crawl_needed': crawl_needed,
            'include_contacts_hook': agent in ['outlook', 'firefox'],
            'constants': constants
        })
        
        return HttpResponse(t.render(c))

    except:
        log(traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')       
