from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
import network.views_cover
import network.views_info           # Must do this import here, so that paypal_ipn_success and ipn_failed get registered
import network.views_sf
import network.views_tracker
import plugins.views
import os
from paypal.standard.signals import payment_was_successful, payment_was_flagged
import assist.payment

from basic.blog import urls
import api.emitters                 # Register the XMLCDataEmitter, since several things use it

admin.autodiscover()

#import mobileadmin
#mobileadmin.autoregister()

#handler500 = 'network.views_error.err_500'
#handler404 = 'network.views_error.err_404'

from network.views_cover import LatestEntries
from plugins.views import LatestConnectors

feeds = {
    'blog': LatestEntries,
    'connectors': LatestConnectors
}

urlpatterns = patterns('',
    (r'^dash/', include('network.urls')),
    (r'^w3c/(?P<path>.*)$', 'django.views.static.serve', {'document_root': os.path.join(settings.PROJECT_DIR, 'static/w3c')}),
    (r'^dashboard/', 'network.views_cover.dashboard_view'),
    (r'^admin/(.*)', admin.site.root),
    (r'^error/$', network.views_cover.always_error),
    (r'^login/$', 'network.views_cover.login_view'),
    (r'^login/forgot/$', 'network.views_cover.forgot_view'),
    (r'^login/reset_password/(?P<slug>\w+)', 'network.views_cover.reset_password_view'),
    (r'^logout/', 'network.views_cover.logout_view'),
    (r'^download/connectors/$', plugins.views.connectors_view),
    (r'^download/connectors/(?P<base_name>\w+)', 'plugins.views.connectors_detail_view'),
    (r'^embed/', include('embed.urls')),
    (r'^openid_auth/', include('openid_auth.urls')),
    (r'^ajax/openid_auth/', include('openid_auth.urls_ajax')),  # must come before the ajax/ line
    (r'^ajax/', include('network.urls_ajax')),
    (r'^api/', include('api.urls')),
    (r'^install/', 'network.views_info.install_view'),          # install page for Firefox/Chrome app

    # Paypal IPN notifications go here:
    (r'^ipn/$', 'paypal.standard.views.ipn'),
    
    # Plaxo widget callback goes here:
    (r'^plaxo/callback/$', 'network.views_plaxo.plaxo_callback_view'),
    
    # General pages and login/registration
    (r'^\@/', include('network.urls_info')),
    (r'^info/', include('network.urls_info')),
                       
    # Proxying images over HTTPS for the social widget
    (r'^proxy/', 'network.views_proxy.proxy_view'),
    
    # Unsubscribing from form mails.
    (r'^unsubscribe/$', 'network.views_formmail.unsubscribe_view'),
        
    # Correlating Twitter users with Aliases
    (r'^twitter/', include('twitter.urls')),
    
    (r'^blog/', include('basic.blog.urls')),

    (r'^tracker/(?P<account_slug>\w+)/(?P<owner_alias_id>\d+)/$', 'network.views_tracker.tracker_view'),
    
    (r'^feeds/(?P<url>.*)/$', 'django.contrib.syndication.views.feed', {'feed_dict': feeds}),
    
    (r'^comments/', include('django.contrib.comments.urls')),
 
    (r'^jsi18n/$', 'django.views.i18n.javascript_catalog',
                   {'packages': 'django.conf'}),    # see http://www.carcosa.net/jason/blog/computing/django/gotchas-2006-04-19
    #(r'^ydnP2Gciz\.html$', 'django.views.generic.simple.direct_to_template', {'template': 'ydnP2Gciz.html'}), # for obtaining Yahoo API key (only needed once)
    
    # TESTING!
    (r'^$', network.views_cover.check_sub_domain),
    #(r'^$', include('network.urls')),
    
    # (r'^$', 'django.views.generic.simple.redirect_to', {'url': '/net'}),    
)

#try:
#    from lib.uwsgi_admin import *
#    urlpatterns.append(url(r'^uwsgi/', include('lib.uwsgi_admin.urls')),)
#except ImportError:
#    pass

# Register IPN callback functions with the Paypal code
payment_was_successful.connect(assist.payment.paypal_ipn_success)
payment_was_flagged.connect(assist.payment.paypal_ipn_flagged)


if (True) or (settings.SERVER == 'staging'):
    
    if settings.UPLOADS_ROOT:
        urlpatterns.append(url(r'^static/uploads/(?P<path>.*)$', 
            'django.views.static.serve', {'document_root': settings.UPLOADS_ROOT }))
        
    media_base = settings.MEDIA_URL
    if media_base.startswith('/'):
        media_base = '^' + media_base[1:]
    urlpatterns.append(url(r'%s/(?P<path>.*)$' % media_base, 
        'django.views.static.serve', {'document_root': settings.MEDIA_ROOT }))
    
    
    
   
