"""
Run a very simple local smtpd server. This server accepts mails to the assistant and processes them with celery.

Postfix on the crawl server relays mails to this server, which runs on a non-default smtp port.
supervisord is used to start this server on boot time and keep it running.
"""

import sys, os, pdb

import init_platform

from django.utils.translation import ugettext as _
from assist.models import *
from twitter.models import *
from crawl.models import *
from remote.models import *
from assist import register
from assist import reminder
from utils import *

from django.db.models import Q
from datetime import *
import random, hashlib, re
import json, pytz, urllib2, hashlib
import traceback, pdb
from collections import defaultdict
from assist import parsedate
from assist import mail, mailthread
import email
from django.conf import settings
import BeautifulSoup
import smtpd, asyncore
#from crawl.tasks import ProcmailDirectTask

# TODO: Implement this on the celery_worker server (?)
#
#class AssistantSMTPServer(smtpd.SMTPServer):
#    
#    def process_message(self, peer, mailfrom, rcpttos, data):
#        log("--------------")
#        log("process_message: peer = ", peer, " mailfrom = ", mailfrom, " rcpttos = ", rcpttos, " len = ", len(data))
#        ProcmailDirectTask().delay(data, rcpttos=rcpttos)            
#        return None             # this return value means normal delivery
#
#
#if __name__ == '__main__':
#    if settings.LOG_ENABLED:
#        settings.LOG_FILE = open(os.path.join(settings.LOG_DIR, 'assistant_smtpd.log'), 'a')
#    log("Starting AssistantSMTPServer...")
#    server = AssistantSMTPServer((settings.ASSISTANT_SMTPD_HOST, settings.ASSISTANT_SMTPD_PORT), None)
#    asyncore.loop()
        

