#!/usr/bin/python

import os, sys, site

sys.stdout = sys.stderr
prev_sys_path = list(sys.path)

site.addsitedir('/usr/local/lib/python2.6/dist-packages')
sys.path.append('/home/alex/workspace/noted')
sys.path.append('/home/alex/workspace/noted/lib')

# reorder sys.path so new directories from the addsitedir show up first
new_sys_path = [p for p in sys.path if p not in prev_sys_path]
for item in new_sys_path:
    sys.path.remove(item)
sys.path[:0] = new_sys_path

os.environ['DJANGO_SETTINGS_MODULE'] = "settings"

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
