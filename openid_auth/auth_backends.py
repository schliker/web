from django.contrib.auth.backends import ModelBackend
from django.conf import settings
from django.contrib.auth.models import User
from remote.models import RemoteCredential
import hashlib, pdb
from utils import *
from openid.consumer import consumer
from openid.extensions import ax

class OpenIDBackend(ModelBackend):
    """
    Authenticate a user using an OpenID consumer object which must be in the success state
    OpenID consumer object must provide the user's email address, and must be from
    a trusted source (i.e., Gmail or Google Apps)!
    """
    
    def authenticate(self, openid_response=None):
        try:
            if openid_response and openid_response.status == consumer.SUCCESS:          
                fr = ax.FetchResponse.fromSuccessResponse(openid_response)
                first_name = fr.getSingle('http://axschema.org/namePerson/first')
                last_name = fr.getSingle('http://axschema.org/namePerson/last')
                email = fr.getSingle('http://axschema.org/contact/email')
                user = User.objects.get(email=email)
                log("OpenIDBackend.authenticate: returning user ", user)
                return user
            else:
                log("OpenIDBackend.authenticate: not success")
                return None
        except User.DoesNotExist:
            return None
        