from models import *


class NotedTaskMetaInline(admin.TabularInline):
    model = NotedTaskMeta
    extra = 0
    exclude = ('emails_crawled',)
    

