from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.auth.models import User
from django.template import Context, loader, Template
from django.contrib import admin
import hashlib
import re, string, os, json
from utils import *
# from datetime import *
import datetime
from assist.constants import *
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
from assist.models import *
from assist import mail
from schedule.models import *
from pytz import UTC
import pdb, traceback, pprint
import urllib, urllib2, sys, subprocess
# Create your models here.

class CrawlState(models.Model):
    num_slices = models.IntegerField(default=96)
    current_slice = models.IntegerField(default=0)

# An extended version of celery's TaskMeta, lets us kill stuck tasks and get progress info

class NotedTaskMetaManager(Manager):

    def kill_all_started(self):
        "Used by the Celery start script only"
        for ntm in NotedTaskMeta.objects.filter(status=NotedTaskMeta.STATUS_STARTED):
            ntm.kill()
            
    def kill_all_stuck(self):
        "Release cron jobs that have gotten stuck for whatever reason, so users can crawl again"
        stuck = NotedTaskMeta.objects.filter(status=NotedTaskMeta.STATUS_STARTED)\
            .filter(Q(date_updated__lt=now()-10*ONE_MINUTE) | Q(date_started__lt=now()-10*ONE_MINUTE, date_updated=None))
        for ntm in stuck:
            ntm.kill()
                    
    def delete_old_finished(self):
        "Run by the cron job (nightly)"
        queryset = NotedTaskMeta.objects.filter(status=NotedTaskMeta.STATUS_FINISHED, date_updated__lt=now()-ONE_WEEK)
        queryset.delete()
        
class NotedTaskMeta(models.Model):
    
    TYPE_CRAWL = 'tCWL'
    TYPE_FORMMAIL = 'tFML'
    
    STATUS_STARTED = 'tSTA'
    STATUS_FINISHED = 'tFIN'
    STATUS_KILLED = 'tKIL'
    
    task_id = models.CharField(max_length=255, blank=True, null=True)       # ID of the underlying Celery task
    type = models.CharField(max_length=4, blank=True, null=True)
    status = models.CharField(max_length=4, blank=True, null=True)
    alias = models.ForeignKey('assist.Alias', blank=True, null=True)
    message = models.TextField(blank=True, null=True)                       # a text message for the progress indicator
    date_started = UTCDateTimeField(default=now)
    date_updated = UTCDateTimeField(null=True, blank=True)
    kill_after_how_long = models.IntegerField(null=True, blank=True)        # after this many seconds, kill the stuck task (NOT USED at the moment)
    success = models.NullBooleanField(null=True, blank=True)
    custom = models.TextField(blank=True, null=True)                        # JSON of any extra data we want to store here (used by the crawlbar)
    emails_crawled = models.ManyToManyField('assist.Email')
    args_json = models.TextField(blank=True, null=True)
    
    objects = NotedTaskMetaManager()
    
    def get_custom(self):
        return json.loads(self.custom or '{}')
        
    def mark_started(self, message=''):
        self.status = NotedTaskMeta.STATUS_STARTED
        self.message = message
        n = now()
        self.date_started = n
        self.date_updated = n
        self.save()
            
    def mark_updated(self, custom=None, message=''):
        if custom:
            self.custom = json.dumps(custom)
        self.date_updated = now()
        self.message=message
        self.save()
    
    def mark_finished(self, custom=None, success=True, message=''):
        self.status = NotedTaskMeta.STATUS_FINISHED
        if custom:
            self.custom = json.dumps(custom)
        self.message = message
        self.success = success
        n = now()
        self.date_updated = n
        self.date_finished = n
        self.save()
        
    def kill(self):
        log("Killing task: ", self.id, self.alias, self.task_id)
        self.success = False
        self.status = NotedTaskMeta.STATUS_KILLED
        if self.task_id:                # Revoke the actual Celery task
            from celery.task.control import revoke
            revoke(self.task_id)
        self.save()
        
        
    def delay(self):
        '''
        Decode the parameters in self.args_json and start a Celery task or a local task
        (depending on where it's implemented).

        Requires args_json to be set!
        '''
        
        assert(self.status == NotedTaskMeta.STATUS_STARTED)
                          
        if self.type == NotedTaskMeta.TYPE_CRAWL:
            
            from django.core.urlresolvers import reverse
            import api.urls
            
            # CrawlTask is a celery_worker task
            
            from crawl.tasks import CrawlTask
            args = json.loads(self.args_json)
            max_msgs = args.get('max_msgs', HOWMANY_IMAP_CRAWL_USER)
            
            # Calculate how far back we are crawling
            recrawl_from = args.get('recrawl_from')         # This is set if we're crawling archived mails     
            if recrawl_from:                            
                # For archive mode, crawl back from the specific date
                since = pytz.UTC.localize(datetime.strptime(recrawl_from, '%Y-%m-%d %H:%M:%S'))
            elif self.alias.account.service.crawl_email_howlong:
                # Crawl back to the limit of the account's retention settings
                since = now() - self.alias.account.service.crawl_email_howlong * ONE_DAY
            else:
                since = now() - HOWLONG_IMAP_CRAWL_DEFAULT
            
            result = CrawlTask().delay(
                self.alias.get_mail_settings(), 
                since,
                settings.INTERNAL_CALLBACK_URL + reverse(api.urls.internal_messages_resource),
                previous_folders = [] if recrawl_from else [f.as_dict() for f in EmailFolder.objects.filter(alias=self.alias)],
                block_confidential_mails = True,
                block_mailing_list_mails = True,
                archive_mode = bool(recrawl_from),
                user_email = self.alias.email,
                callback_struct = {
                    'alias_id':             self.alias.id,
                    'noted_task_meta_id':   self.id
                }
            )
                
            self.task_id = result.task_id
            self.save()
            
            return result
        
        elif self.type == NotedTaskMeta.TYPE_FORMMAIL:
            # SendFormMailTask is a local task, for now
                        
            from assist.tasks import SendFormMailTask
            args = json.loads(self.args_json)
            return SendFormMailTask().delay(self.alias, args, noted_meta=self)
        
        else:
            self.mark_finished(success=False)
            raise NotImplementedError
        
            
    