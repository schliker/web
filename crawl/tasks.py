"""
Tasks related to crawling

These tasks actually run on the celery_worker server and the code is in the celery_worker repo.
The tasks in this file are placeholders with the same names, so that code in the Noted repo can 
call Celery tasks defined in another repo.
"""
    
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.auth.models import User
from django.contrib import admin
import hashlib
import re, string, os, cPickle
import json
from utils import *
# from datetime import *
import datetime
from assist.constants import *
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
from schedule.models import *
from models import *
import pdb, traceback
import urllib, urllib2, sys
import time
import multiprocessing
import subprocess
import crawl
from celery.task import *

patch_urllibs()

CELERY_WORKER_QUEUE = 'celery_worker'

class CeleryWorkerTestTask(Task):
    """
    Easy way to test that we're talking to celery_worker
    The real CeleryWorkerTestTask takes the argument (a number), and returns that number plus 111.
    """
    
    queue = CELERY_WORKER_QUEUE

    def run(self, *args, **kwargs):
        return "BAD CeleryWorkerTestTask"               # placeholder
    

class CeleryWorkerTestWaitTask(Task):
    """
    Easy way to test that we're talking to celery_worker
    The real CeleryWorkerTestWaitTask takes the argument (a number), and returns that number plus 111.
    """
    
    queue = CELERY_WORKER_QUEUE

    def run(self, *args, **kwargs):
        return "BAD CeleryWorkerTestWaitTask"               # placeholder
    
    
class MailServerAuthTask(Task):
    """
    Check that a the given mail settings are valid by connecting to the mail server
    """
    
    queue = CELERY_WORKER_QUEUE
    
    def run(self, mail_settings, **kwargs):
        return "BAD MailServerAuthTask"                 # placeholder
    
               
class GetCaptchaTask(Task): 
    """
    Retrieve a captcha challenge from a mail server (only Google Mail / Google Apps for now)
    """  
    
    queue = CELERY_WORKER_QUEUE
  
    def run(self, mail_settings, **kwargs):
        return "BAD GetCaptchaTask"                          # placeholder
    

class CrawlTask(Task): 
    """
    Crawl an alias and call the internal callback API with the messages.
    """  
    
    queue = CELERY_WORKER_QUEUE
  
    def run(self, mail_settings, since, callback_url, **kwargs):
        return "BAD CrawlTask"                              # placeholder
    
    
    
class RetrieveAndStoreFileAttachmentTask(Task):
    """
    Retrieve a file attachment from a mail stored somewhere on this user's server, store it on Cloud Files
    """   
    
    queue = CELERY_WORKER_QUEUE

    def run(self, mail_settings, folder, path, mimepath, container_name, file_name):
        return "BAD RetrieveAndStoreFileAttachmentTask"     # placeholder
    
        
        