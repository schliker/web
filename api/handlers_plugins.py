import re, pdb, traceback
from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import *
from plugins.models import *
from piston.emitters import Emitter

# API calls that don't require authentication

class ListPluginsHandler(AnonymousBaseHandler):
    
    allowed_methods = ('GET', )

    def read(self, request):
        
        revisions = []
        queryset = Plugin.objects.filter(status=Plugin.STATUS_ACTIVE).order_by('name')
        if bool(int(request.GET.get('required_install', '0'))):
            queryset = queryset.filter(required_install=True)
        for p in queryset:
            pr = p.get_latest_revision()
            if pr is not None:
                revisions.append(pr.as_json())
        
        return {'success': True, 'revisions': revisions}
    
    