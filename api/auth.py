from django.conf.urls.defaults import *
from django.contrib.auth.models import User, AnonymousUser
from piston.resource import Resource
from piston.authentication import HttpBasicAuthentication
from api.handlers_auth import *
from assist.models import Alias
import pdb
import json
from django.utils.translation import ugettext as _

#=======================================
# Legacy stuff   

# Safe to have this here because it requires that user already be not-None
class LegacyDummyAuthBackend(object):
    def authenticate(self, user=None):
        return user
    
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class LegacyAuthentication(object):

#    Authentication handlers must implement two methods:
#     - `is_authenticated`: Will be called when checking for
#        authentication. Receives a `request` object, please
#        set your `User` object on `request.user`, otherwise
#        return False (or something that evaluates to False.)
#     - `challenge`: In cases where `is_authenticated` returns
#        False, the result of this method will be returned.
#        This will usually be a `HttpResponse` object with
#        some kind of challenge headers and 401 code on it.

#    def __init__(self, auth_func=django_auth, realm='API'):
#        self.auth_func = auth_func
#        self.realm = realm

    def is_authenticated(self, request):
        log("Legacy is_authenticated start...")
        token = request.GET and request.GET.get('token', None)
        if not token:
            token = request.POST.get('token', None)
        if not token:
            return False
        log("... token starting with ", token[:6])
        try:
            token = RemoteCredential.objects.filter(incoming=True, type=RemoteCredential.TYPE_LEGACY)\
                .filter(Q(date_expires=None) | Q(date_expires__gte=now())).get(token=token)
            log("... found it!")
            token.date_last_accessed = now()
            token.save()
            request.user = token.alias.user
            request.session['alias'] = token.alias
            request.session['apitoken'] = token
            return True
        except:
            log("... not found.")
            return False
                
    def challenge(self):
        resp = HttpResponse("Authorization Required")
#        resp['WWW-Authenticate'] = 'Basic realm="%s"' % self.realm
        resp.status_code = 401
        return resp
    
    
#==============================================
# New API


class AliasHttpBasicAuthentication(object):
    """
    Modified version of HttpBasicAuthentication from Piston.
    Adds the following:
        * authenticate by token or by password (according to use_token in __init__)
        * sets request.session['alias'] on successful authentication
        * if authenticating by token, sets request.session['remote_user']
    """
    
    def __init__(self, auth_func=authenticate, realm='API', use_token=False):
        self.auth_func = auth_func
        self.realm = realm
        self.use_token = use_token
        self.result = {'success': False, 'msg': 'Authorization Required'}

    def _get_user_tuple(self, request):
        """
        Takes a request and attempts to find the user, using password or token
        according to self.use_token
        Returns a tuple: user, alias, remote_credential
        """
        
        log("AliasHttpBasicAuthentication._is_authenticated")
        auth_string = request.META.get('HTTP_AUTHORIZATION', None)
        log("... auth_string: ", auth_string[:10] if auth_string else 'None')
        
        if not auth_string:
            return None, None, None
            
        try:
            (authmeth, auth) = auth_string.split(" ", 1)

            if not authmeth.lower() == 'basic':
                return None, None, None

            auth = auth.strip().decode('base64')
            (username, secret) = auth.split(':', 1)
        except (ValueError, binascii.Error):
            return None, None, None
        
        aliases = Alias.objects.get_aliases_by_email(username.strip().lower(), include_inactive_accounts=True)

        if self.use_token:
            log("AliasHttpBasicAuthentication[1]:", username, secret[0] if secret else '')
            for a in aliases:
                remote_credential = a.validate_token(secret, RemoteCredential.TYPE_BASIC_AUTH_TOKEN)            # token validation
                if remote_credential:
                    try:
                        Alias.objects.active().get(pk=a.pk) # make sure it's an active user on an active account
                        return a.user, a, remote_credential
                    except:
                        self._expired(a, remote_credential.remote_user)
                        return None, None, None
        else:
            log("AliasHttpBasicAuthentication[2]:", username, secret[0] if secret else '')
            for a in aliases:
                if a.validate_password(secret) or a.validate_master_password(secret):     
                    # password validation
                    try:
                        Alias.objects.active().get(pk=a.pk) # make sure it's an active user on an active account
                        return a.user, a, None
                    except:
                        self._expired(a, None)
                        return None, None, None                    
                     
        self.result = {'success': 'False', 'msg': _('Invalid username or token.'), 'invalid': True}
        return None, None, None
       
    def _expired(self, alias, remote_user):
        # Return a one-time web token they can use to log in to upgrade/pay for their account
        self.result = {
            'success': False, 
            'msg': _('Your account has expired.'), 
            'expired': True
        }
        
        if remote_user:
            cred_type = RemoteCredential.TYPE_WEB_ONETIME_TOKEN
            token, created = RemoteCredential.objects.get_or_create_for_remote_user(\
                alias, remote_user, cred_type)            
            self.result.update({\
                'token': token.token,
                'cred_type': cred_type
            })
        
    def is_authenticated(self, request):
        "Wrapper for _authenticate that sets request variables"
        user, alias, remote_credential = self._get_user_tuple(request)
        
        if user:
            request.user = user
            request.session['alias'] = alias
            if remote_credential:
                request.session['remote_credential'] = remote_credential
                request.session['remote_user'] = remote_credential.remote_user
            return True
        else:
            request.user = AnonymousUser()
            return False
        
    def challenge(self):
        resp = HttpResponse(json.dumps(self.result))    #, mimetype='application/json')
        resp['WWW-Authenticate'] = 'Basic realm="%s"' % self.realm
        resp.status_code = 401
        return resp
    
    
    