import re, pdb, traceback
from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import *
from remote.models import *
from django.utils.translation import ugettext as _
from piston.emitters import Emitter
from django.utils.xmlutils import SimplerXMLGenerator
from django.http import HttpRequest
from xml.dom import minidom


class RegisterHandler(AnonymousBaseHandler):
    """
    Register for an account with the given email address.
    Not much more than a wrapper for ajax/send_registration.
    """
    
    allowed_methods = ('POST', )

    def create(self, request):        
        from assist import register
        if hasattr(request, 'data'):
            data = request.data                                     # data as JSON
        else:
            data = request.REQUEST                                  # form-encoded POST data
        if not data.get('service_name'):
            data['service_name'] = settings.API_SERVICE_DEFAULT     # default for registration through (say) desktop app
        return register.register(request, data)
    
            
    

class ServicesHandler(BaseHandler):
    """
    Return a list of the available Services, along with their prices in human-readable form
    """

    allowed_methods = ('GET', )
    
    def read(self, request):
        queryset = Service.objects.get_available_services(agent=request.REQUEST.get('agent'))
        services = []
        
        for s in queryset:
            services.append({
                'service': {
                            'name':         s.name,
                            'terms_string': s.get_alternate_terms_string()
                }
            })
        
        return { 'success': True, 'services': services }
    
    
                     