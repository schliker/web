#!python2.6

"Main entry point into Calenvy for command-line operations"

import sys, os, pdb

import init_platform

from django.utils.translation import ugettext as _
from assist.models import *
from crawl.models import *
from remote.models import *
from assist import reminder
from utils import *

from django.db.models import Q
from datetime import *
import random, hashlib, re
import json, pytz, urllib2, hashlib
import traceback, pdb
from collections import defaultdict
from assist import parsedate
from assist import mail, mailthread
from crawl import crawler, procmail
import email
from django.conf import settings
import crawl.tasks
import assist.tasks
        
def cron_nightly():
    reminder.mark_old_faded_events_completed()
    Account.objects.inactivate_expired()        # Active -> expired
    Account.objects.delete_pending()            # Pending deletion -> deleted
    #Account.objects.delete_old_inactivated()    # expired, old -> deleted
    if settings.WARN_MAX_USERS_EXCEEDED:
        reminder.send_warning_max_users_exceeded_all()
        reminder.inactivate_max_users_exceeded_all()
    assist.tasks.purge_emails_all()
    if settings.MOSSO_FILES_ENABLED:
        Account.objects.delete_old_files_from_cloud()
    NotedTaskMeta.objects.delete_old_finished()
    assist.tasks.GetSocialDataTask().delay()        # Get social data for recently created contacts
    log("cron nightly done")

def cron_morning(): # 6am
    reminder.send_unused_account_reminders_all()
    reminder.send_expiration_soon_reminders_all()
    log("cron morning done")
    
def cron_quarterhourly(do_it=True):
    reminder.send_reminders_all_last_minute(do_it=do_it)
    FileToken.objects.inactivate_old_tokens()
#    if settings.SERVER == 'live':
#        # For debugging this specific problem
#        alias = Alias.objects.get(email='calenvy.com', account__name='cc')
#        log("cron quarterhourly: server_smtp_enabled = ", alias.server_smtp_enabled)
    log("cron quarterhourly done")

def cron_halfhourly(do_it=True):
    reminder.send_event_response_requests_all(do_it=do_it) 
    assist.tasks.FollowupSalesforceInstallationsTask().delay()
    log("cron halfhourly done")
    
def cron_twicedaily():
    pass
    log("cron twicedaily done")

def cron_kill_stuck():
    "Runs every so often to clear stuck bars"
    NotedTaskMeta.objects.kill_all_stuck()  # While we're at it, reset any stuck crawlbars
 
def cron_crawl_slice():
    "Runs every 5 minutes to crawl a portion of the aliases in the background"
    NotedTaskMeta.objects.kill_all_stuck()  # While we're at it, reset any stuck crawlbars
    queryset = CrawlState.objects.all()
    if queryset.count() == 0:
        cs = CrawlState()
        cs.save()
    else:
        cs = queryset[0]
    crawler.crawl_slice(cs.current_slice, cs.num_slices)
    log("crawl slice (%d/%d) done."% (cs.current_slice, cs.num_slices))
    cs.current_slice = (cs.current_slice + 1) % cs.num_slices
    cs.save()

def cron_logo_download():
    assist.tasks.LogoDownloadTask().delay()
    log("Started CronLogoDownloadTask in background")
        
def cron(freq_string, force_send=False):

    if not freq_string:
        freq_string = '[not specified]'
    freq_string = freq_string.lower()

    log("cron job, freq = %s, now = %s" % (freq_string, str(now)))

    if freq_string == 'nightly':
        cron_nightly()
    elif freq_string == 'morning':
        cron_morning()
    elif freq_string == 'halfhour' or freq_string == 'halfhourly':
        cron_halfhourly()
    elif freq_string == 'quarterhour' or freq_string == 'quarterhourly':
        cron_quarterhourly()
    elif freq_string == 'kill_stuck':
        cron_kill_stuck()
    elif freq_string == 'crawl_slice':
        cron_crawl_slice()
    elif freq_string == 'logo_download':
        cron_logo_download()
    elif freq_string == 'weekdaily':
        cron_weekdaily()
    else:
        raise Exception("worker.py cron: unknown frequency setting %s" % freq_string)
      
            
# ===========================================================

def main():
    "Main entry point for Calenvy for command-line operations (cron jobs and certain maintenance tasks)"
    
    def stream(filename=None):
        if filename:
            log("About to open %s" % filename)
            try:
                return open(filename, 'r')
            except:
                raise Exception("Couldn't open the file!")
        else:
            return sys.stdin
            
    def argv_stream(default=None):
        if len(sys.argv) > 2:
            return stream(sys.argv[2])
        else:
            return stream(default)
            
    try:            
        if sys.argv[1] == '-d':
            debug = True
            settings.LOG_FILE = sys.stdout
            del sys.argv[1]
        else:
            debug = False

        if sys.argv[1] == 'celery_kill_all_started':
            # Reset the crawl bars -- called by the push_celery_worker script
            NotedTaskMeta.objects.kill_all_started()

        elif sys.argv[1] == 'procmail':
            procmail.procmail_direct(argv_stream(), debug=debug)

        elif sys.argv[1] == 'cron':
            # Run a cron job. Name of cron job is the second argument
            log("cron: args = ", sys.argv)
            if len(sys.argv) == 2:
                cron(None)
            elif len(sys.argv) == 3:
                if sys.argv[2] == '-f':
                    cron (None, force_send=True)
                else:
                    cron(sys.argv[2])
            elif len(sys.argv) == 4:
                force_send = sys.argv[3] == '-f'
                cron(sys.argv[2], force_send=force_send)
                
        elif sys.argv[1] == 'package_from_svn':
            # Called by the SVN post-commit hook for desktoppy packages:
            #  push-live updates the package
            import plugins.package
            p = plugins.package.Packager()
            p.package_from_svn(sys.argv[2], int(sys.argv[3]))
            
            
        else:
            raise Exception()
        
        return
    except:
        log(traceback.format_exc())
        log('worker.py: Invalid args ', ' '.join(sys.argv))
        
if __name__ == '__main__':
    if len(sys.argv) > 1: 
        if settings.SERVER == 'live' and settings.LOG_ENABLED:
            if sys.argv[1] == 'cron':
                settings.LOG_FILE = open(os.path.join(settings.LOG_DIR, 'cron_%s.log' % sys.argv[2]), 'a')
            else:
                settings.LOG_FILE = open(os.path.join(settings.LOG_DIR, 'live.log'), 'a')
        
    else:
        settings.LOG_FILE = sys.stdout
        log('Interactive mode')

    if len(sys.argv) > 1:
        log('&&&&&&&&&&&&&&&&&& worker.py: ', now().astimezone(pytz.timezone('US/Pacific')), ' '.join(sys.argv))
        main()
        log('&&&&&&&&&&&&&&&&&& worker.py done!')
        
        



