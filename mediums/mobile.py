"""
Middleware and template loaders to handle agents like iphone, firefox plugin, etc.

Agent can be specified in the GET or POST parameters (agent=iphone), or implicitly
in the User-Agent (for example, various smartphones map to "iphone")

This is used to specify the template directories used for the template loader.

Current agents used in this project:
    iphone         - iPhone (duh) and various smartphones with sufficiently good web browsers
    salesforce     - the iframe within the Salesforce plugin
    firefox        - the firefox/chrome plugin (not the Firefox browser!)
"""

from django.conf import settings
import re
from utils import *

# from http://opensource.washingtontimes.com/blog/post/coordt/2010/02/loading-templates-based-request-headers-django/
#  a place for the middleware to store the request object

try:
    from threading import local
except ImportError:
    from django.utils._threading_local import local

_thread_locals = local()

def get_current_request():
    return getattr(_thread_locals, 'request', None)

class RequestMiddleware(object):
    def process_request(self, request):
        _thread_locals.request = request

def load_template_source(template_name, template_dirs=None):
    """
    Wrapper for django's load_template_source, that uses additional template
    directories using the "agent" field in the request object (if any)
    
    If template_dirs is specified, overrides any other template_dirs we would use
    """
    
    import django.template.loaders.filesystem
    
    if template_dirs is None:
        
        template_dirs = settings.TEMPLATE_DIRS  # default template dirs
        request = get_current_request()         # get the current request from the local vars
        if request:
            agent = get_agent(request)
            if agent:                           # agent = 'iphone', etc.
                template_dirs = (os.path.join(settings.PROJECT_DIR, 'network/templates/mobile/%s' % agent), ) + template_dirs
       
    return django.template.loaders.filesystem.load_template_source(template_name, template_dirs =template_dirs)

load_template_source.is_usable = True


# from http://forums.macrumors.com/showthread.php?t=205417
MOBILE_HTTP_USER_AGENTS = [
    (r'Blazer',             'iphone'), # windowmob
    (r'Palm',               'iphone'), # windowmob
    (r'Handspring',         'iphone'),# windowmob 
    (r'Nokia',              'iphone'),# windowmob
    (r'Kyocera',            'iphone'),# windowmobiphone
    (r'Samsung',            'iphone'),# windowmobiphone
    (r'Motorola',           'iphone'),# windowmobiphone
    (r'Smartphone',         'iphone'),# windowmobiphone
    (r'Windows CE',         'iphone'),# windowmobiphone
    (r'BlackBerry9530',     'iphone'),
    (r'Blackberry',         'iphone'),# windowmobiphone
    (r'WAP',                'iphone'),# windowmobiphone
    (r'SonyEricsson',       'iphone'),# windowmobiphone
    (r'PlayStation Portable','iphone'),# windowmobiphone
    (r'LG',                 'iphone'),# windowmobiphone
    (r'MMP',                'iphone'),# windowmobiphone
    (r'OPWV',               'iphone'),# windowmobiphone
    (r'Symbian',            'iphone'),# windowmobiphone
    (r'EPOC',               'iphone'),# windowmob
    (r'iPhone',             'iphone'),
    (r'Android',            'iphone'),
    (r'Opera Mobi',         'iphone'),
    (r'Opera Mini',         'iphone'),
    #(r'mozilla',            'windowmob'),         # for debugging only
]

class MobileMiddleware(object):
    """
    If the Middleware detects a mobile phone, redirect to the same URL with agent=blah
    """

    def process_request(self, request):
                         
        agent = get_agent(request)
        if agent is None and 'HTTP_USER_AGENT' in request.META:
            user_agent = request.META['HTTP_USER_AGENT']
            #log("The user_agent is:", user_agent)
            for device, found_agent in MOBILE_HTTP_USER_AGENTS:
                if re.search(device, user_agent, re.IGNORECASE):
                    log("Found mobile template: ", device, " --> ", found_agent)
                    # Append agent=<agent> to the end of the path
                    return HttpResponseRedirect(redirect_agent_url(found_agent, request.get_full_path()))
       